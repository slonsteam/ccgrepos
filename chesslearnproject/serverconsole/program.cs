﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using CommonDll.Data;
using Contract.Cards;
using Contract.Core;
using SamePorjLibrary;
using SamePorjLibrary.Data;
using SamePorjLibrary.Encoders;
using SamePorjLibrary.Interfaces;
using ServerService;
using ServerService.Concrete.ActionLoger;
using ServerService.Concrete.Client;
using ServerService.Concrete.CommDispatcher;
using ServerService.Concrete.MessageHandler;
using ServerService.Concrete.Models;
using ServerService.Concrete.Notifyer;
using ServerService.Controllers;
using ServerService.Data;
using ServerService.Interfaces;

namespace ServerConsole
{
    public class Program
    {
        private static INotifyer _notifyer;

        public static void Main(string[] args)
        {
            var DIEncoders = new NinjectDependencyResolver();
            GlobalCore.DIEncoders = DIEncoders;
            DIEncoders.CreateBindings += k =>
            {
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("SessionFinded"))
                    .Named("SessionFinded");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("PlayCardResponce"))
                    .Named("PlayCardResponce");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("StartGame")).Named("StartGame");
                //k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("PlayCardAnswer")).Named("PlayCardAnswer");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("LogInResponse"))
                    .Named("LogInResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("EndTurnResponse"))
                    .Named("EndTurnResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetCardModelResponse"))
                    .Named("GetCardModelResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("ReturnToGameOffer"))
                    .Named("ReturnToGameOffer");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetTableResponse"))
                    .Named("GetTableResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("ClientLeave")).Named("ClientLeave");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetTargetsResponse"))
                    .Named("GetTargetsResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("EndGameResponse"))
                    .Named("EndGameResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetSessionListResponse"))
                    .Named("GetSessionListResponse");
                k.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("ViewForSessionResponse"))
                    .Named("ViewForSessionResponse");
            };
            DIEncoders.InitializeBindings();


            var DI = new NinjectDependencyResolver();
            DI.CreateBindings += kernel =>
            {
                var ServerPort = 103;

                kernel.Bind<int>().ToConstant(ServerPort).Named("ServerPort");
                kernel.Bind<ICommunicator>().To<ServerCommunicator>();
                kernel.Bind<IServerCommunicator>().To<ServerCommunicator>();
                kernel.Bind<IClient>().To<ClientTcp>();
                kernel.Bind<Encoding>().ToConstant(Encoding.UTF8);
                kernel.Bind<TcpListener>()
                    .ToConstant(new TcpListener(IPAddress.Any, ServerPort));
                kernel.Bind<ICommDispatcher>()
                    .ToConstant(new ThreadCommDispatcher(null, GlobalCore.DependencyResolver));
                kernel.Bind<INotifyer>().To<EventCenterNotifyer>().InSingletonScope();
                kernel.Bind<IDependencyResolver>().ToConstant(DI);
                kernel.Bind<ClientController>().ToSelf().InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<ISessionSearcher<StandartGameSession>>().To<StandartSessionSearcher>().InSingletonScope()
                    .WithConstructorArgument("DI", DI);
                kernel.Bind<ISessionController<StandartGameSession>>().To<StandartSessionController>()
                    .InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<ISessionStorage>().To<SessionStorage>().InSingletonScope()
                    .WithConstructorArgument("sessionCountInThread", 100);
                kernel.Bind<IActionLogger>().To<StandartActionLoger>();
                kernel.Bind<IEngine>().To<Engine>();
                //todo заменить
                kernel.Bind<Settings>().ToConstant(new Settings(decksize: 15));
                kernel.Bind<IRepository<Card>>().To<CardRepos>();
                kernel.Bind<IRepository<CardModel>>().To<CardModelRepos>();
                kernel.Bind<IDependencyResolver>().ToConstant(DIEncoders).Named("DIEncoders");
            };
            DI.InitializeBindings();
            GlobalCore.DependencyResolver = DI;

            var DIForMessageHandling = new NinjectDependencyResolver();
            GlobalCore.DIForMessageHandling = DIForMessageHandling;
            DIForMessageHandling.CreateBindings += k =>
            {
                k.Bind<IDependencyResolver>().ToConstant(GlobalCore.DependencyResolver);
                k.Bind<IServerMessageHandler>().To<LogInMessageHandler>().Named("LogIn");
                k.Bind<IServerMessageHandler>().To<ChatMessageHandler>().Named("ChatMessage");
                k.Bind<IServerMessageHandler>().To<RegisterMessageHandler>().Named("Register");
                k.Bind<IServerMessageHandler>().To<ReturnToOldGameResponceHandler>()
                    .Named("ReturnToOldGameResponce");
                k.Bind<IServerMessageHandler>().To<PlayCardMessageHandler>()
                    .Named("PlayCardMessage");
                k.Bind<IServerMessageHandler>().To<SearchGameMessageHandler>().Named("SearchGame");
                k.Bind<IServerMessageHandler>().To<EndTurnHandler>().Named("EndTurn");
                k.Bind<IServerMessageHandler>().To<ReturnToOldGameResponceHandler>().Named("ReturnToGameOfferResponse");
                k.Bind<IServerMessageHandler>().To<GetCardModelHandler>().InSingletonScope().Named("GetCardModel");
                k.Bind<IServerMessageHandler>().To<GetTableMessageHandler>().InSingletonScope().Named("GetTable");
                k.Bind<IServerMessageHandler>().To<GetTargetsHandler>().InSingletonScope().Named("GetTargets");
                k.Bind<IServerMessageHandler>().To<GetSessionListHandler>().InSingletonScope().Named("GetSessionList");
                k.Bind<IServerMessageHandler>().To<ViewForSessionHandler>().InSingletonScope().Named("ViewForSession");
                k.Bind<IServerMessageHandler>().To<EndGameHandler>().InSingletonScope().Named("EndGame");
            };
            DIForMessageHandling.InitializeBindings();

            GlobalCore.DIForMessageHandling = DIForMessageHandling;

            _notifyer = DI.Get<INotifyer>();

            var conveyor =
                new MessageHandlingConveyor(GlobalCore.DIForMessageHandling, GlobalCore.DependencyResolver);
            conveyor.Start();

            Application.ApplicationExit += (s, a) => { conveyor.Dispose(); };

            //_notifyer.CanNotHandleMessage += (s, a) => Console.WriteLine($"non standart message: {a.Message}");
            _notifyer.MessageRecieved += (s, a) => Console.WriteLine(a.Message);

            Console.ReadLine();
        }
    }
}