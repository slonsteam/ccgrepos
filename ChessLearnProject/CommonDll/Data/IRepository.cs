﻿using System.Collections.Generic;

namespace CommonDll.Data
{
    public interface IRepository<T>
    {
        IEnumerable<T> Collection { get; set; }
        T Get(string id);
    }
}