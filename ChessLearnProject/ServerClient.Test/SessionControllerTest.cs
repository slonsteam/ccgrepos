﻿namespace ServerClient.Test
{
    //[TestClass]
    //public class SessionControllerTest
    //{
    //    private static IDependencyResolver DI;
    //    static INotifyer notifayer;
    //    private List<StandartGameSession> list;
    //    Mock<StandartSessionController> controllerMock;

    //    [TestInitialize]
    //    public void TestSturtUp()
    //    {

    //        list = new List<StandartGameSession>();
    //        Mock<ISessionStorage> storageMock=new Mock<ISessionStorage>();
    //        storageMock.Setup(m => m.AddSesssions(It.IsAny<IGameSession>()))
    //            .Callback<IGameSession>((s) => list.Add(s as StandartGameSession));
    //        storageMock.Setup(m => m.Remove(It.IsAny<IGameSession>()))
    //            .Callback<IGameSession>((s) => list.Remove(s as StandartGameSession));
    //        notifayer = new EventCenterNotifyer();
    //        Mock<IDependencyResolver> DIMock = new Mock<IDependencyResolver>();
    //        DIMock.Setup(di => di.Get<TcpListener>()).Returns(new TcpListener(IPAddress.Any, 103));
    //        DIMock.Setup(di => di.Get<Encoding>()).Returns(Encoding.ASCII);
    //        DIMock.Setup(di => di.Get<INotifyer>()).Returns(notifayer);
    //        DIMock.Setup(di => di.Get<ICommDispatcher>()).Returns(new ThreadCommDispatcher(null, DIMock.Object));
    //        DIMock.Setup(di => di.Get<IServerMessageHandler>("LogIn")).Returns(new LogInMessageHandler(DIMock.Object));
    //        DIMock.Setup(di => di.Get<ISessionStorage>()).Returns(storageMock.Object);
    //        controllerMock = new Mock<StandartSessionController>(DIMock.Object);
    //        DIMock.Setup(di => di.Get<ISessionController<StandartGameSession>>()).Returns(controllerMock.Object);
    //        DI = DIMock.Object;

    //    }

    //    [TestMethod]
    //    public void CreatingSessionCorrect()
    //    {

    //        StandartSessionController controller = controllerMock.Object;

    //        controller.Create(new List<IClient> {new ClientTcp {Login = "1"}, new ClientTcp {Login = "2"}});

    //        Assert.AreEqual(1,list.Count);
    //        Assert.AreEqual("1",list[0].FirstPlayer.Login);
    //        Assert.AreEqual("2", list[0].SecondPlayer.Login);
    //    }

    //    [TestMethod]
    //    public void PlayerLeaveCorrect()
    //    {
    //        AutoResetEvent reset=new AutoResetEvent(false);

    //        StandartSessionController controller = new StandartSessionController(DI);
    //        IClient cl1 = new ClientTcp { Login = "1", IsLoged = true};
    //        IClient cl2 = new ClientTcp { Login = "2", IsLoged = true};

    //        controller.Create(new List<IClient> { cl1, cl2 });
    //        list[0].Start();

    //        list[0].SessionPaused += (s, a) => reset.Set();

    //        cl1.IsLoged = false;
    //        notifayer.OnClientTryLogOut(cl1);

    //        if(!reset.WaitOne(1000))
    //            Assert.Fail();

    //        Assert.AreEqual(SessionStatus.AwaitPlayer, list[0].Status);
    //        Assert.IsFalse(list[0].Clients == null);
    //    }

    //    [TestMethod]
    //    public void PlayerReturnCorrect()
    //    {
    //        AutoResetEvent reset = new AutoResetEvent(false);

    //        StandartSessionController controller = new StandartSessionController(DI);
    //        IClient cl1 = new ClientTcp { Login = "1", IsLoged = true };
    //        IClient cl2 = new ClientTcp { Login = "2", IsLoged = true };

    //        controller.Create(new List<IClient> { cl1, cl2 });
    //        list[0].Start();

    //        list[0].SessionPaused += (s, a) => reset.Set();

    //        cl1.IsLoged = false;
    //        notifayer.OnClientTryLogOut(cl1);

    //        if (!reset.WaitOne(1000))
    //            Assert.Fail();

    //        list[0].SessionResumed += (s, a) => reset.Set();

    //        cl1.IsLoged = true;
    //        controller.PlayerReturnToOldSession(list[0],cl1);

    //        if (!reset.WaitOne(1000))
    //            Assert.Fail();

    //        Assert.AreEqual(SessionStatus.Play, list[0].Status);
    //        Assert.IsFalse(list[0].Clients == null);
    //    }

    //    [TestMethod]
    //    public void PlayerDenyOldSessionCorrect()
    //    {
    //        AutoResetEvent reset = new AutoResetEvent(false);

    //        StandartSessionController controller = new StandartSessionController(DI);
    //        IClient cl1 = new ClientTcp { Login = "1", IsLoged = true };
    //        IClient cl2 = new ClientTcp { Login = "2", IsLoged = true };

    //        var session = controller.Create(new List<IClient> {cl1, cl2});
    //        session.Start();

    //        session.SessionPaused += (s, a) => reset.Set();

    //        cl1.IsLoged = false;
    //        notifayer.OnClientTryLogOut(cl1);

    //        if (!reset.WaitOne(1000))
    //            Assert.Fail();

    //        session.SessionEndedUnstandart += (s, a) => reset.Set();

    //        cl1.IsLoged = true;
    //        controller.PlayerDenyOldSession(session, cl1);

    //        if (!reset.WaitOne(1000))
    //            Assert.Fail();

    //        Assert.AreEqual(SessionStatus.AwaitPlayer, session.Status);
    //        Assert.IsTrue(session.Clients == null);
    //        CollectionAssert.DoesNotContain(list,session);
    //    }

    //    [TestMethod]
    //    public void BothPlayerLeaveCorrect()
    //    {
    //        AutoResetEvent reset = new AutoResetEvent(false);

    //        StandartSessionController controller = new StandartSessionController(DI);
    //        IClient cl1 = new ClientTcp { Login = "1", IsLoged = true };
    //        IClient cl2 = new ClientTcp { Login = "2", IsLoged = true };

    //        var session = controller.Create(new List<IClient> { cl1, cl2 });
    //        session.Start();

    //        session.SessionEndedUnstandart += (s, a) => reset.Set();

    //        cl1.IsLoged = false;
    //        notifayer.OnClientTryLogOut(cl1);

    //        cl2.IsLoged = false;
    //        notifayer.OnClientTryLogOut(cl2);

    //        if (!reset.WaitOne(1000))
    //            Assert.Fail();

    //        Assert.AreEqual(SessionStatus.AwaitPlayer, session.Status);
    //        Assert.IsTrue(session.Clients == null);
    //        CollectionAssert.DoesNotContain(list, session);
    //    }
    //}
}