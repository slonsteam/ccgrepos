﻿using System.Threading;
using Client.Concrete.MessageEncoders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Concrete.Client;
using ServerService.Concrete.MessageHandler;
using ServerService.Concrete.Notifyer;
using ServerService.Controllers;
using ServerService.Interfaces;
using Sprache;

namespace ServerClient.Test
{
    [TestClass]
    public class LoginMessageProcessTest
    {
        private static IDependencyResolver DI;

        [ClassInitialize]
        public static void TestSturtUp(TestContext context)
        {
            var DIMock = new Mock<IDependencyResolver>();
            var notifayer = new EventCenterNotifyer();
            DIMock.Setup(d => d.Get<INotifyer>()).Returns(notifayer);
            var controller = new ClientController(DIMock.Object);
            controller.RegisterClient("testLogin", "testPassword");
            DIMock.Setup(di => di.Get<ClientController>()).Returns(controller);
            DI = DIMock.Object;
        }

        [TestMethod]
        public void MessageHandledCorrect()
        {
            var messageBodyParser =
                from type in Parse.Letter.Many()
                from spliter in Parse.Char(':').Once()
                from body in Parse.AnyChar.Many().Text().End()
                select body;
            var reset = new AutoResetEvent(false);
            var model = new LogInNetworkModel("testLogin", "testPassword");
            var encoder = new LogInMessageEncoder();
            var message = encoder.EncodeMessage(model);
            var handler = new LogInMessageHandler(DI);
            var login = "";
            var password = "";
            DI.Get<INotifyer>().ClientTryLogIn += (s, a) =>
            {
                login = a.Login;
                password = a.Password;
                reset.Set();
            };

            var ComMock = new Mock<IServerCommunicator>();
            var client = new UnauthorisedClient(ComMock.Object);
            ComMock.Setup(com => com.Owner).Returns(client);
            client.Communicator = ComMock.Object;
            handler.HandleMessage(messageBodyParser.Parse(message), client);

            if (!reset.WaitOne(1000))
                Assert.Fail();

            Assert.AreEqual(model.Login, login);
            Assert.AreEqual(model.Password, password);
        }
    }
}