﻿using System.Linq;
using Cards.Units;
using Contract.Actions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServerClient.Test
{
    [TestClass]
    public class LoggerTest
    {
        [TestMethod]
        public void TestParameters()
        {
            var action = new Attack(new Ogre(), new Ogre());

            var parameters = action.GetParameters();

            Assert.AreEqual(2, parameters.Count);
            Assert.AreEqual("Source", parameters.Keys.First());
            Assert.IsTrue(parameters["Source"] is Ogre);
        }
    }
}