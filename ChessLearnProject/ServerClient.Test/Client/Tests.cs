﻿using Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerConsole;

namespace ServerClient.Test.Client
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void StartGameTest()
        {
            Program.Main(null);
            var client1 = new App();
            var client2 = new App();

            client1.Run();
            client2.Run();
        }
    }
}