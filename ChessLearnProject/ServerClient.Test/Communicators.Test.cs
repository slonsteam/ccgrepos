﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerService;

namespace ServerClient.Test
{
    [TestClass]
    public class CommunicatorsTest
    {
        private readonly TcpListener listener = new TcpListener(IPAddress.Any, 103);

        [TestMethod]
        public void CommunicatorRecieveMessageCorrect()
        {
            var reset = new AutoResetEvent(false);

            listener.Start();
            var clientCommunicator = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            var serverCommunicator = new ServerCommunicator(listener.AcceptTcpClient(), Encoding.ASCII);
            serverCommunicator.BeginListenForMessage();
            clientCommunicator.BeginListenForMessage();
            var serverReadMessage = false;
            serverCommunicator.MessageReaded += (s, a) =>
            {
                serverReadMessage = a.Message.Equals("Hello");
                reset.Set();
            };
            var clientResponseMessage = false;
            clientCommunicator.MessageReaded += (s, a) =>
            {
                clientResponseMessage = a.Message.Equals("Hello too");
                reset.Set();
            };

            clientCommunicator.SendMessage("Hello");
            if (!reset.WaitOne(1000)) Assert.Fail();

            serverCommunicator.SendMessage("Hello too");
            if (!reset.WaitOne(1000)) Assert.Fail();

            listener.Stop();

            Assert.IsTrue(serverReadMessage);
            Assert.IsTrue(clientResponseMessage);
        }

        [TestMethod]
        public void CommunicatorsCanConnect()
        {
            var reset = new AutoResetEvent(false);
            listener.Start();
            var clientCommunicator = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            var serverCommunicator = new ServerCommunicator(listener.AcceptTcpClient(), Encoding.ASCII);
            serverCommunicator.BeginListenForMessage();
            clientCommunicator.BeginListenForMessage();

            serverCommunicator.MessageReaded += (s, a) => { reset.Set(); };
            clientCommunicator.MessageReaded += (s, a) => { reset.Set(); };

            clientCommunicator.SendMessage("Hello:Hello");
            if (!reset.WaitOne(1000)) Assert.Fail();

            serverCommunicator.SendMessage("Hello:Hello too");
            if (!reset.WaitOne(1000)) Assert.Fail();
            listener.Stop();
        }

        [TestMethod]
        public void CommunicatorsCanDisconnect()
        {
            var reset = new AutoResetEvent(false);
            var DisconectReset = new AutoResetEvent(false);
            listener.Start();
            var clientCommunicator = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            var serverCommunicator = new ServerCommunicator(listener.AcceptTcpClient(), Encoding.ASCII);
            serverCommunicator.BeginListenForMessage();
            clientCommunicator.BeginListenForMessage();

            serverCommunicator.MessageReaded += (s, a) => { reset.Set(); };
            clientCommunicator.ConnectionBroken += (s, a) => { DisconectReset.Set(); };

            clientCommunicator.SendMessage("Hello:Hello");
            if (!reset.WaitOne(1000)) Assert.Fail();

            serverCommunicator.Disconect();
            if (!DisconectReset.WaitOne(2000)) Assert.Fail();
            listener.Stop();
        }

        [TestMethod]
        public void CanReadBigMessage()
        {
            var reset = new AutoResetEvent(false);
            listener.Start();
            var clientCommunicator = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            var serverCommunicator = new ServerCommunicator(listener.AcceptTcpClient(), Encoding.ASCII);
            serverCommunicator.BeginListenForMessage();

            var recievedMessage = "";

            serverCommunicator.MessageReaded += (s, a) =>
            {
                recievedMessage = a.Message;
                reset.Set();
            };

            var message = "spam:";
            for (var i = 0; i < 2056; i++) message += "*";

            clientCommunicator.SendMessage(message);

            if (!reset.WaitOne(1000)) Assert.Fail();

            Assert.AreEqual(message.Length, recievedMessage.Length);
            Assert.AreEqual(message, recievedMessage);
        }

        [TestMethod]
        public void CanReadDoubleMessage()
        {
            var reset = new AutoResetEvent(false);
            listener.Start();
            var client = new TcpClient("127.0.0.1", 103);
            var serverCommunicator = new ServerCommunicator(listener.AcceptTcpClient(), Encoding.ASCII);
            serverCommunicator.BeginListenForMessage();

            var recievedMessage = "";

            serverCommunicator.MessageReaded += (s, a) =>
            {
                recievedMessage = a.Message;
                reset.Set();
            };
            var FirstHalf = "test:FirstHalf";
            var SecondHalf = "SecondHalf";

            var message = FirstHalf + SecondHalf;


            var stream = client.GetStream();
            var bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes
                ((uint) (FirstHalf.Length + SecondHalf.Length)));
            bytes.AddRange(Encoding.ASCII.GetBytes(FirstHalf));
            var FirstHalfByte = bytes.ToArray();
            var SeconHalfByte = Encoding.ASCII.GetBytes(SecondHalf);
            stream.Write(FirstHalfByte, 0, FirstHalfByte.Length);
            stream.Write(SeconHalfByte, 0, SeconHalfByte.Length);

            if (!reset.WaitOne(1000)) Assert.Fail();

            Assert.AreEqual(message.Length, recievedMessage.Length);
            Assert.AreEqual(message, recievedMessage);
        }
    }
}