﻿using System.Collections.Generic;
using System.Linq;
using Cards.Units;
using CommonDll.Data;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ServerClient.Test.GameLogic
{
    [TestClass]
    public partial class LogicTest
    {
        private static IRepository<Card> Cards;
        private static Settings Settings;
        private static Dictionary<string, List<string>> playerdecks;

        public LogicTest()
        {
            var mock = new Mock<IRepository<Card>>();
            var cards = new List<Card>
            {
                new Ogre(),
                new Warlock()
            };
            mock.Setup(x => x.Collection).Returns(cards);
            mock.Setup(x => x.Get(It.IsAny<string>()))
                .Returns<string>(x => cards.FirstOrDefault(d => d.Identificator == x));
            Cards = mock.Object;
            playerdecks = new Dictionary<string, List<string>>
            {
                {"kek", new List<string> {"Warlock", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre"}},
                {"dokek", new List<string> {"Warlock", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre"}}
            };
            Settings = new Settings(2, 5);
        }

        [TestMethod]
        public void StartGameTest()
        {
            //arrange
            var engine = new Engine(Cards, null, Settings);
            //act
            engine.StartGame(playerdecks);
            var curplayer = engine.Table.Players[engine.Table.Queue];
            engine.Table.Queue++;
            var enemyplayer = engine.Table.Players[engine.Table.Queue];
            //assert
            Assert.AreEqual(2, engine.Table.Players.Count, "Количество игроков не соответствует");
            Assert.AreEqual("Warlock", curplayer.Hero.Identificator, "Неверный герой");
            Assert.AreEqual(0, curplayer.Cards.FromLocation(Location.InDeck).Count,
                "Количество карт в колоде первого игрока не совпадает");
            Assert.AreEqual(5, curplayer.Cards.FromLocation(Location.InHand).Count,
                "Количество карт в руке первого игрока");
            Assert.AreEqual(4, enemyplayer.Cards.FromLocation(Location.InHand).Count,
                "Количество карт в руке противника");
            Assert.AreEqual(1, curplayer.MaxMana, "Постоянная мана игрока");
            Assert.AreEqual(1, curplayer.Mana, "Мана игрока на текущий ход");
        }

        [TestMethod]
        public void GiveCard()
        {
            //arrange
            var engine = new Engine(Cards, null);
            var player = new PlayerModel(engine);
            player.CardFactory.Create("Ogre");
            player.CardFactory.Create("Ogre");
            //act
            engine.Actions.Launch(new GiveCard(player));
            engine.Actions.Launch(new GiveCard(player));
            engine.Actions.Launch(new GiveCard(player));
            //assert
            Assert.AreEqual(2, player.Cards.FromLocation(Location.InHand).Count);
        }

        [TestMethod]
        public void CellTest()
        {
            //arrange
            var engine = new Engine(Cards, null);
            var player = new PlayerModel(engine);

            for (var i = 0; i < 3; i++)
            {
                player.CardFactory.Create("Ogre");
                engine.Actions.Launch(new GiveCard(player));
            }
            //act

            //assert
            Assert.AreEqual(2, player.Cards.FromLocation(Location.InHand).Count);
        }

        [TestMethod]
        public void GameProcess()
        {
            //arrange
            var engine = new Engine(Cards, null, Settings);
            engine.StartGame(playerdecks);
            var curpPlayer = engine.Table.Players[engine.Table.Queue];
            engine.PlayCard(curpPlayer.Token, curpPlayer.Cards.FromLocation(Location.InHand).First().Token, null);
            engine.EndTurn(curpPlayer.Token);
            var enemyPlayer = engine.Table.Players[engine.Table.Queue];
            engine.PlayCard(enemyPlayer.Token, enemyPlayer.Cards.FromLocation(Location.InHand).First().Token, null);
            engine.EndTurn(enemyPlayer.Token);
            var source = curpPlayer.Cards.FromLocation(Location.Table).First();
            var target = enemyPlayer.Cards.FromLocation(Location.Table).First();
            //act
            engine.PlayCard(curpPlayer.Token, source.Token, new List<string> {target.Token});
            engine.PlayCard(curpPlayer.Token, source.Token, new List<string> {target.Token});
            //assert
            Assert.AreEqual(3, (source as Unit).Health);
            Assert.AreEqual(3, (source as Unit).Health);
        }

        [TestMethod]
        public void EndGameTest()
        {
            //arrange
            var engine = new Engine(Cards, null);
            var manager = engine.Actions;
            var factory = new CardFactory(new PlayerModel(engine));
            var hero = factory.Initialize(new Warlock());
            //act
            manager.Launch(new CreateDamage(31, hero));
            //assert
            Assert.IsTrue(hero.Location == Location.Death);
        }

        [TestMethod]
        public void CardTokenTest()
        {
            var engine = new Engine(Cards, null);
            var cf = new CardFactory(new PlayerModel(engine));

            var card = cf.Create("Ogre");

            Assert.IsTrue(card.Token.Length == 6);
        }
        //[TestMethod]
        //public void TestParametersStartGame()
        //{
        //    var engine = new Engine(Cards, new StandartActionLoger(), Settings);

        //   var strin = engine.StartGame(playerdecks);


        //    Assert.AreEqual(2, strin.Length);

        //}
    }
}