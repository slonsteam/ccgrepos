﻿using System.Linq;
using Contract.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServerClient.Test.GameLogic
{
    public partial class LogicTest
    {
        [TestMethod]
        public void SettingsTest()
        {
            //arrange
            var engine = new Engine(Cards, null, new Settings(2, 6, 3));
            playerdecks.First().Value.Add("Ogre");
            playerdecks.Last().Value.Add("Ogre");
            //act
            engine.StartGame(playerdecks);

            //assert
            Assert.AreEqual(4, engine.Table.Players[engine.Table.Queue].Cards.FromLocation(Location.InHand).Count);
            Assert.AreEqual(2, engine.Table.Players[engine.Table.Queue].Cards.FromLocation(Location.InDeck).Count);
        }
    }
}