﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Client;
using Client.Concrete.MessageEncoders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService;
using ServerService.Concrete.CommDispatcher;
using ServerService.Concrete.MessageHandler;
using ServerService.Concrete.Notifyer;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerClient.Test
{
    [TestClass]
    public class MessageHanlingConveyorTest
    {
        private static IDependencyResolver DI;

        [ClassInitialize]
        public static void TestSturtUp(TestContext context)
        {
            INotifyer notifayer = new EventCenterNotifyer();
            var syncContext = new SynchronizationContext();

            var DIMock = new Mock<IDependencyResolver>();
            DIMock.Setup(di => di.Get<SynchronizationContext>("MainThread")).Returns(syncContext);
            DIMock.Setup(di => di.Get<TcpListener>()).Returns(new TcpListener(IPAddress.Any, 103));
            DIMock.Setup(di => di.Get<Encoding>()).Returns(Encoding.ASCII);
            DIMock.Setup(di => di.Get<INotifyer>()).Returns(notifayer);
            var controller = new ClientController(DIMock.Object);
            controller.RegisterClient("login", "password");
            DIMock.Setup(di => di.Get<ClientController>()).Returns(controller);
            DIMock.Setup(di => di.Get<ICommDispatcher>()).Returns(new ThreadCommDispatcher(null, DIMock.Object));
            DIMock.Setup(di => di.Get<IServerMessageHandler>("LogIn")).Returns(new LogInMessageHandler(DIMock.Object));
            DI = DIMock.Object;
        }

        [TestMethod]
        public void IncorrectMessageHandleCorrect()
        {
            var resetEvent = new AutoResetEvent(false);
            var conveyor = new MessageHandlingConveyor(DI, DI);
            conveyor.Start();
            var TestCorrect = false;
            DI.Get<INotifyer>().CanNotHandleMessage +=
                (s, a) =>
                {
                    TestCorrect = true;
                    resetEvent.Set();
                };
            ICommunicator client = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            client.SendMessage("Hello");

            if (!resetEvent.WaitOne(2000))
                Assert.Fail();

            Assert.IsTrue(TestCorrect);
        }

        [TestMethod]
        public void LogInCorrect()
        {
            var resetEvent = new AutoResetEvent(false);
            var Conveyor = new MessageHandlingConveyor(DI, DI);
            Conveyor.Start();
            var TestCorrect = false;
            DI.Get<INotifyer>().ClientTryLogIn +=
                (s, a) =>
                {
                    TestCorrect = true;
                    resetEvent.Set();
                };
            ICommunicator client = new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            client.SendMessage(new LogInMessageEncoder().EncodeMessage(new LogInNetworkModel("login", "password")));

            if (!resetEvent.WaitOne(2000))
                Assert.Fail();

            Assert.IsTrue(TestCorrect);
        }
    }
}