﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SamePorjLibrary.Interfaces;
using ServerService.Concrete.Client;
using ServerService.Concrete.Notifyer;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerClient.Test
{
    [TestClass]
    public class ChatMessageTest
    {
        private static IDependencyResolver DI;

        [ClassInitialize]
        public static void TestSturtUp(TestContext context)
        {
            var DIMock = new Mock<IDependencyResolver>();
            var notifayer = new EventCenterNotifyer();
            DIMock.Setup(d => d.Get<INotifyer>()).Returns(notifayer);
            var controller = new ClientController(DIMock.Object);
            controller.AllClients.Add(new ClientTcp {Login = "log1", Password = "pass1", Identificator = "id1"});
            controller.RegisterClient("testLogin", "testPassword");
            DIMock.Setup(di => di.Get<ClientController>()).Returns(controller);
            DI = DIMock.Object;
        }

        [TestMethod]
        public void TestMethod1()
        {
        }
    }
}