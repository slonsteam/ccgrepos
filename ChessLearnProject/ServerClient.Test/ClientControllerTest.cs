﻿using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Client;
using Client.Concrete.MessageEncoders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService;
using ServerService.Concrete.CommDispatcher;
using ServerService.Concrete.MessageHandler;
using ServerService.Concrete.Notifyer;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerClient.Test
{
    [TestClass]
    public class ClientControllerTest
    {
        private static IDependencyResolver DI;
        private static INotifyer notifayer;
        private static ClientController controller;

        [TestInitialize]
        public void TestSturtUp()
        {
            var encoder = new Mock<IMessageEncoder>();
            encoder.Setup(e => e.EncodeMessage(It.IsAny<INetworkModel>())).Returns("LogInResponse:response");
            var DIEncoders = new Mock<IDependencyResolver>();
            DIEncoders.Setup(di => di.Get<IMessageEncoder>("LogInResponse"))
                .Returns(encoder.Object);
            notifayer = new EventCenterNotifyer();
            var DIMock = new Mock<IDependencyResolver>();
            DIMock.Setup(di => di.Get<TcpListener>()).Returns(new TcpListener(IPAddress.Any, 103));
            DIMock.Setup(di => di.Get<Encoding>()).Returns(Encoding.ASCII);
            DIMock.Setup(di => di.Get<INotifyer>()).Returns(notifayer);
            DIMock.Setup(di => di.Get<IDependencyResolver>("DIEncoders")).Returns(DIEncoders.Object);
            controller = new ClientController(DIMock.Object);
            DIMock.Setup(di => di.Get<ClientController>()).Returns(controller);
            DIMock.Setup(di => di.Get<ICommDispatcher>()).Returns(new ThreadCommDispatcher(null, DIMock.Object));
            DIMock.Setup(di => di.Get<IServerMessageHandler>("LogIn")).Returns(new LogInMessageHandler(DIMock.Object));

            DI = DIMock.Object;
        }

        [TestMethod]
        public void CanRegisterNewUser()
        {
            var controller = new ClientController(DI);

            var result = controller.RegisterClient("user", "password");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CanNotRegisterExistingUser()
        {
            var controller = new ClientController(DI);
            controller.RegisterClient("user", "password");

            var result = controller.RegisterClient("user", "password2");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void UserLogInCorrectIntegrTest()
        {
            var reset = new AutoResetEvent(false);
            controller.RegisterClient("user", "password");

            var conveyor = new MessageHandlingConveyor(DI, DI);
            conveyor.Start();
            var clientCommunicator =
                new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            clientCommunicator.SendMessage(
                new LogInMessageEncoder().EncodeMessage(new LogInNetworkModel("user", "password")));
            notifayer.ClientTryLogIn += (s, a) => reset.Set();

            if (!reset.WaitOne(1000))
                Assert.Fail();

            Assert.IsTrue(controller.AllClients.First().IsLoged);
            Assert.AreEqual(1, controller.AllClients.Count);
        }

        [TestMethod]
        public void UserLogInWithTokenCorrectIntegrTest()
        {
            var reset = new AutoResetEvent(false);
            controller.RegisterClient("user", "password");
            controller.AllClients.First().Identificator = "12345";

            var conveyor = new MessageHandlingConveyor(DI, DI);
            conveyor.Start();
            var clientCommunicator =
                new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            clientCommunicator.SendMessage(
                new LogInMessageEncoder().EncodeMessage(new LogInNetworkModel("12345")));
            notifayer.ClientTryLogIn += (s, a) => reset.Set();

            if (!reset.WaitOne(1000))
                Assert.Fail();

            Assert.IsTrue(controller.AllClients.First().IsLoged);
            Assert.AreEqual(1, controller.AllClients.Count);
        }

        [TestMethod]
        public void UserLogOutCorrectIntegrTest()
        {
            var reset = new AutoResetEvent(false);
            controller.RegisterClient("user", "password");

            var conveyor = new MessageHandlingConveyor(DI, DI);
            conveyor.Start();
            var clientCommunicator =
                new ClientCommunicator(new TcpClient("127.0.0.1", 103), Encoding.ASCII);
            clientCommunicator.SendMessage(
                new LogInMessageEncoder().EncodeMessage(new LogInNetworkModel("user", "password")));

            notifayer.ClientTryLogIn += (s, a) => reset.Set();

            if (!reset.WaitOne(1000))
                Assert.Fail();

            Assert.IsTrue(controller.AllClients.First().IsLoged);

            clientCommunicator.Disconect();

            notifayer.ClientLogOut += (s, a) => reset.Set();

            if (!reset.WaitOne(2000))
                Assert.Fail();

            Assert.IsFalse(controller.AllClients.First().IsLoged);
        }
    }
}