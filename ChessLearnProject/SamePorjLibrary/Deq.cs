﻿using System;

namespace SamePorjLibrary
{
    internal class Deq<T>
    {
        private T[] array;

        public Deq()
        {
            array = new T[0];
        }

        public int Count => array.Length;

        public bool Empty => array.Length > 0;

        public T Front => array[0];

        public T Back => array[array.Length - 1];

        public void PushBack(T item)
        {
            Array.Resize(ref array, array.Length + 1);
            array[array.Length - 1] = item;
        }

        public void PushFront(T item)
        {
            Array.Resize(ref array, array.Length + 1);
            for (var i = array.Length - 1; i > 0; i--)
                array[i] = array[i - 1];
            array[0] = item;
        }

        public T PopBack()
        {
            var item = array[array.Length - 1];
            Array.Resize(ref array, array.Length - 1);
            return item;
        }

        public T PopFront()
        {
            var item = array[0];
            for (var i = 0; i < array.Length - 1; i++)
                array[i] = array[i + 1];
            Array.Resize(ref array, array.Length - 1);
            return item;
        }
    }
}