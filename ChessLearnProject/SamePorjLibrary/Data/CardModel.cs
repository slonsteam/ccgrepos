﻿namespace SamePorjLibrary.Data
{
    public class CardModel
    {
        public string Identificator { get; set; }
        public int BaseCost { get; set; }
        public int BaseHealth { get; set; }
        public int BaseDamage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string ImageSource { get; set; }
        public int Priority { get; set; }
        public bool CanBeInDec { get; set; }
    }
}