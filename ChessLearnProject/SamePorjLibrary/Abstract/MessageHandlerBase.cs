﻿using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.Abstract
{
    public class MessageHandlerBase
    {
        protected IDependencyResolver DI;

        public MessageHandlerBase(string MessageType, IDependencyResolver DI)
        {
            this.DI = DI;
            this.MessageType = MessageType;
        }

        public virtual string MessageType { get; }

        protected virtual bool HandleMessage(string message, ICommunicator communicator)
        {
            return true;
        }
    }
}