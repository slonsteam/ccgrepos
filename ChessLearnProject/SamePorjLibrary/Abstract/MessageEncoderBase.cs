﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.Abstract
{
    public abstract class MessageEncoderBase : IMessageEncoder
    {
        public MessageEncoderBase(string messageType)
        {
            MessageType = messageType;
        }

        public string MessageType { get; }

        public virtual string EncodeMessage(INetworkModel model)
        {
            return MessageType + ":" + JsonConvert.SerializeObject(model);
        }
    }
}