﻿using System.Collections.Generic;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class GetSessionListResponseNetworkModel : INetworkModel
    {
        public List<SessionModel> Sessions;

        public GetSessionListResponseNetworkModel(List<SessionModel> session)
        {
            Sessions = session;
        }
    }

    public class SessionModel
    {
        public string Id;
        public List<string> Players;
    }
}