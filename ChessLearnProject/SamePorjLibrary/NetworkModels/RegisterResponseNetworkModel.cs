﻿using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class RegisterResponseNetworkModel : INetworkModel
    {
        public string Message;
        public bool RegisterComplete;
    }
}