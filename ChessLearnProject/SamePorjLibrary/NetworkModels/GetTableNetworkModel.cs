﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class GetTableNetworkModel : INetworkModel
    {
        public string Table;

        [JsonConstructor]
        public GetTableNetworkModel(string table)
        {
            Table = table;
        }
    }
}