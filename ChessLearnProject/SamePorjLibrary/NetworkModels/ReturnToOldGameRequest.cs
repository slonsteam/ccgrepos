﻿namespace SamePorjLibrary.NetworkModels
{
    public class ReturnToOldGameRequest : SimpleTextNetworkModel
    {
        public ReturnToOldGameRequest(string message) : base(message)
        {
        }
    }

    public class ReturnToOldGameResponce
    {
        public bool Answer;
    }
}