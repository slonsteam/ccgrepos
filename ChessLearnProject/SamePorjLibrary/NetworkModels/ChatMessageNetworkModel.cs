﻿using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class ChatMessageNetworkModel : INetworkModel
    {
        public string Message;
        public string Name;
    }
}