﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class SimpleTextNetworkModel : INetworkModel
    {
        public string Message;

        [JsonConstructor]
        public SimpleTextNetworkModel(string message)
        {
            Message = message;
        }
    }
}