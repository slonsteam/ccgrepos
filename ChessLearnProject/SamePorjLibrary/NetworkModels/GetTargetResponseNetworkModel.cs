﻿using System.Collections.Generic;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class GetTargetResponseNetworkModel : INetworkModel
    {
        public string Id;
        public List<string> Targets;
    }
}