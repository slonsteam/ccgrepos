﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class LogInNetworkModel : INetworkModel
    {
        public string Login;
        public string Password;
        public string Token;

        public LogInNetworkModel(string login, string password)
        {
            Login = login;
            Password = password;
        }

        [JsonConstructor]
        public LogInNetworkModel(string login, string password, string token)
        {
            Login = login;
            Password = password;
            Token = token;
        }

        public LogInNetworkModel(string token)
        {
            Token = token;
        }
    }
}