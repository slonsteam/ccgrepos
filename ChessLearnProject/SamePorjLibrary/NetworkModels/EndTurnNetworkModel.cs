﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class EndTurnNetworkModel : INetworkModel
    {
        public string Actions;

        [JsonConstructor]
        public EndTurnNetworkModel(string actions)
        {
            Actions = actions;
        }
    }
}