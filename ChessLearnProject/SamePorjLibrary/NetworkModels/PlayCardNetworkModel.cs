﻿using System.Collections.Generic;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class PlayCardNetworkModel : INetworkModel
    {
        public string CardPlayed { get; set; }
        public List<string> Targets { get; set; }
    }
}