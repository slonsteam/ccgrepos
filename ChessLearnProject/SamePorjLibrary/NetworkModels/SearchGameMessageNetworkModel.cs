﻿using System.Collections.Generic;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class SearchGameMessageNetworkModel : INetworkModel
    {
        public List<string> Deck;
        public string Hero;
        public SearchGameStatus Status;

        [JsonConstructor]
        public SearchGameMessageNetworkModel(SearchGameStatus status, List<string> deck, string hero)
        {
            Status = status;
            if (status == SearchGameStatus.GoInSearch)
            {
                Deck = deck;
                Hero = hero;
            }
        }
    }

    public enum SearchGameStatus
    {
        GoInSearch,
        GoOutSearch
    }
}