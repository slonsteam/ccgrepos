﻿using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.NetworkModels
{
    public class RegisterMessageNetworkModel : INetworkModel
    {
        public string Login;
        public string Password;
    }
}