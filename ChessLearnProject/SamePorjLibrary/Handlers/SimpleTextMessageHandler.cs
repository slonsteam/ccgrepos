﻿using System;
using SamePorjLibrary.Abstract;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary.Handlers
{
    public class SimpleTextMessageHandler : MessageHandlerBase
    {
        public delegate void HandlingDelegat(string message, IDependencyResolver DI);

        public IDependencyResolver DI;

        public HandlingDelegat Handler = (s, di) => { };

        public SimpleTextMessageHandler(string MessageType, IDependencyResolver DI) : base(MessageType, DI)
        {
            this.DI = DI;
        }

        protected override bool HandleMessage(string message, ICommunicator communicator)
        {
            try
            {
                Handler(message, DI);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}