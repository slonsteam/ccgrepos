﻿using SamePorjLibrary.Abstract;

namespace SamePorjLibrary
{
    public class ChatMessageEncoder : MessageEncoderBase
    {
        public ChatMessageEncoder() : base("ChatMessage")
        {
        }
    }
}