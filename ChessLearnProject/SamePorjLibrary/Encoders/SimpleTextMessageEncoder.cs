﻿using Newtonsoft.Json;
using SamePorjLibrary.Abstract;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace SamePorjLibrary.Encoders
{
    public class SimpleTextMessageEncoder : MessageEncoderBase
    {
        public SimpleTextMessageEncoder(string messageType) : base(messageType)
        {
        }

        public override string EncodeMessage(INetworkModel model)
        {
            string text;
            if (model is SimpleTextNetworkModel)
                text = (model as SimpleTextNetworkModel).Message;
            else
                text = JsonConvert.SerializeObject(model);

            return MessageType + ":" + text;
        }
    }
}