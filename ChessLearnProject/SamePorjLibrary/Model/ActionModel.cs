﻿using System.Collections.Generic;

namespace SamePorjLibrary.Model
{
    public class ActionModel
    {
        public string ActionName;
        public bool Failed;
        public bool IsClosed;
        public Dictionary<string, object> Parametrs;
        public ActionModelStatus Status;

        public ActionModel()
        {
            Parametrs = new Dictionary<string, object>();
            IsClosed = false;
            Failed = false;
        }
    }

    public enum ActionModelStatus
    {
        Start,
        End,
        Midle
    }
}