﻿namespace SamePorjLibrary
{
    public abstract class Figure
    {
        public abstract bool[,] GetTilesForTurn(int x, int y);
        public abstract bool[,] GetTilesForAttack(int x, int y);
    }
}