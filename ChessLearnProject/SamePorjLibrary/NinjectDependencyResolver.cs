﻿using Ninject;
using SamePorjLibrary.Interfaces;

namespace SamePorjLibrary
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        public delegate void CreateBindingsDelegat(IKernel kernel);

        private readonly IKernel _kernel;

        public CreateBindingsDelegat CreateBindings = k => { };

        public NinjectDependencyResolver()
        {
            _kernel = new StandardKernel();
        }

        public T Get<T>(string named)
        {
            return _kernel.Get<T>(named);
        }

        public T Get<T>()
        {
            return _kernel.Get<T>();
        }

        public void InitializeBindings()
        {
            CreateBindings(_kernel);
        }
    }
}