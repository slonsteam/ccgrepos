﻿namespace SamePorjLibrary.Interfaces
{
    public interface IDependencyResolver
    {
        T Get<T>(string name);
        T Get<T>();
    }
}