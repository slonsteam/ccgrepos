﻿namespace SamePorjLibrary.Interfaces
{
    public interface IMessageEncoder
    {
        string EncodeMessage(INetworkModel model);
    }
}