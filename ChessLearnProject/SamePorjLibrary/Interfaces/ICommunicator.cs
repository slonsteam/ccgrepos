﻿using System;

namespace SamePorjLibrary.Interfaces
{
    public interface ICommunicator : IDisposable
    {
        void SendMessage(string text);
        void BeginListenForMessage();
        void EndListenForMessage();
        void CheckCommunicator();
        event EventHandler<MessageReadedEventArgs> MessageReaded;
        event EventHandler ConnectionBroken;
        event EventHandler ConnectionCreated;
        bool Connect(string adres, int port);
        void ConnectAsync(string adres, int port);
        void Disconect();
    }

    public class MessageReadedEventArgs : EventArgs
    {
        public string Message;
    }
}