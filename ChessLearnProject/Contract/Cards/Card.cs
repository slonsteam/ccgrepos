﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contract.Actions;
using Contract.Core;

namespace Contract.Cards
{
    public abstract class Card
    {
        /// <summary>
        ///     Название карты
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Описание карты
        /// </summary>
        public string Description { get; set; }

        public int BaseCost { get; set; }

        /// <summary>
        ///     Стоимость
        /// </summary>
        public int Cost { get; set; }

        public string Group { get; set; }

        /// <summary>
        ///     Урон по умолчанию
        /// </summary>
        public int BaseDamage { get; set; }

        public int CurrentBaseDamage
        {
            get
            {
                var damage = BaseDamage;
                foreach (var buff in Buffs) damage = buff.Damage(damage);
                return damage;
            }
        }

        /// <summary>
        ///     Урон
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        ///     Местонахождение карты
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        ///     Ячейка
        /// </summary>
        public Cell Cell { get; set; }

        public PlayerModel PlayerModel { get; set; }
        public List<Buff> Buffs { get; set; }

        /// <summary>
        ///     Уникальный токен карты
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        ///     Идентификатор карты
        /// </summary>
        public string Identificator { get; protected set; }

        public string ImageSource { get; set; }

        /// <summary>
        ///     Числовой показатель приоритета, который может влиять на атаку или другие способности
        /// </summary>
        public int Priority { get; set; }

        public bool Initialized { get; set; }

        public virtual IEnumerable<Card> GetTargets(Table table)
        {
            return new List<Card>();
        }

        public virtual bool CanBeTargetOf(Card card)
        {
            return true;
        }

        public abstract Card Initialize();

        public virtual Card Clone()
        {
            var clone = (Card) MemberwiseClone();
            return clone;
        }
    }

    public abstract class Unit : Card
    {
        protected Unit()
        {
            Abilities = new Abilities(this);
            Buffs = new List<Buff>();
            Priority = 1;
            ImageSource = "pack://application:,,,/UI/Content/Images/Units/";
        }

        public virtual Abilities Abilities { get; set; }

        public int CurrentBaseHealth
        {
            get
            {
                var health = BaseHealth;
                foreach (var buff in Buffs) health = buff.Health(health);
                return health;
            }
        }

        public int BaseHealth { get; set; }

        public virtual int Health { get; set; }

        public override bool CanBeTargetOf(Card card)
        {
            if (card is Unit) return base.CanBeTargetOf(card) && new Attack(card, this).Validation();

            return base.CanBeTargetOf(card);
        }

        public override IEnumerable<Card> GetTargets(Table table)
        {
            var cardsontable = table.Players.First(x => x.Token != PlayerModel.Token).Cards
                .FromLocation(Location.Table);
            return cardsontable.Where(x => x.CanBeTargetOf(this));
        }

        public override Card Clone()
        {
            var clone = (Unit) MemberwiseClone();
            clone.Buffs = new List<Buff>();
            clone.Abilities = new Abilities(clone);


            return clone;
        }
    }

    public abstract class Spell : Card
    {
        protected BaseAction Action;
        protected Engine Engine;

        public Spell()
        {
            ImageSource = "pack://application:,,,/UI/Content/Images/Spells/";
        }

        public void SetEngine(Engine engine)
        {
            Engine = engine;
        }

        public abstract void Do(List<Card> targets);
    }


    public abstract class Hero : Unit
    {
        public int Defendce { get; set; }
    }

    public class Buff
    {
        public Func<int, int> Damage;
        public Func<int, int> Health;
        public Func<int, int> Mana;

        public Buff(Card card)
        {
            From = card;
        }

        public Card From { get; }
    }
}