﻿using System.Collections.Generic;
using System.Linq;
using Contract.Core;

namespace Contract.Cards
{
    public class CardContainer
    {
        private readonly List<Card> _cards;
        private readonly PlayerModel _player;

        public CardContainer(PlayerModel player)
        {
            _cards = new List<Card>();
            _player = player;
        }

        public IEnumerable<Card> Collection => _cards;

        public List<Card> FromLocation(params Location[] location)
        {
            return _cards.Where(c => location.Contains(c.Location)).ToList();
        }

        public Card Get(string token)
        {
            return _cards.FirstOrDefault(x => x.Token == token);
        }

        public void Add(Card card)
        {
            if (_cards.FirstOrDefault(x => x.Token == card.Token) == null)
            {
                card.PlayerModel = _player;
                _cards.Add(card);
            }
        }

        public bool Remove(Card card)
        {
            return _cards.Remove(card);
        }

        public Card GetRandomCard()
        {
            var deck = FromLocation(Location.InDeck);
            return deck.FirstOrDefault();
        }

        public void Shuffle()
        {
            Shuffle(_cards);
        }

        private void Shuffle<T>(List<T> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                var r = RandomGenerator.RandomNumber(list.Count, i);

                // Swap cards
                var temp = list[i];
                list[i] = list[r];
                list[r] = temp;
            }
        }
    }
}