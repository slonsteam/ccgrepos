﻿using CommonDll.Data;
using Contract.Core;

namespace Contract.Cards
{
    /// <summary>
    ///     Здесь выполняются все операции над картами
    /// </summary>
    public class CardFactory
    {
        private readonly IRepository<Card> _cards;
        private readonly Engine _engine;

        public CardFactory(PlayerModel playerModel)
        {
            _engine = playerModel.Engine;
            _cards = _engine.Cards;
            Player = playerModel;
        }

        public PlayerModel Player { get; }

        private void BaseInit(Card card)
        {
            card.Token = RandomGenerator.RandomString(6);
            card.PlayerModel = Player;
        }

        public Card Create(string id)
        {
            var card = _cards.Get(id).Clone();
            BaseInit(card);
            if (card is Hero hero)
            {
                Initialize(card);
                hero.Location = Location.Table;
                Player.Hero = hero;
            }
            else
            {
                card.Location = Location.InDeck;
            }

            Player.Cards.Add(card);
            return card;
        }

        public Card Initialize(Card card)
        {
            //для карт не из колоды
            if (!card.Initialized)
            {
                if (string.IsNullOrEmpty(card.Token))
                    BaseInit(card);
                card.Initialize();
                if (card is Unit unit)
                    foreach (var act in unit.Abilities.GetAll())
                    {
                        _engine.Emitter.Subscribe(act);
                        act.SetEngine(_engine);
                    }

                if (card is Spell spell)
                    spell.SetEngine(_engine);

                card.Initialized = true;
            }

            Player.Cards.Add(card);
            return card;
        }
    }
}