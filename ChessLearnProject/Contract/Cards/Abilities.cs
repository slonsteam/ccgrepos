﻿using System.Collections.Generic;
using System.Linq;
using Contract.Abilities;
using Contract.Abilities.States;

namespace Contract.Cards
{
    public class Abilities
    {
        private readonly List<Ability> _abilities;
        private readonly Card _card;

        public Abilities(Card card)
        {
            _abilities = new List<Ability>();
            _card = card;
        }

        public void Add(List<Ability> range)
        {
            foreach (var ability in range) Add(ability);
        }

        public void Add(Ability ability)
        {
            ability.Source = _card;
            _abilities.Add(ability);
        }

        /// <summary>
        ///     Получение всех абилок
        /// </summary>
        /// <returns></returns>
        public List<Ability> GetAll()
        {
            return _abilities;
        }

        /// <summary>
        ///     Получить абилку конкретного типа
        /// </summary>
        /// <typeparam name="TAbility">Тип абилки</typeparam>
        /// <returns>Возвращает абилку или null, если её нет</returns>
        public TAbility Get<TAbility>() where TAbility : Ability
        {
            return _abilities.OfType<TAbility>().FirstOrDefault();
        }

        public IEnumerable<State> States()
        {
            return _abilities.OfType<State>();
        }

        public void Remove(Ability ability)
        {
            if (ability != null)
            {
                _abilities.Remove(ability);
                ability.IsActive = false;
            }
        }

        public void Remove<TAbility>() where TAbility : Ability
        {
            Remove(_abilities.OfType<TAbility>().First());
        }
    }
}