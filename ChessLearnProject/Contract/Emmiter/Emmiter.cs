﻿using System.Collections.Generic;
using System.Linq;

namespace Contract.Emmiter
{
    /// <summary>
    ///     Вызов игровых событий и оповещение подписчиков
    /// </summary>
    public class Emmiter
    {
        private readonly List<IListener> _listeners = new List<IListener>();

        /// <summary>
        ///     Подписать слушателя
        /// </summary>
        /// <param name="listener">Слушатель событий</param>
        public void Subscribe(IListener listener)
        {
            _listeners.Add(listener);
        }

        /// <summary>
        ///     Вызвать событие
        /// </summary>
        /// <typeparam name="TEvent">Тип события, которое вызывается</typeparam>
        /// <param name="e">Само событие</param>
        public void Publish<TEvent>(TEvent e)
            where TEvent : IEvent
        {
            //ищем подписчиков, которые наследуются от типа TEvent
            foreach (var l in _listeners.OfType<IListener<TEvent>>()) l.On(e);
        }
    }
}