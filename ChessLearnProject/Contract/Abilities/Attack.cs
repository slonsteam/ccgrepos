﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities
{
    public class Attack : Ability, IListener<Inside<BeginTurn>>, IListener<After<Actions.Attack>>
    {
        private int _quantity;

        public Attack()
        {
            IsActive = false;
        }

        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                if (_quantity == 0) IsActive = false;
            }
        }


        public void On(After<Actions.Attack> ev)
        {
            if (ev.Action.Source.Token == Source.Token)
                Quantity--;
        }

        public void On(Inside<BeginTurn> ev)
        {
            if (ev.Action.CurPlayer == Source.PlayerModel)
                Quantity = 1;
        }
    }
}