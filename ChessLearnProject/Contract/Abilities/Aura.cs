﻿using System.Collections.Generic;
using Contract.Actions;
using Contract.Cards;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities
{
    public class Aura : Ability, IListener<After<DeployCard>>
    {
        private readonly Buff _buff;
        private readonly List<Card> _targets;

        public Aura(Buff buffs, List<Card> targets)
        {
            _buff = buffs;
            _targets = targets;
        }

        public void On(After<DeployCard> ev)
        {
            if (ev.Action.DeployedCard == Source.Token)
                foreach (var target in _targets)
                    Engine.Actions.Delay(new AddBuff(target, _buff));
        }

        public void On(After<Death> ev)
        {
            if (ev.Action.DiedCardToken == Source.Token)
                foreach (var target in _targets)
                    Engine.Actions.Delay(new RemoveBuff(target, _buff));
        }
    }
}