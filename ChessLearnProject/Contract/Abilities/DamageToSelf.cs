﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities
{
    public class DamageToSelf : Ability, IListener<After<CardPlay>>
    {
        public void On(After<CardPlay> ev)
        {
            //(ev.Action.Card as Unit).Health--;
        }
    }
}