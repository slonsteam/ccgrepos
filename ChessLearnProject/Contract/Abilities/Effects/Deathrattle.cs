﻿using System.Collections.Generic;
using Contract.Actions;
using Contract.Core;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.Effects
{
    public class Deathrattle : Ability, IListener<After<Death>>
    {
        private readonly List<BaseAction> _actions;

        public Deathrattle(List<BaseAction> actions)
        {
            _actions = actions;
        }

        public virtual void On(After<Death> ev)
        {
            if (ev.Action.DiedCard.Token == Source.Token)
                foreach (var action in _actions)
                    Engine.Actions.Delay(action);
        }
    }
}