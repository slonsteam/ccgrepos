﻿using System.Collections.Generic;
using Contract.Actions;
using Contract.Core;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.Effects
{
    public class BattleCry : Ability, IListener<Inside<DeployCard>>
    {
        private readonly List<BaseAction> _actions;

        public BattleCry(List<BaseAction> actions)
        {
            _actions = actions;
        }

        public void On(Inside<DeployCard> ev)
        {
            if (ev.Action.DeployedCard == Source.Token)
                foreach (var action in _actions)
                    Engine.Actions.Delay(action);
        }
    }
}