﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities
{
    public class Damage : Ability, IListener<Inside<CardPlay>>
    {
        public void On(Inside<CardPlay> ev)
        {
            if (Source == ev.Action.PlayedCard)
            {
            }
        }
    }
}