﻿using Contract.Cards;
using Contract.Core;
using Contract.Emmiter;

namespace Contract.Abilities
{
    /// <summary>
    ///     Способности карты(юнита)
    /// </summary>
    public abstract class Ability : IListener
    {
        protected Engine Engine;
        public Card Source;

        protected Ability()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public void SetEngine(Engine engine)
        {
            Engine = engine;
        }
    }
}