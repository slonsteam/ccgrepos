﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.States
{
    public class DivineShield : State, IListener<Before<CreateDamage>>
    {
        public DivineShield()
        {
            Identificator = nameof(DivineShield);
        }


        public void On(Before<CreateDamage> ev)
        {
            if (ev.Action.Target.Token == Source.Token) ev.Action.Quntity = 0;
        }
    }
}