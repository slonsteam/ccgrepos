﻿using Contract.Actions;
using Contract.Cards;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.States
{
    public class WindRage : State, IListener<After<BeginTurn>>
    {
        public WindRage()
        {
            Identificator = nameof(WindRage);
        }

        public void On(After<BeginTurn> ev)
        {
            if (ev.Action.CurPlayer.Token == Source.PlayerModel.Token)
                (Source as Unit).Abilities.Get<Attack>().Quantity = 2;
        }
    }
}