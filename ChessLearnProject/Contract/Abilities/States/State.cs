﻿namespace Contract.Abilities.States
{
    public abstract class State : Ability
    {
        public string Identificator { get; set; }
        public string ImageSource { get; set; }
    }
}