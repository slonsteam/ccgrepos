﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.States
{
    public class Сamouflage : State, IListener<After<DeployCard>>, IListener<After<Actions.Attack>>
    {
        public Сamouflage()
        {
            Identificator = nameof(Сamouflage);
        }

        public void On(After<Actions.Attack> ev)
        {
            if (ev.Action.Source.Token == Source.Token) ev.Action.Source.Priority = 1;
        }

        public void On(After<DeployCard> ev)
        {
            if (ev.Action.Card.Token == Source.Token) ev.Action.Card.Priority = 0;
        }
    }
}