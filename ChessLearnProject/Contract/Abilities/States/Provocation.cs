﻿using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Abilities.States
{
    /// <summary>
    ///     Способность - провокация. Меняет приоритет атаки у вражеских юнитов
    /// </summary>
    public class Provocation : State, IListener<After<DeployCard>>
    {
        public Provocation()
        {
            Identificator = "PROVO";
        }

        public void On(After<DeployCard> ev)
        {
            if (ev.Action.Card.Token == Source.Token) ev.Action.Card.Priority = 2;
        }
    }
}