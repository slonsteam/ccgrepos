﻿namespace Contract.Core
{
    public class Settings
    {
        public Settings(int turnTime = 2, int decksize = 10, int startPack = 4, int maxCardsinHand = 5)
        {
            TurnTime = turnTime;
            DeckSize = decksize;
            StartPack = startPack;
            MaxCardInHand = maxCardsinHand;
        }

        public int TurnTime { get; }
        public int DeckSize { get; }
        public int StartPack { get; }
        public int MaxCardInHand { get; }
    }
}