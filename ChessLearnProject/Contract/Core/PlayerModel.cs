﻿using System.Collections.Generic;
using System.Linq;
using Contract.Cards;

namespace Contract.Core
{
    public class PlayerModel
    {
        private int _maxmana;

        public PlayerModel(Engine engine, string token = null)
        {
            MaxMana = Mana = 0;
            Engine = engine;
            CardFactory = new CardFactory(this);
            Cards = new CardContainer(this);
            Timer = new GameTimer(this);
            Token = token ?? RandomGenerator.RandomString(4);
            StartDeck = new List<Card>();
        }

        public Engine Engine { get; }
        public Hero Hero { get; set; }

        public int MaxMana
        {
            get => _maxmana;
            set
            {
                _maxmana = value;
                if (_maxmana > 10)
                    _maxmana = 10;
            }
        }

        public string Token { get; }
        public GameTimer Timer { get; set; }
        public List<Cell> Cells => Engine.Table.Cells.Where(x => x.Player == this).ToList();
        public CardFactory CardFactory { get; set; }
        public int Mana { get; set; }
        public CardContainer Cards { get; set; }
        public bool IsMyTurn { get; set; }
        public List<Card> StartDeck { get; }

        public bool IsTurnOwner()
        {
            return Engine.Table.Queue == Engine.Table.Players.IndexOf(this);
        }
    }
}