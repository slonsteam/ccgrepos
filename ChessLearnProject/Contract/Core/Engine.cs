﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDll.Data;
using Contract.Actions;
using Contract.Cards;

namespace Contract.Core
{
    //игровое ядро
    public class Engine : IEngine
    {
        public readonly Manager Actions;
        public readonly IRepository<Card> Cards;

        public readonly Emmiter.Emmiter Emitter;
        public readonly IActionLogger Logger;
        public readonly Settings Settings;
        public readonly Table Table;

        public Engine(IRepository<Card> cards, IActionLogger logger, Settings settings = null)
        {
            Emitter = new Emmiter.Emmiter();
            Actions = new Manager(this);
            Table = new Table();
            Cards = cards;
            Logger = logger;
            Settings = settings ?? new Settings(120);
        }

        //todo костыль
        public List<Card> Targets { get; private set; }

        public string StartGame(Dictionary<string, List<string>> playerdecks)
        {
            Actions.Launch(new StartGame(playerdecks));
            return Logger?.GetMessage();
        }

        public void EndTurn(string token)
        {
            var player = Table.Players.First(x => x.Token == token);
            Actions.Launch(new EndTurn(player));
            Actions.Launch(new BeginTurn());
            EndTurnEvent?.Invoke(null, Logger?.GetMessage());
        }

        public void EndGame(string token)
        {
            var player = Table.Players.First(x => x.Token == token);
            Actions.Delay(new EndGame(player));
            EndGameEvent?.Invoke(null, Logger?.GetMessage());
        }

        public string ContinueGame(string token)
        {
            Actions.Delay(new GetTable(Table.Players.First(x => x.Token == token)));
            return Logger?.GetMessage();
        }

        public string ContinueGame()
        {
            return "";
        }

        public event EventHandler<string> EndGameEvent;

        public string PlayCard(string playertoken, string token, List<string> targets)
        {
            var player = Table.Players.First(x => x.Token == playertoken);
            var card = player.Cards.Get(token);
            var targetsl = new List<Card>();
            //todo подумать над тем, где должны хранится все карты на столе
            if (targets != null)
                foreach (var target in targets)
                {
                    var targetcard = Table.Cards.FirstOrDefault(x => x.Token == target);
                    if (targetcard != null)
                        targetsl.Add(targetcard);
                }

            Targets = targetsl;
            Actions.Launch(new CardPlay(card, player, targetsl));
            Actions.Launch(new CanBePlayed(player));
            return Logger?.GetMessage();
        }

        public List<string> GetTargets(string cardtoken)
        {
            var card = Table.Cards.FirstOrDefault(x => x.Token == cardtoken);
            return card.GetTargets(Table).Select(x => x.Token).ToList();
        }

        public event EventHandler<string> EndTurnEvent;

        public string EndGame()
        {
            return "";
        }
    }
}