﻿using Contract.Cards;

namespace Contract.Core
{
    public class Cell
    {
        public Cell(Card card, PlayerModel player)
        {
            Card = card;
            Player = player;
            card.Cell = this;
        }

        public int Position { get; set; }
        public Card Card { get; }
        public PlayerModel Player { get; set; }
    }
}