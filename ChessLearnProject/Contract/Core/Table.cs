﻿using System.Collections.Generic;
using Contract.Cards;

namespace Contract.Core
{
    public class Table
    {
        private int _queue;

        public Table()
        {
            Players = new List<PlayerModel>();
            Cells = new List<Cell>();
        }

        public IEnumerable<Card> Cards
        {
            get
            {
                var cards = new List<Card>();
                Players.ForEach(x => { cards.AddRange(x.Cards.Collection); });
                Players.ForEach(x => cards.Add(x.Hero));
                return cards;
            }
        }

        public List<Cell> Cells { get; }
        public List<PlayerModel> Players { get; set; }

        public int Queue
        {
            get => _queue;
            set
            {
                _queue = value;
                if (_queue >= Players.Count)
                    _queue = 0;
            }
        }
    }
}