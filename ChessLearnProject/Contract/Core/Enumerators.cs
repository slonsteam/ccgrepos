﻿namespace Contract.Core
{
    public enum Location
    {
        InDeck,
        OutDeck,
        Table,
        InHand,
        Death,
        Core
    }
}