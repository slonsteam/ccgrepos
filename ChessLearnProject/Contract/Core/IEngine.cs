﻿using System;
using System.Collections.Generic;

namespace Contract.Core
{
    public interface IEngine
    {
        /// <summary>
        ///     Старт новой игры
        /// </summary>
        /// <param name="decks">Первый элемент - герой, все последующие карты</param>
        /// <returns></returns>
        string StartGame(Dictionary<string, List<string>> decks);

        /// <summary>
        /// </summary>
        /// <param name="token"></param>
        void EndTurn(string token);

        void EndGame(string token);
        string ContinueGame(string token);
        string ContinueGame();
        event EventHandler<string> EndTurnEvent;
        event EventHandler<string> EndGameEvent;
        string PlayCard(string playertoken, string id, List<string> targets);
        List<string> GetTargets(string cardtoken);
        string EndGame();
    }
}