﻿using System.Collections.Generic;

namespace Contract.Core
{
    public interface IActionLogger
    {
        void StartAction(string name);
        void EndAction(string name);
        void ActionFailed(string name);
        void SetActionParameters(string name, Dictionary<string, object> parameters);
        string GetMessage();
    }
}