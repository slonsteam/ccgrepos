﻿using System;
using System.Timers;
using Contract.Actions;
using Contract.Emmiter;
using TestGameLogic.Events;

namespace Contract.Core
{
    public class GameTimer : IListener<After<BeginTurn>>, IListener<After<EndTurn>>, IListener<After<EndGame>>
    {
        private readonly Engine _engine;
        private readonly PlayerModel _playerModel;
        private readonly Timer _timer;

        public GameTimer(PlayerModel player)
        {
            _playerModel = player;
            _engine = player.Engine;
            _engine.Emitter.Subscribe(this);
            _timer = new Timer(1000); //miliseconds
            _timer.Elapsed += Tick;
            TurnTimeLeft = _engine.Settings.TurnTime * 60;
        }

        private int TurnTimeLeft { get; set; }


        public void On(After<BeginTurn> ev)
        {
            if (_playerModel.IsTurnOwner())
            {
                TurnTimeLeft = _engine.Settings.TurnTime * 60;
                _timer.Start();
            }
        }

        public void On(After<EndGame> ev)
        {
            _timer.Stop();
            _timer.Dispose();
        }

        public void On(After<EndTurn> ev)
        {
            if (_playerModel.IsTurnOwner())
            {
                _timer.Stop();
                TurnTimeLeft = _engine.Settings.TurnTime * 60;
            }
        }

        public void Tick(object sender, EventArgs e)
        {
            TurnTimeLeft--;
            if (TurnTimeLeft == 0) _engine.EndTurn(_playerModel.Token);
        }
    }
}