﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Contract.Core
{
    /// <summary>
    ///     Управление всеми действиями и событиями в игре
    /// </summary>
    public class Manager
    {
        private readonly List<BaseAction> _activeActions = new List<BaseAction>();
        private readonly List<BaseAction> _delayedActions = new List<BaseAction>();
        private readonly Engine _engine;

        public Manager(Engine engine)
        {
            _engine = engine;
            Emitter = engine.Emitter;
        }

        public Emmiter.Emmiter Emitter { get; }

        public void Delay<TAction>(TAction action)
            where TAction : BaseAction
        {
            if (IsIdle())
                Launch(action);
            else
                _delayedActions.Add(action);
        }

        //здесь инициализируем экшн, для срабатывания эмиттера
        internal void Register(BaseAction action)
        {
            action.SetActionManager(this);
            action.SetEngine(_engine);
        }

        public bool Launch<TAction>(TAction action)
            where TAction : BaseAction
        {
            if (IsIdle()) return ForceLaunch(action);
            throw new Exception("Actions are running now");
        }

        private bool ForceLaunch<TAction>(TAction action)
            where TAction : BaseAction
        {
            Register(action);
            Debug.WriteLine($"Start({action.GetType().Name})");
            _engine.Logger?.StartAction(action.GetType().Name);
            action.Validate();

            if (action.IsValid())
            {
                _activeActions.Add(action);
                LaunchRec(action);
                return true;
            }

            Debug.WriteLine($"Failre({action.GetType().Name})");

            action.NotifyFailure(Emitter);
            _engine.Logger?.ActionFailed(action.GetType().Name);

            return false;
        }

        public bool IsIdle()
        {
            return _activeActions.Count == 0;
        }

        private void LaunchRec<TAction>(TAction action)
            where TAction : BaseAction
        {
            action.NotifyBefore(Emitter);


            action.Configure();

            _engine.Logger?.SetActionParameters(action.GetType().Name, action.GetParameters());


            action.NotifyInside(Emitter);

            action.Close();

            foreach (var child in action.Children) ForceLaunch(child);

            action.Complete();
            Debug.WriteLine($"End({action.GetType().Name})");

            _engine.Logger?.EndAction(action.GetType().Name);


            action.NotifyAfter(Emitter);

            _activeActions.Remove(action);

            CheckFinish(action);
        }

        private void CheckFinish<TAction>(TAction action)
            where TAction : BaseAction
        {
            if (!IsIdle()) return;
            if (_delayedActions.Count > 0)
            {
                var first = _delayedActions[0];
                _delayedActions.Remove(first);
                Launch(first);
            }
            else
            {
                action.NotifyFinish(Emitter);
            }
        }
    }
}