﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contract.Cards;
using TestGameLogic.Events;

namespace Contract.Core
{
    public interface IAction
    {
        Func<object, bool> CanExecute { get; set; }
        void Execute(object param);
    }

    /// <summary>
    ///     Базовый класс всех действий
    /// </summary>
    public abstract class BaseAction : IAction
    {
        private bool _isClosed;
        private Manager _manager;

        private BaseAction _parent;
        private bool _status;
        protected Engine Engine;
        public Card Card { get; set; }
        public BaseAction Parent { get; set; }

        public List<BaseAction> Children { get; } = new List<BaseAction>();

        public Func<object, bool> CanExecute { get; set; } = t => true;

        public virtual void Execute(object param)
        {
        }

        internal abstract void NotifyBefore(Emmiter.Emmiter emitter);
        internal abstract void NotifyInside(Emmiter.Emmiter emitter);
        internal abstract void NotifyAfter(Emmiter.Emmiter emitter);
        internal abstract void NotifyFailure(Emmiter.Emmiter emitter);
        internal abstract void NotifyFinish(Emmiter.Emmiter emitter);

        internal void SetActionManager(Manager manager)
        {
            _manager = manager;
        }

        /// <summary>
        ///     Добавить действие, которое выполнится после текущего
        /// </summary>
        /// <param name="action">Действие, которое требуется выполнить</param>
        /// <returns></returns>
        public BaseAction AddChild(BaseAction action)
        {
            if (_isClosed) throw new Exception("Cannot add child action to closed action");

            Children.Add(action);
            action.SetParent(this);
            _manager.Register(action);
            return this;
        }

        internal void Close()
        {
            _isClosed = true;
        }

        internal void SetEngine(Engine engine)
        {
            Engine = engine;
        }

        private void SetParent(BaseAction action)
        {
            if (_parent != null) throw new Exception("Parent is already set");

            _parent = action;
        }

        public BaseAction AddChildren(IEnumerable<BaseAction> actions)
        {
            foreach (var action in actions) AddChild(action);
            return this;
        }

        /// <summary>
        ///     Провека, может ли действие быть выполнено
        /// </summary>
        /// <returns></returns>
        public virtual bool Validation()
        {
            return true;
        }

        internal bool IsValid()
        {
            return _status;
        }

        internal void Validate()
        {
            _status = Validation();
        }

        public virtual void Complete()
        {
        }

        /// <summary>
        ///     Выполнение действия
        /// </summary>
        public virtual void Configure()
        {
        }

        public Dictionary<string, object> GetParameters()
        {
            var parameters = new Dictionary<string, object>();
            var props = GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(ParameterAttribute)));
            foreach (var prop in props)
            {
                var attributes = prop.GetCustomAttributes(true);
                var name = (attributes.First() as ParameterAttribute).Name;
                parameters.Add(name, prop.GetValue(this));
            }

            return parameters;
        }
    }

    public abstract class BaseAction<TAction> : BaseAction
        where TAction : BaseAction<TAction>
    {
        internal override void NotifyBefore(Emmiter.Emmiter emitter)
        {
            emitter.Publish(new Before<TAction>((TAction) this));
            //emitter.Publish(new Before<BaseAction>(this));
        }

        internal override void NotifyInside(Emmiter.Emmiter emitter)
        {
            emitter.Publish(new Inside<TAction>((TAction) this));
            //emitter.Publish(new Inside<BaseAction>(this));
        }

        internal override void NotifyAfter(Emmiter.Emmiter emitter)
        {
            emitter.Publish(new After<TAction>((TAction) this));
            //emitter.Publish(new After<BaseAction>(this));
        }

        internal override void NotifyFailure(Emmiter.Emmiter emitter)
        {
            emitter.Publish(new Failure<TAction>((TAction) this));
            //emitter.Publish(new Failure<BaseAction>(this));
        }

        internal override void NotifyFinish(Emmiter.Emmiter emitter)
        {
            emitter.Publish(new Finish<TAction>((TAction) this));
            //emitter.Publish(new Finish<BaseAction>(this));
        }
    }
}