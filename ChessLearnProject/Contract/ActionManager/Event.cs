﻿using Contract.Core;
using Contract.Emmiter;

namespace SimpleContract.Events
{
    public abstract class Event<TEvent, TAction> : IEvent
        where TEvent : Event<TEvent, TAction>
        where TAction : BaseAction
    {
        public readonly TAction Action;

        public Event(TAction action)
        {
            Action = action;
        }
    }
}