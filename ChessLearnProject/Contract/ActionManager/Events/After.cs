﻿using Contract.Core;
using SimpleContract.Events;

namespace TestGameLogic.Events
{
    public class After<TAction> : Event<After<TAction>, TAction>
        where TAction : BaseAction
    {
        public After(TAction action) : base(action)
        {
        }
    }
}