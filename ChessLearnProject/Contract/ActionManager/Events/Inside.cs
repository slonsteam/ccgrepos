﻿using Contract.Core;
using SimpleContract.Events;

namespace TestGameLogic.Events
{
    public class Inside<TAction> : Event<Inside<TAction>, TAction>
        where TAction : BaseAction
    {
        public Inside(TAction action) : base(action)
        {
        }
    }
}