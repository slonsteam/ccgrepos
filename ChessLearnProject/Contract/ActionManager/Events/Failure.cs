﻿using Contract.Core;
using SimpleContract.Events;

namespace TestGameLogic.Events
{
    public class Failure<TAction> : Event<Failure<TAction>, TAction>
        where TAction : BaseAction
    {
        public Failure(TAction action) : base(action)
        {
        }
    }
}