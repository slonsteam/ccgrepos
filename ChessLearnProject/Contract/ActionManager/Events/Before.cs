﻿using Contract.Core;
using SimpleContract.Events;

namespace TestGameLogic.Events
{
    public class Before<TAction> : Event<Before<TAction>, TAction>
        where TAction : BaseAction
    {
        public Before(TAction action) : base(action)
        {
        }
    }
}