﻿using Contract.Core;
using SimpleContract.Events;

namespace TestGameLogic.Events
{
    public class Finish<TAction> : Event<Finish<TAction>, TAction>
        where TAction : BaseAction
    {
        public Finish(TAction action) : base(action)
        {
        }
    }
}