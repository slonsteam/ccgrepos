﻿using System.Linq;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class Attack : BaseAction<Attack>
    {
        public Attack(Card source, Card target)
        {
            Source = source;
            Target = target;
        }

        [Parameter("Source")] public string SourceToken => Source.Token;
        [Parameter("Target")] public string TargetToken => Target.Token;
        public Card Source { get; }

        public Card Target { get; }

        public override void Configure()
        {
            AddChild(new CreateDamage(Source.Damage, Target));
            if (!(Target is Hero))
                AddChild(new CounterAttack(this));
        }

        public override bool Validation()
        {
            Abilities.Attack attack = null;
            if (Source is Unit unit)
                attack = unit.Abilities.Get<Abilities.Attack>();

            return Source.PlayerModel.IsTurnOwner() &&
                   attack != null &&
                   Source.Location == Location.Table &&
                   Target.Location == Location.Table &&
                   attack.Quantity > 0 &&
                   Target.Priority > 0 &&
                   //todo подумать как исправить
                   (!Target.PlayerModel.Cards.FromLocation(Location.Table) // есть провокация 
                        .Any(x => x.Priority > 1) || Target.Priority >= 2);
        }
    }
}