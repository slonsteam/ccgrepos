﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class RemoveCard : BaseAction<RemoveCard>
    {
        public RemoveCard(Card card, Location location = Location.OutDeck)
        {
            Card = card;
            Location = location;
        }

        [Parameter("Card")] public string CardToken => Card.Token;
        [Parameter("Player")] public string Player => Card.PlayerModel.Token;
        public Card Card { get; }

        public Location Location { get; }

        public override void Configure()
        {
            Card.Location = Location;
            if (Location != Location.Table)
                Card.PlayerModel.Cards.Remove(Card);
        }

        public override bool Validation()
        {
            return base.Validation();
        }
    }
}