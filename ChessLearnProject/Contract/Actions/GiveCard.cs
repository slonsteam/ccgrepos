﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    //todo: Написать тесты

    public class GiveCard : BaseAction<GiveCard>
    {
        public GiveCard(PlayerModel player, Card card = null)
        {
            Player = player;
            CardGiven = card;
        }

        [Parameter("Player")] public string PlayerToken => Player.Token;
        [Parameter("Card")] public string CardGivenToken => CardGiven.Token;

        public PlayerModel Player { get; }

        public Card CardGiven { get; private set; }

        public override void Configure()
        {
            if (CardGiven == null)
            {
                var card = Player.Cards.GetRandomCard();
                CardGiven = card;
            }

            CardGiven.Location = Location.InHand;
            if (Player.Cards.FromLocation(Location.InHand).Count > Engine.Settings.MaxCardInHand)
                AddChild(new RemoveCard(CardGiven));
        }

        public override bool Validation()
        {
            return Player.Cards.FromLocation(Location.InDeck).Count > 0 || CardGiven != null;
        }
    }
}