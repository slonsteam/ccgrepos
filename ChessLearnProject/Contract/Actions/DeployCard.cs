﻿using System.Collections.Generic;
using System.Linq;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class DeployCard : BaseAction<DeployCard>
    {
        private readonly Card _card;
        private readonly PlayerModel _player;

        public DeployCard(PlayerModel player, Card card, List<Card> targets = null)
        {
            _player = player;
            Card = _card = card;
            Targets = targets;
        }

        [Parameter("Player")] public string Player => _player.Token;
        [Parameter("Card")] public string DeployedCard => _card.Token;

        [Parameter("Identificator")] public string Identificator => _card.Identificator;

        //переделать
        [Parameter("States")]
        public string States => string.Join(",", (_card as Unit).Abilities.States().Select(x => x.Identificator));

        public List<Card> Targets { get; }

        public override void Configure()
        {
            _player.CardFactory.Initialize(_card);
            switch (_card.Location)
            {
                case Location.Core:
                {
                    _card.Location = Location.Table;
                    break;
                }
                case Location.InHand:
                {
                    AddChild(new PayResources(_player, _card));
                    AddChild(new RemoveCard(_card, Location.Table));
                    break;
                }
            }
        }

        public override bool Validation()
        {
            return _card.Location == Location.InHand &&
                   _card.Cost <= _player.Mana ||
                   _card.Location == Location.Core;
        }
    }
}