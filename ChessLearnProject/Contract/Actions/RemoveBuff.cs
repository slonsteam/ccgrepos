﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class RemoveBuff : BaseAction<RemoveBuff>
    {
        private readonly Buff _buff;
        private readonly Card _card;

        public RemoveBuff(Card card, Buff buff)
        {
            _card = card;
            _buff = buff;
        }

        [Parameter("Health")] public int Health { get; private set; }

        [Parameter("Damage")] public int Damage { get; private set; }

        [Parameter("Card")] public string CardToken => _card.Token;

        public override void Configure()
        {
            if (_card is Unit unit)
            {
                var healthbefore = unit.CurrentBaseHealth;
                var damagebefore = unit.CurrentBaseDamage;
                _card.Buffs.Remove(_buff);
                Health = healthbefore - unit.CurrentBaseHealth;
                Damage = damagebefore - unit.CurrentBaseDamage;
                unit.Damage -= Damage;
                unit.Health -= Health;
            }
        }
    }
}