﻿using Contract.Core;

namespace Contract.Actions
{
    //todo: Написать тесты
    /// <summary>
    ///     Начало хода игрока
    /// </summary>
    public class BeginTurn : BaseAction<BeginTurn>
    {
        public PlayerModel CurPlayer { get; private set; }

        [Parameter("Player")] public string PlayerToken => CurPlayer.Token;

        public override void Configure()
        {
            var curplayer = Engine.Table.Players[Engine.Table.Queue];
            CurPlayer = curplayer;
            //todo костыль с восстановлением маны 
            //curplayer.Mana = curplayer.MaxMana;
            AddChild(new GiveMana(curplayer));
            AddChild(new GiveMana(curplayer, staticmana: false, quantity: curplayer.MaxMana + 1 - curplayer.Mana));
            AddChild(new GiveCard(curplayer));
            AddChild(new CanBePlayed(CurPlayer));
        }
    }
}