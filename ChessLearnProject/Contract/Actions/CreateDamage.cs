﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class CreateDamage : BaseAction<CreateDamage>
    {
        public CreateDamage(int quantity, Card target)
        {
            Target = target;
            Quntity = quantity;
        }

        [Parameter("Target")] public string TargetToken => Target.Token;

        public Card Target { get; set; }

        [Parameter("Quantity")] public int Quntity { get; set; }

        public override void Configure()
        {
            (Target as Unit).Health -= Quntity;
            if ((Target as Unit).Health <= 0)
                AddChild(new Death(Target));
        }

        public override bool Validation()
        {
            return Target != null && Quntity != 0;
        }
    }
}