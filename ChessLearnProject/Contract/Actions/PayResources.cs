﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class PayResources : BaseAction<PayResources>
    {
        public PayResources(PlayerModel player, Card card)
        {
            _card = card;
            _player = player;
        }

        [Parameter("Quantity")] public int Quantity => _card.Cost;
        [Parameter("Player")] public string PlayerToken => _player.Token;
        public Card ForCard => _card;

        private Card _card { get; }
        public PlayerModel _player { get; }

        public override void Configure()
        {
            _player.Mana -= _card.Cost;
        }
    }
}