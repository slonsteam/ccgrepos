﻿using Contract.Abilities.States;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class AddState : BaseAction<AddState>
    {
        private readonly State _state;
        private readonly Unit _unit;

        public AddState(Unit unit, State state)
        {
            _unit = unit;
            _state = state;
        }

        public override void Configure()
        {
            _unit.Abilities.Add(_state);
        }

        public override bool Validation()
        {
            return true;
        }
    }
}