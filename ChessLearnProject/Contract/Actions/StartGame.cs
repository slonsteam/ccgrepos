﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contract.Core;

namespace Contract.Actions
{
    public class StartGame : BaseAction<StartGame>
    {
        private readonly Dictionary<string, List<string>> _decks;


        public StartGame(Dictionary<string, List<string>> decks)
        {
            _decks = decks;
            CardTokens = new Dictionary<string, List<string>>();
        }

        [Parameter("CardTokens")] public Dictionary<string, List<string>> CardTokens { get; }

        [Parameter("Queue")] public string Queue { get; private set; }

        //todo исправить костыль
        [Parameter("EnemyHero")]
        public string EnemyHero => Queue == Engine.Table.Players[0].Token
            ? Engine.Table.Players[0].Hero.Identificator
            : Engine.Table.Players[1].Hero.Identificator;

        public override void Configure()
        {
            foreach (var pair in _decks)
            {
                CardTokens.Add(pair.Key, new List<string>());
                var deck = pair.Value.Skip(1);
                var hero = pair.Value[0];
                var player = new PlayerModel(Engine, pair.Key);

                var herom = player.CardFactory.Create(hero);

                CardTokens[pair.Key].Add(herom.Token);

                foreach (var cardid in deck)
                {
                    var card = player.CardFactory.Create(cardid);
                    CardTokens[pair.Key].Add(card.Token);
                }

                //создаем игроков с колодами
                player.Cards.Shuffle();
                Engine.Table.Players.Add(player);
            }

            var r = new Random();
            //рандомим первый ход
            Engine.Table.Queue = r.Next(Engine.Table.Players.Count);
            Queue = Engine.Table.Players[Engine.Table.Queue].Token;
            //выдаем карты
            for (var i = 1; i <= Engine.Settings.StartPack; i++)
            {
                AddChild(new GiveCard(Engine.Table.Players[0]));
                AddChild(new GiveCard(Engine.Table.Players[1]));
            }

            //начинаем первый ход
            AddChild(new BeginTurn());
        }

        public override bool Validation()
        {
            //todo проверить что первая карта это герой - некритично
            return _decks.Count == 2 &&
                   _decks.Values.First().Count == Engine.Settings.DeckSize + 1 &&
                   _decks.Values.Last().Count == _decks.Values.First().Count;
        }
    }
}