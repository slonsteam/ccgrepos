﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    //todo написать тест
    public class Death : BaseAction<Death>
    {
        public Death(Card card)
        {
            DiedCard = card;
        }

        [Parameter("Card")] public string DiedCardToken => DiedCard.Token;
        [Parameter("Player")] public string PlayerTOken => DiedCard.PlayerModel.Token;
        public Card DiedCard { get; }

        public override void Configure()
        {
            if (DiedCard is Hero)
                Engine.EndGame(DiedCard.PlayerModel.Token);
            else AddChild(new RemoveCard(DiedCard, Location.Death));
        }
    }
}