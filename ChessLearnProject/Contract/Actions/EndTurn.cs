﻿using Contract.Core;

namespace Contract.Actions
{
    public class EndTurn : BaseAction<EndTurn>
    {
        public EndTurn(PlayerModel player)
        {
            Player = player;
        }

        [Parameter("Player")] public string PlayerToken => Player.Token;
        public PlayerModel Player { get; }

        public override void Configure()
        {
            Engine.Table.Queue++;
        }

        public override bool Validation()
        {
            return Player.IsTurnOwner();
        }
    }
}