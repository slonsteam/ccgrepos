﻿using System.Linq;
using Contract.Core;

namespace Contract.Actions
{
    public class EndGame : BaseAction<EndGame>
    {
        private PlayerModel _first;
        private readonly PlayerModel _loser;
        private PlayerModel _second;
        private PlayerModel _winner;

        public EndGame(PlayerModel loser = null)
        {
            _loser = loser;
        }

        [Parameter("Winner")] public string Winner => _winner != null ? _winner.Token : "draw";

        public override void Configure()
        {
            _winner = Engine.Table.Players.FirstOrDefault(x => x.Token != _loser.Token);
        }
    }
}