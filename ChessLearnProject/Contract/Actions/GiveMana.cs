﻿using Contract.Core;

namespace Contract.Actions
{
    public class GiveMana : BaseAction<GiveMana>
    {
        public GiveMana(PlayerModel player, int quantity = 1, bool staticmana = true)
        {
            _staticmana = staticmana;
            Player = player;
            _quantity = quantity;
        }

        [Parameter("Player")] public string PlayerToken => Player.Token;
        [Parameter("Quantity")] public int Quantity => _quantity;
        [Parameter("Staticmana")] public bool StaticMana => _staticmana;

        public PlayerModel Player { get; }

        private int _quantity { get; }
        private bool _staticmana { get; }

        public override void Configure()
        {
            if (_staticmana)
                Player.MaxMana += _quantity;
            else
                Player.Mana += _quantity;
        }
    }
}