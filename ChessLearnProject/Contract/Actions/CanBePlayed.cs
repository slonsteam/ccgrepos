﻿using System.Collections.Generic;
using System.Linq;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class CanBePlayed : BaseAction<CanBePlayed>
    {
        private IEnumerable<Card> _canBePlayedCards;
        private readonly PlayerModel _playerModel;

        public CanBePlayed(PlayerModel playerModel)
        {
            _playerModel = playerModel;
        }

        [Parameter("CanBePlayed")]
        public List<string> CanBePlayedCards => _canBePlayedCards.Select(x => x.Token).ToList();

        public override void Configure()
        {
            _canBePlayedCards = _playerModel.Cards.FromLocation(Location.Table)
                .Where(x => x.GetTargets(Engine.Table).Any());
            _canBePlayedCards = _canBePlayedCards.Concat(_playerModel.Cards.FromLocation(Location.InHand)
                .Where(x => x.Cost <= _playerModel.Mana));
        }
    }
}