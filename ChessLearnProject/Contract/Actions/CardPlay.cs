﻿using System.Collections.Generic;
using System.Linq;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class CardPlay : BaseAction<CardPlay>
    {
        public CardPlay(Card card, PlayerModel player, List<Card> targets)
        {
            PlayedCard = card;
            _player = player;
            Targets = targets;
        }

        [Parameter("Player")] public string PlayerToken => _player.Token;
        [Parameter("Card")] public string PlayedCardToken => PlayedCard.Token;

        public List<Card> Targets { get; }

        public PlayerModel Player => _player;
        private PlayerModel _player { get; }
        public Card PlayedCard { get; }

        public override void Configure()
        {
            switch (PlayedCard.Location)
            {
                case Location.Table:
                    //todo исправить
                    if (Targets.Count > 0)
                        AddChild(new Attack(PlayedCard, Targets.First()));
                    break;
                case Location.InHand:
                {
                    if (PlayedCard is Unit)
                        AddChild(new DeployCard(_player, PlayedCard, Targets));

                    if (PlayedCard is Spell spell)
                    {
                        _player.CardFactory.Initialize(PlayedCard);
                        spell.Do(Targets);
                        AddChild(new RemoveCard(PlayedCard));
                        AddChild(new PayResources(Player, PlayedCard));
                    }

                    break;
                }
            }

            AddChild(new CanBePlayed(_player));
        }

        public override bool Validation()
        {
            return PlayedCard != null;
        }
    }
}