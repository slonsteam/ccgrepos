﻿using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class CounterAttack : BaseAction<CounterAttack>
    {
        private readonly Attack _attack;

        public CounterAttack(Attack attack)
        {
            _attack = attack;
        }


        public override void Configure()
        {
            AddChild(new CreateDamage(_attack.Target.Damage, _attack.Source));
        }

        public override bool Validation()
        {
            return (_attack.Target as Unit)?.Abilities.Get<Abilities.CounterAttack>() != null;
        }
    }
}