﻿using System.Collections.Generic;
using System.Linq;
using Contract.Cards;
using Contract.Core;

namespace Contract.Actions
{
    public class GetTable : BaseAction<GetTable>
    {
        private PlayerModel _enemyplayer;
        private readonly PlayerModel _player;

        public GetTable(PlayerModel player)
        {
            _player = player;
        }

        [Parameter("EnemyCardsTable")] public object EnemyCardsTable { get; set; }

        //доделать только одному клиенту
        [Parameter("CardsInDeck")]
        public object CardsInDeck =>
            _player.Cards.FromLocation(Location.InDeck).Select(c => new {c.Token, c.Identificator});

        [Parameter("CardsInDeckEnemy")]
        public List<string> CardsInDeckEnemy =>
            _enemyplayer.Cards.FromLocation(Location.InDeck).Select(c => c.Token).ToList();

        [Parameter("CardsHandEnemy")]
        public List<string> EnemyCards =>
            _enemyplayer.Cards.FromLocation(Location.InHand).Select(x => x.Token).ToList();

        [Parameter("CardsHand")] public object Cards { get; set; }

        [Parameter("CardsTable")] public object CardsTable { get; set; }

        [Parameter("Player")] public string Player => _player.Token;
        [Parameter("EnemyPlayer")] public string EnemyPlayer => _player.Token;

        public override void Configure()
        {
            _enemyplayer = Engine.Table.Players.First(x => x.Token != _player.Token);
            EnemyCardsTable = _enemyplayer.Cards.FromLocation(Location.Table).Select(x => new
            {
                x.Identificator,
                x.Token,
                (x as Unit).BaseHealth,
                (x as Unit).Health,
                x.Damage,
                x.BaseDamage
            }).ToList();
            CardsTable = _player.Cards.FromLocation(Location.Table).Select(
                x => new
                {
                    x.Identificator,
                    x.Token,
                    (x as Unit).BaseHealth,
                    (x as Unit).Health,
                    x.Damage,
                    x.BaseDamage
                });
            Cards = _player.Cards.FromLocation(Location.InHand).Select(x => new
            {
                x.Identificator,
                x.Token
            });

            AddChild(new CanBePlayed(_player));
        }
    }
}