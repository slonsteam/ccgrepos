﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Controllers
{
    public class StandartSessionController : ISessionController<StandartGameSession>
    {
        public static TimeSpan TimeForAwaitPlayer = new TimeSpan(0, 0, 10, 0);
        private readonly Lazy<IMessageEncoder> _encoderForClientLeave;

        private readonly IMessageEncoder _getTableResponseEncoder;
        private readonly INotifyer _notifyer;

        private IMessageEncoder _returnToGameOfferEncoder;
        private readonly ISessionStorage _storage;
        private readonly IDependencyResolver DI;
        private readonly IMessageEncoder EndGameEncoder;

        public StandartSessionController(IDependencyResolver DI)
        {
            this.DI = DI;
            _notifyer = DI.Get<INotifyer>();
            _storage = DI.Get<ISessionStorage>();
            _encoderForClientLeave = new Lazy<IMessageEncoder>(() =>
                DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("ClientLeave"));
            EndGameEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("EndGameResponse");
            _getTableResponseEncoder =
                DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("GetTableResponse");
        }

        private IMessageEncoder ReturnToGameEncoder
        {
            get
            {
                if (_returnToGameOfferEncoder == null)
                    _returnToGameOfferEncoder = DI.Get<IDependencyResolver>("DIEncoders")
                        .Get<IMessageEncoder>("ReturnToGameOffer");
                return _returnToGameOfferEncoder;
            }
        }


        public StandartGameSession Create(List<SearcherModel> clients)
        {
            //Запихиваем новую сессию в репозиторий сессий
            //Запихиваем сессию в определенный SynchronizationContext
            var PlayersHaveNotSession = true;
            var sessionOfClient1 = GetSessionOf(clients[0].Client);
            if (sessionOfClient1 != null)
            {
                PlayerReturns(sessionOfClient1, clients[0].Client);
                PlayersHaveNotSession = false;
            }

            var sessionOfClient2 = GetSessionOf(clients[1].Client);
            if (sessionOfClient2 != null)
            {
                PlayerReturns(sessionOfClient2, clients[1].Client);
                PlayersHaveNotSession = false;
            }

            if (!PlayersHaveNotSession)
                throw new ArgumentException();

            var newSession = new StandartGameSession(clients[0].Client, clients[1].Client, DI);
            _storage.AddSesssions(newSession);

            newSession.Engine.EndGameEvent += (s, a) =>
            {
                foreach (var client in newSession.Clients)
                    client.Communicator?.SendMessage(EndGameEncoder.EncodeMessage(new SimpleTextNetworkModel(a)));
            };
            return newSession;
        }

        public bool AddPlayerToSession(StandartGameSession session, IClient client)
        {
            return session.AddViewer(client);
        }

        public void AwaitLeavedPlayer(StandartGameSession session, IClient client)
        {
            //Сессия ставится на паузу
            //Начинается ожидание возвращения второго игрока на сервер
            //ставится тамер, по истечению которого сессия загрывается, если второй игрок не ливнет раньше
            if (ClientIsPlayer(session, client))
            {
                if (session.Status != SessionStatus.AwaitPlayer)
                    session.TimerToSessionClose = new Timer(
                        s => { EndSession(session, EndSessionStatus.PlayerLeave); },
                        null, TimeForAwaitPlayer, TimeSpan.Zero);
                else
                    EndSession(session, EndSessionStatus.PlayerLeave);
                session.Pause();
            }

            foreach (var cl in session.Clients.Where(c => c.IsLoged))
                cl.Communicator.SendMessage(
                    _encoderForClientLeave.Value.EncodeMessage(new SimpleTextNetworkModel(session.GetToken(client))));
        }

        public string GetTable(StandartGameSession session, IClient client)
        {
            return session.Engine.ContinueGame(session.GetToken(client));
        }

        public void PlayerReturns(StandartGameSession session, IClient client)
        {
            if (ClientIsPlayer(session, client))
                client.Communicator.SendMessage(
                    ReturnToGameEncoder.EncodeMessage(new SimpleTextNetworkModel("Do you want return to game?")));
            else
                client.Communicator.SendMessage(
                    ReturnToGameEncoder.EncodeMessage(
                        new SimpleTextNetworkModel("Do you want return to game as a viewer?")));
            //Игроку предлагается вернуться в игру
        }

        public void PlayerDenyOldSession(StandartGameSession session, IClient client)
        {
            //Сессия завершается
            //Таймер отключается
            //Делегат ожидания снимается
            if (ClientIsPlayer(session, client))
                EndSession(session, EndSessionStatus.PlayerLeave);
            else
                session.Clients.Remove(client);
        }

        public void PlayerReturnToOldSession(StandartGameSession session, IClient client)
        {
            //Сессия снимается с паузы
            //Таймер отключается
            //Делегат ожидания снимается
            if (ClientIsPlayer(session, client))
            {
                session.Resume();
                session.TimerToSessionClose.Dispose();
                _notifyer.ClientlogIn -= session.ClientLoginAwaiter;
            }

            client.Communicator.SendMessage(
                _getTableResponseEncoder.EncodeMessage(
                    new GetTableNetworkModel(session.Engine.ContinueGame(session.GetToken(client)))));
        }

        public void EndSession(StandartGameSession session, EndSessionStatus status, IClient client = null)
        {
            session.SynchronizationContext.Post(s =>
            {
                var standartEnd = status == EndSessionStatus.EndGame;

                _storage.Remove(session);
                if (status == EndSessionStatus.PlayerLeave)
                {
                    session.EndSession(standartEnd, session.GetToken(client));
                    session.TimerToSessionClose.Dispose();
                    _notifyer.ClientlogIn -= session.ClientLoginAwaiter;
                }
                else
                {
                    if (client != null)
                        session.EndSession(standartEnd, session.GetToken(client));
                    else
                        session.EndSession(standartEnd);
                }

                session.Dispose();
            }, null);

            //Сессия завершается
        }

        public void PlayCard(IClient player, StandartGameSession session, string id, List<string> targets)
        {
            session.SynchronizationContext.Post(s => session.PlayCard(player, id, targets), null);
        }

        public void EndTurn(IClient client, StandartGameSession session)
        {
            session.SynchronizationContext.Post(s => session.EndTurn(client), null);
        }

        public StandartGameSession GetSessionOf(IClient client)
        {
            return _storage.GetSessionOf(client) as StandartGameSession;
        }

        private bool ClientIsPlayer(StandartGameSession session, IClient client)
        {
            return session.FirstPlayer == client || session.SecondPlayer == client;
        }
    }
}