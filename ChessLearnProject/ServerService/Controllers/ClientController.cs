﻿using System;
using System.Collections.Generic;
using System.Linq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Concrete.Client;
using ServerService.Interfaces;

namespace ServerService.Controllers
{
    public class ClientController
    {
        private readonly List<IClient> _allClients;
        private readonly object _clientsLock;
        private readonly IMessageEncoder _logInResponsEncoder;
        private readonly INotifyer _notifyer;
        private readonly Random _random;
        private IDependencyResolver DI;

        public ClientController(IDependencyResolver DI)
        {
            _allClients = new List<IClient>();
            this.DI = DI;
            _notifyer = DI.Get<INotifyer>();
            _notifyer.ClientTryLogIn += (s, a) => { AuthorizeClient(a.Communicator.Owner, a.Login, a.Password); };
            _notifyer.ClientLogOut += (s, a) => { a.Client.IsLoged = false; };
            _logInResponsEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("LogInResponse");
            _random = new Random();
            _clientsLock = new object();
        }

        public List<IClient> AllClients
        {
            get
            {
                lock (_clientsLock)
                {
                    return _allClients;
                }
            }
        }

        public bool RegisterClient(string Login, string Password)
        {
            if (AllClients.FirstOrDefault(c => c.Login == Login) == null)
            {
                _allClients.Add(new ClientTcp {Login = Login, Password = Password});
                return true;
            }

            return false;
        }

        public void Add(IUnauthorisedClient client)
        {
            if (!AllClients.Contains(client))
                AllClients.Add(client);
            if (client.Identificator == null || client.Identificator == string.Empty)
                client.Identificator = GenerateToken();
        }

        public void AuthorizeClient(IClient client, string login, string password)
        {
            var User = AllClients.FirstOrDefault(c => c.Login == login && c.Password == password && !c.IsLoged);
            if (User == null)
            {
                var temp = login;
                //if (AllClients.Any(c => c.Login == login && c.Password == password))
                //{
                //    int i;

                //    for (i = 0; AllClients.Any(c => c.Login == temp); i++)
                //    {
                //        temp = login + i;
                //    }
                //    RegisterClient(temp, password);
                //}
                //else
                //{
                RegisterClient(login, password);
                //}

                User = AllClients.FirstOrDefault(c => c.Login == temp && c.Password == password && !c.IsLoged);
            }

            AuthorizeBase(client, User);
        }

        public void AuthorizeClient(IClient client, string identificator)
        {
            var User = AllClients.FirstOrDefault(c => c.Identificator == identificator && !c.IsLoged);
            AuthorizeBase(client, User, false);
        }

        private void AuthorizeBase(IClient client, IClient User, bool needNewToken = true)
        {
            if (User != null && client is IUnauthorisedClient)
            {
                User = (client as IUnauthorisedClient).AuthorizeClient(User);
                User.IsLoged = true;
                AllClients.Remove(client);
                if (needNewToken)
                    User.Identificator = GenerateToken();
                User.Communicator.SendMessage(
                    _logInResponsEncoder.EncodeMessage(new SimpleTextNetworkModel($"Correct:{User.Identificator}")));
                _notifyer.OnClientLogIn(User);
            }
            else
            {
                client.Communicator.SendMessage(
                    _logInResponsEncoder.EncodeMessage(new SimpleTextNetworkModel($"Error:error")));
            }
        }

        protected string GenerateToken()
        {
            var ret = string.Empty;
            do
            {
                ret = string.Empty;
                for (var i = 0; i < 5; i++)
                    ret += _random.Next().ToString();
            } while (AllClients.Any(c => c.Identificator == ret));

            return ret;
        }

        public void LogOutClient(IClient client)
        {
            if (client is IUnauthorisedClient)
            {
                AllClients.Remove(client);
            }
            else
            {
                client.IsLoged = false;
                client.Communicator = null;
            }

            _notifyer.OnClientTryLogOut(client);
        }
    }
}