﻿using System.Collections.Generic;
using System.Linq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Controllers
{
    public class StandartSessionSearcher : ISessionSearcher<StandartGameSession>
    {
        private readonly List<SearcherModel> _clients;
        private readonly ISessionController<StandartGameSession> _controller;
        private readonly IMessageEncoder _sessionFindEncoder;

        public StandartSessionSearcher(ISessionController<StandartGameSession> controller, IDependencyResolver DI)
        {
            _clients = new List<SearcherModel>();
            _controller = controller;
            _sessionFindEncoder =
                DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("SessionFinded"); //Изменить позже
            DI.Get<INotifyer>().ClientLogOut += (s, a) =>
            {
                if (_clients.Any(m => m.Client == a.Client))
                    Remove(a.Client);
            };
        }

        public void Add(IClient client, List<string> deck, string hero)
        {
            _clients.Add(new SearcherModel(client, hero, deck));

            if (_clients.Count >= 2)
                MatchFound(_clients[0], _clients[1]);
        }

        public void Remove(IClient client)
        {
            if (_clients.Any(m => m.Client == client))
                _clients.Remove(_clients.FirstOrDefault(m => m.Client == client));
        }

        private void MatchFound(SearcherModel cl1, SearcherModel cl2)
        {
            var clients = new List<SearcherModel> {cl1, cl2};
            var session = _controller.Create(clients);
            cl1.Client.Communicator.SendMessage(_sessionFindEncoder.EncodeMessage(
                new SimpleTextNetworkModel(
                    $"Your Token:{session.GetToken(cl1.Client)}:Session finded, your opponent {cl2.Client.Login ?? "Anonim"}")));
            cl2.Client.Communicator.SendMessage(_sessionFindEncoder.EncodeMessage(
                new SimpleTextNetworkModel(
                    $"Your Token:{session.GetToken(cl2.Client)}:Session finded, your opponent {cl1.Client.Login ?? "Anonim"}")));
            _clients.Remove(cl1);
            _clients.Remove(cl2);
            var decks = new Dictionary<IClient, List<string>>();
            foreach (var client in clients)
            {
                var deck = new List<string>();
                deck.Add(client.Hero);
                deck.AddRange(client.Deck);
                decks.Add(client.Client, deck);
            }

            session.Start(decks);
        }
    }

    public class SearcherModel
    {
        public IClient Client;
        public List<string> Deck;
        public string Hero;

        public SearcherModel(IClient client, string hero, List<string> deck)
        {
            Client = client;
            Hero = hero;
            Deck = deck;
        }
    }
}