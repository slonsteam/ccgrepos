﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using CommonDll.Data;
using Contract.Cards;

namespace ServerService.Data
{
    public class CardRepos : IRepository<Card>
    {
        public CardRepos()
        {
            var catalog = new DirectoryCatalog(Directory.GetCurrentDirectory());
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        [ImportMany(AllowRecomposition = true)]
        public IEnumerable<Card> Collection { get; set; }

        public Card Get(string id)
        {
            return Collection.FirstOrDefault(x => x.Identificator == id);
        }
    }
}