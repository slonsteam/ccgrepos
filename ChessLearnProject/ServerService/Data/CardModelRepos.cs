﻿using System.Collections.Generic;
using System.Linq;
using CommonDll.Data;
using Contract.Cards;
using Contract.Core;
using SamePorjLibrary.Data;

namespace ServerService.Data
{
    //todo временное решение исправить (Серафим) по таску
    public class CardModelRepos : IRepository<CardModel>
    {
        public CardModelRepos(IRepository<Card> cards)
        {
            var list = new List<CardModel>();
            foreach (var card in cards.Collection)
            {
                var cardmodel = new CardModel
                {
                    BaseDamage = card.BaseDamage,
                    BaseCost = card.Cost,
                    Description = card.Description,
                    Name = card.Name,
                    Identificator = card.Identificator,
                    ImageSource = card.ImageSource,
                    Priority = card.Priority,
                    CanBeInDec = card.Location != Location.Core
                };

                if (card is Unit unit)
                {
                    cardmodel.BaseHealth = unit.BaseHealth;
                    cardmodel.Type = "Unit";
                }

                if (card is Hero) cardmodel.Type = "Hero";
                if (card is Spell) cardmodel.Type = "Spell";
                list.Add(cardmodel);
            }

            Collection = list;
        }

        public IEnumerable<CardModel> Collection { get; set; }

        public CardModel Get(string id)
        {
            return Collection.FirstOrDefault(x => x.Identificator == id);
        }
    }
}