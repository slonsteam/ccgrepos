﻿using System;
using System.Net.Sockets;
using System.Text;
using SamePorjLibrary.Interfaces;
using ServerService.Controllers;
using ServerService.Interfaces;
using Sprache;

namespace ServerService
{
    public class MessageHandlingConveyor : IDisposable
    {
        private readonly ICommDispatcher _commDispatcher;
        private readonly IDependencyResolver _container;
        private readonly Encoding _encoding;
        private readonly TcpListener _listener;

        private readonly Parser<string> _messageTypeParser =
            from type in Parse.Letter.Many().Text()
            from spliter in Parse.Char(':').Once()
            from message in Parse.AnyChar.Many()
            select type;

        private readonly INotifyer _notifyer;
        private readonly ClientController _clientController;
        private readonly CustomSynchronizationContext _synchronizationContext;

        protected Parser<string> MessageBodyParser =
            from type in Parse.Letter.Many()
            from spliter in Parse.Char(':').Once()
            from message in Parse.AnyChar.Many().Text()
            select message;

        public MessageHandlingConveyor(IDependencyResolver container, IDependencyResolver globalContainer)
        {
            _container = container;
            _listener = globalContainer.Get<TcpListener>();
            _commDispatcher = globalContainer.Get<ICommDispatcher>();
            _encoding = globalContainer.Get<Encoding>();
            _notifyer = globalContainer.Get<INotifyer>();
            _clientController = globalContainer.Get<ClientController>();
            _synchronizationContext = new CustomSynchronizationContext();

            _commDispatcher.MessageRecieved += (s, a) => { InvokeMessageRecievedEventInMainThread(a); };
            _commDispatcher.ClientLogOut += (s, a) => { _clientController.LogOutClient(a.Client); };
        }

        public void Dispose()
        {
            _synchronizationContext?.Dispose();
            _commDispatcher.Dispose();
        }

        private void InvokeMessageRecievedEventInMainThread(MessageRecievedEventArgs a)
        {
            _notifyer.OnMessageRecieved(a.Client, a.Message);

            try
            {
                var MessageType = _messageTypeParser.Parse(a.Message);
                if (a.Client is IUnauthorisedClient && MessageType != "LogIn"
                ) //Неавторизованный пользователь может только авторизоваться
                    return;
                _synchronizationContext.Post(state =>
                {
                    var MessageHandler = _container.Get<IServerMessageHandler>(MessageType);
                    MessageHandler.HandleMessage(MessageBodyParser.Parse(a.Message), a?.Client);
                }, null);
            }
            catch (Exception e)
            {
                _notifyer.OnCanNotHandleMessage(a.Client, a.Message);
            }
        }

        public void Start()
        {
            _listener.Start();
            AcceptTcpClientAsync();
            _commDispatcher.Process();
        }

        private void AcceptTcpClientAsync()
        {
            _listener.BeginAcceptTcpClient(AsyncCallback, _listener);
        }


        protected void AsyncCallback(IAsyncResult ar)
        {
            var listener = (TcpListener) ar.AsyncState;
            var client = listener.EndAcceptTcpClient(ar);

            var communicator = new ServerCommunicator(client, _encoding);
            _commDispatcher.Add(communicator);
            communicator.MessageReaded += (s, a) => { };
            communicator.BeginListenForMessage();
            _clientController.Add(communicator.Owner as IUnauthorisedClient);
            AcceptTcpClientAsync();
        }
    }
}