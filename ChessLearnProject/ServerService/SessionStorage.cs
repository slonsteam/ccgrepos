﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ServerService.Interfaces;

namespace ServerService
{
    public class SessionStorage : ISessionStorage
    {
        private readonly List<IGameSession> _allSessions;
        private readonly Random _rand;
        private readonly int? _sessionCountInThread;
        private readonly Dictionary<SynchronizationContext, List<IGameSession>> _sessions;

        public SessionStorage(int? sessionCountInThread = null)
        {
            _sessionCountInThread = sessionCountInThread;
            _sessions = new Dictionary<SynchronizationContext, List<IGameSession>>();
            _rand = new Random();
            _allSessions = new List<IGameSession>();
        }

        public List<IGameSession> AllSessions
        {
            get
            {
                _allSessions.Clear();
                foreach (var pair in _sessions) _allSessions.AddRange(pair.Value);

                return _allSessions;
            }
        }

        public IGameSession GetSessionBy(string id)
        {
            return AllSessions.FirstOrDefault(s => s.SessionID == id);
        }

        public void AddSesssions(IGameSession session)
        {
            if (_sessions.Count == 0)
                _sessions.Add(new CustomSynchronizationContext(), new List<IGameSession>());
            var id = _rand.Next().ToString();
            while (_allSessions.Any(s => s.SessionID == id)) id = _rand.Next().ToString();

            session.SessionID = id;
            KeyValuePair<SynchronizationContext, List<IGameSession>> SessionList;

            if (_sessionCountInThread != null)
                SessionList = _sessions.FirstOrDefault(p => p.Value.Count < (int) _sessionCountInThread);
            else
                SessionList = _sessions.First();

            SessionList.Value.Add(session);
            session.SynchronizationContext = SessionList.Key;
        }

        public void Remove(IGameSession session)
        {
            var list = _sessions.Values.FirstOrDefault(p => p.Contains(session));

            if (list != null) list.Remove(session);
        }

        public IGameSession GetSessionOf(IClient client)
        {
            foreach (var list in _sessions.Values)
            {
                var ret = list.FirstOrDefault(s => s.Clients.Contains(client));
                if (ret != null)
                    return ret;
            }

            return null;
        }
    }
}