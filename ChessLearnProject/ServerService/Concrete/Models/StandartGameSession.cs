﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Contract.Core;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Interfaces;

namespace ServerService.Concrete.Models
{
    public class StandartGameSession : IStandartGameSession, IDisposable
    {
        private readonly ISessionController<StandartGameSession> _controller;
        private readonly IEngine _engine;
        private readonly INotifyer _notifyer;
        private readonly IMessageEncoder EndTurnEncoder;
        private readonly IMessageEncoder PlayCardAnswerEncoder;
        private readonly IMessageEncoder StartGameEncoder;
        public Timer TimerToSessionClose;

        public StandartGameSession(IClient client1, IClient client2, IDependencyResolver DI)
        {
            Status = SessionStatus.Create;
            _notifyer = DI.Get<INotifyer>();
            _engine = DI.Get<IEngine>();
            Engine = _engine;
            _controller = DI.Get<ISessionController<StandartGameSession>>();
            Clients = new ObservableCollection<IClient> {client1, client2};
            SessionResumed = (s, a) => { Status = SessionStatus.Play; };
            ClientLoginAwaiter = (s, a) =>
            {
                if (Clients.Contains(a.client))
                    _controller.PlayerReturns(this, a.client);
            };
            _notifyer.ClientLogOut += OnClientLogOut;
            PlayCardAnswerEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("PlayCardResponce");
            StartGameEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("StartGame");
            EndTurnEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("EndTurnResponse");
            _engine.EndTurnEvent += (s, a) =>
            {
                foreach (var client in Clients.Where(c => c.IsLoged))
                    client.Communicator.SendMessage(EndTurnEncoder.EncodeMessage(new SimpleTextNetworkModel(a)));
            };
        }

        public EventHandler<ClientLogInEventArgs> ClientLoginAwaiter { get; }

        public void Dispose()
        {
            _notifyer.ClientLogOut -= OnClientLogOut;
            Clients = null;
        }

        public string GetToken(IClient client)
        {
            if (!Clients.Contains(client))
                return null;
            if (client == FirstPlayer)
                return "FirstPlayer";
            if (client == SecondPlayer)
                return "SecondPlayer";
            return $"viewer{Clients.IndexOf(client) - 1}";
        }

        public SynchronizationContext SynchronizationContext { get; set; }
        public ObservableCollection<IClient> Clients { get; private set; }


        public void Pause()
        {
            if (!FirstPlayer.IsLoged || !SecondPlayer.IsLoged)
                Status = SessionStatus.AwaitPlayer;
            else
                Status = SessionStatus.Pause;
            OnSessionPaused();
        }

        public void Resume()
        {
            OnSessionResumed();
        }

        public void Start(Dictionary<IClient, List<string>> playerdecks)
        {
            Status = SessionStatus.Play;
            var decks = new Dictionary<string, List<string>>();
            foreach (var pair in playerdecks) decks.Add(GetToken(pair.Key), pair.Value);

            var message =
                _engine.StartGame(decks);
            foreach (var client in Clients)
                client.Communicator.SendMessage(StartGameEncoder.EncodeMessage(new SimpleTextNetworkModel(message)));
            //Точка входа для игрового процессора.
        }

        public SessionStatus Status { get; private set; }

        public void EndTurn(IClient client)
        {
            var token = GetToken(client);
            if (token != null)
                _engine.EndTurn(token);
        }

        public void PlayCard(IClient client, string id, List<string> targets)
        {
            var token = GetToken(client);
            if (token == null)
                return;
            var message = _engine.PlayCard(token, id, targets);
            foreach (var cl in Clients.Where(c => c.IsLoged))
                cl.Communicator.SendMessage(PlayCardAnswerEncoder.EncodeMessage(
                    new SimpleTextNetworkModel(message)));
        }

        public event EventHandler SessionPaused;
        public event EventHandler SessionResumed;
        public event EventHandler SessionEndedStandart;
        public event EventHandler SessionEndedUnstandart;
        public IEngine Engine { get; }

        public void EndSession(bool standartEnd, string token = null)
        {
            if (token != null)
                Engine.EndGame(token);
            if (standartEnd)
                OnSessionEndedStandart();
            else
                OnSessionEndedUnstandart();
        }

        public string SessionID { get; set; }

        public IClient FirstPlayer => Clients[0];

        public IClient SecondPlayer => Clients[1];

        public bool AddViewer(IClient viewer)
        {
            if (Status == SessionStatus.Play)
            {
                Clients.Add(viewer);
                return true;
            }

            return false;
        }

        private void OnClientLogOut(object sender, ClientLogOutEventArgs a)
        {
            if (Clients.Contains(a.Client))
            {
                _controller.AwaitLeavedPlayer(this, a.Client);
                _notifyer.ClientlogIn += ClientLoginAwaiter;
            }
        }

        protected virtual void OnSessionPaused()
        {
            SessionPaused?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnSessionResumed()
        {
            SessionResumed?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnSessionEndedStandart()
        {
            SessionEndedStandart?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnSessionEndedUnstandart()
        {
            SessionEndedUnstandart?.Invoke(this, EventArgs.Empty);
        }
    }
}