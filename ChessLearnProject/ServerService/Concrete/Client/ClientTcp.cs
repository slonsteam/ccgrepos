﻿using ServerService.Interfaces;

namespace ServerService.Concrete.Client
{
    public class ClientTcp : IClient
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Identificator { get; set; }
        public IServerCommunicator Communicator { get; set; }
        public bool IsLoged { get; set; }
    }
}