﻿using ServerService.Interfaces;

namespace ServerService.Concrete.Client
{
    public class UnauthorisedClient : IUnauthorisedClient
    {
        private IServerCommunicator _communicator;

        public UnauthorisedClient(IServerCommunicator communicator)
        {
            Communicator = communicator;
            communicator.Owner = this;
            IsLoged = true;
        }

        public string Login { get; set; }
        public string Password { get; set; }
        public string Identificator { get; set; }

        public IServerCommunicator Communicator
        {
            get => _communicator;
            set
            {
                if (value is IServerCommunicator)
                    _communicator = value;
            }
        }

        public bool IsLoged { get; set; }

        public TClient AuthorizeClient<TClient>(TClient client) where TClient : IClient
        {
            client.Communicator = Communicator;
            _communicator.Owner = client;
            return client;
        }
    }
}