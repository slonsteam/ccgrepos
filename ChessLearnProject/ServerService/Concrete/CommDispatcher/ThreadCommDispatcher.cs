﻿using System;
using System.Collections.Generic;
using System.Threading;
using SamePorjLibrary.Interfaces;
using ServerService.Abstract;
using ServerService.Interfaces;

namespace ServerService.Concrete.CommDispatcher
{
    public class ThreadCommDispatcher : CommDispatcherBase, IDisposable
    {
        private readonly List<IServerCommunicator> _communicators = new List<IServerCommunicator>();


        private bool _isInProcess;

        public ThreadCommDispatcher(int? maxServedComm, IDependencyResolver DI) : base(DI)
        {
            MaxServedComm = MaxServedComm;
        }

        public int? MaxServedComm { get; set; }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
        }

        public override void Add(IServerCommunicator communicator)
        {
            base.Add(communicator);
            if (MaxServedComm == null || _communicators.Count < MaxServedComm)
            {
                _communicators.Add(communicator);
                if (_isInProcess) ThreadPool.QueueUserWorkItem(ThreadPoolCallback, communicator);
            }
            else
            {
                throw new InvalidOperationException("Мест на сервере нет");
            }
        }

        private void ThreadPoolCallback(object context)
        {
            LaunchCommunicator(context as IServerCommunicator);
        }

        private void LaunchCommunicator(IServerCommunicator communicator)
        {
            communicator.BeginListenForMessage();
            communicator.MessageReaded += (s, a) =>
            {
                lock (communicator)
                {
                    OnMessageRecieved(communicator, a.Message);
                }
            };
            communicator.ConnectionBroken += (s, a) =>
            {
                OnClientLogOut(new ClientLogOutEventArgs {Client = communicator.Owner});
            };
        }

        public event EventHandler<MessageRecievedEventArgs> MessageRecieved = (s, a) => { };

        public override void Process()
        {
            if (!_isInProcess)
            {
                foreach (var communicator in _communicators)
                    ThreadPool.QueueUserWorkItem(ThreadPoolCallback, communicator);
                _isInProcess = true;
            }
        }


        ~ThreadCommDispatcher()
        {
            ReleaseUnmanagedResources();
        }


        private void ReleaseUnmanagedResources()
        {
            foreach (var communicator in _communicators) communicator.Dispose();
            // TODO release unmanaged resources here
        }
    }
}