﻿using SamePorjLibrary.Abstract;

namespace ServerService.Concrete.MessageEncoder
{
    public class ReturnToOldGameRequestEncoder : MessageEncoderBase
    {
        public ReturnToOldGameRequestEncoder() : base("ReturnToOldGameRequest")
        {
        }
    }
}