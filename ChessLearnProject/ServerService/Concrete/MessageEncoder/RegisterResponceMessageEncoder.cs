﻿using SamePorjLibrary.Abstract;

namespace ServerService.Concrete.MessageEncoder
{
    public class RegisterResponceMessageEncoder : MessageEncoderBase
    {
        public RegisterResponceMessageEncoder() : base("RegisterResponce")
        {
        }
    }
}