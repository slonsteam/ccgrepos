﻿using SamePorjLibrary.Interfaces;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class EndTurnHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;

        public EndTurnHandler(IDependencyResolver DI) : base("EndTurn", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            var session = _controller.GetSessionOf(client);
            if (session != null) _controller.EndTurn(client, session);
        }
    }
}