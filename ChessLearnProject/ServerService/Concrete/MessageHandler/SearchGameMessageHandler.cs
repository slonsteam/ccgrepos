﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class SearchGameMessageHandler : MessageHandlerServerBase
    {
        private readonly ISessionSearcher<StandartGameSession> _searcher;

        public SearchGameMessageHandler(IDependencyResolver DI) : base("SearchGame", DI)
        {
            _searcher = DI.Get<ISessionSearcher<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            SearchGameMessageNetworkModel Model = null;

            Model =
                JsonConvert.DeserializeObject<SearchGameMessageNetworkModel>(message);
            if (Model == null)
                return;

            if (Model.Status == SearchGameStatus.GoInSearch)
            {
                _searcher.Add(client, Model.Deck, Model.Hero);
                return;
            }

            if (Model.Status == SearchGameStatus.GoOutSearch)
            {
                _searcher.Remove(client);
            }
        }
    }
}