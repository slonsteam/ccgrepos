﻿using System.Linq;
using CommonDll.Data;
using Newtonsoft.Json;
using SamePorjLibrary.Data;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class GetCardModelHandler : MessageHandlerServerBase
    {
        private readonly IMessageEncoder _responseEncoder;
        private IDependencyResolver DI;
        private readonly IRepository<CardModel> repos;

        public GetCardModelHandler(IDependencyResolver DI) : base("GetCardModel", DI)
        {
            this.DI = DI;
            repos = DI.Get<IRepository<CardModel>>();
            _responseEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("GetCardModelResponse");
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            //todo доделать нормальный репозиторий cardmodel'ов
            client.Communicator.SendMessage(
                _responseEncoder.EncodeMessage(
                    new SimpleTextNetworkModel(JsonConvert.SerializeObject(repos.Collection.ToList()))));
        }
    }
}