﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class PlayCardMessageHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;

        public PlayCardMessageHandler(IDependencyResolver DI) : base("PlayCardMessage", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            var model = JsonConvert.DeserializeObject<PlayCardNetworkModel>(message);
            _controller.PlayCard(client, _controller.GetSessionOf(client), model.CardPlayed,
                model.Targets);
        }
    }
}