﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class RegisterMessageHandler : MessageHandlerServerBase
    {
        private readonly INotifyer _notifyer;
        private readonly ClientController controller;
        private readonly IDependencyResolver DI;

        public RegisterMessageHandler(IDependencyResolver DI) : base("Register", DI)
        {
            this.DI = DI;
            _notifyer = DI.Get<INotifyer>();
            controller = DI.Get<ClientController>();
        }

        protected override bool HandleMessage(string message, ICommunicator communicator)
        {
            var model = JsonConvert.DeserializeObject<RegisterMessageNetworkModel>(message);
            _notifyer.OnClientTryRegister(model.Login, model.Password, (IServerCommunicator) communicator);
            var responceEncoder =
                DI.Get<IDependencyResolver>("MessageEncoders").Get<IMessageEncoder>("RegisterResponce");
            if (controller.RegisterClient(model.Login, model.Password))
            {
                communicator.SendMessage(responceEncoder.EncodeMessage(
                    new RegisterResponseNetworkModel {RegisterComplete = true, Message = "Registration complete!"}));
                return true;
            }

            communicator.SendMessage(responceEncoder.EncodeMessage(
                new RegisterResponseNetworkModel {RegisterComplete = false, Message = "Registration failed"}));
            return false;
        }
    }
}