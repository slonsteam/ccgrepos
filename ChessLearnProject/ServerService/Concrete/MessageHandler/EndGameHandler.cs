﻿using SamePorjLibrary.Interfaces;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class EndGameHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;

        public EndGameHandler(IDependencyResolver DI) : base("EndGame", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            _controller.EndSession(_controller.GetSessionOf(client), EndSessionStatus.EndGame, client);
        }
    }
}