﻿using System;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class GetTargetsHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;
        private readonly Lazy<IMessageEncoder> _encoderForResponce;

        public GetTargetsHandler(IDependencyResolver DI) : base("GetTargets", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
            _encoderForResponce = new Lazy<IMessageEncoder>(() =>
                DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("GetTargetsResponse"));
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            var session = _controller.GetSessionOf(client);
            var model = JsonConvert.DeserializeObject<GetTargetsNetworkModel>(message);

            var responseModel = new GetTargetResponseNetworkModel();
            responseModel.Id = model.Id;

            responseModel.Targets = session.Engine.GetTargets(model.Id);

            client.Communicator.SendMessage(_encoderForResponce.Value.EncodeMessage(responseModel));
        }
    }
}