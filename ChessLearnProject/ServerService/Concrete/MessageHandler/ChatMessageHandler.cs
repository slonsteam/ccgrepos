﻿using System.Linq;
using Newtonsoft.Json;
using SamePorjLibrary;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class ChatMessageHandler : MessageHandlerServerBase
    {
        private readonly ClientController controller;

        private IDependencyResolver DI;
        private readonly bool UnauthorizedUserCanSendMessage;

        public ChatMessageHandler(IDependencyResolver DI, bool availableForUnauthorized = false) : base("ChatMessage",
            DI)
        {
            this.DI = DI;
            UnauthorizedUserCanSendMessage = availableForUnauthorized;
            controller = DI.Get<ClientController>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            if (!(client is IUnauthorisedClient) || UnauthorizedUserCanSendMessage)
            {
                var model = JsonConvert.DeserializeObject<ChatMessageNetworkModel>(message);
                var encoder = new ChatMessageEncoder();
                var NewMessage =
                    encoder.EncodeMessage(new ChatMessageNetworkModel
                    {
                        Message = model.Message,
                        Name = client.Login ?? model.Name
                    });

                foreach (var _client in controller.AllClients.Where(c => c.IsLoged))
                    _client.Communicator.SendMessage(NewMessage);
            }
        }
    }
}