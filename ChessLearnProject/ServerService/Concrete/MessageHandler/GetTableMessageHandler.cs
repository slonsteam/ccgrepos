﻿using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class GetTableMessageHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;
        private readonly IMessageEncoder _responseEncoder;

        public GetTableMessageHandler(IDependencyResolver DI) : base("GetTable", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
            _responseEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("GetTableResponse");
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            var session = _controller.GetSessionOf(client);
            var response = _controller.GetTable(session, client);
            client.Communicator.SendMessage(_responseEncoder.EncodeMessage(new GetTableNetworkModel(response)));
        }
    }
}