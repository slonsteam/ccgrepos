﻿using System;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Controllers;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class LogInMessageHandler : MessageHandlerServerBase
    {
        private readonly ClientController _controller;

        public LogInMessageHandler(IDependencyResolver DI) : base("LogIn", DI)
        {
            _controller = DI.Get<ClientController>();
        }

        protected override bool HandleMessage(string model, ICommunicator communicator)
        {
            LogInNetworkModel Model = null;

            Model =
                JsonConvert.DeserializeObject<LogInNetworkModel>(model);
            if (Model == null)
                return false;

            Authorize(Model, (communicator as IServerCommunicator).Owner);
            return true;
        }

        private void Authorize(LogInNetworkModel model, IClient client)
        {
            if (model.Token == null)
                _controller.AuthorizeClient(client, model.Login, model.Password);
            else
                _controller.AuthorizeClient(client, model.Token);
        }

        public event EventHandler MessageHandled = (s, e) => { };

        protected virtual void OnMessageHandled()
        {
            MessageHandled(this, EventArgs.Empty);
        }
    }
}