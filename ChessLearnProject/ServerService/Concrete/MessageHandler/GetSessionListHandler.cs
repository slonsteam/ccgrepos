﻿using System.Collections.Generic;
using System.Linq;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class GetSessionListHandler : MessageHandlerServerBase
    {
        private readonly IMessageEncoder _responseEncoder;
        private readonly ISessionStorage _storage;

        public GetSessionListHandler(IDependencyResolver DI) : base("GetSessionList", DI)
        {
            _storage = DI.Get<ISessionStorage>();
            _responseEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("GetSessionListResponse");
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            var Model = new GetSessionListResponseNetworkModel(_storage.AllSessions.OfType<StandartGameSession>()
                .Where(s => client != s.FirstPlayer && client != s.SecondPlayer && s.Status == SessionStatus.Play)
                .Select(s =>
                    new SessionModel
                    {
                        Players = new List<string> {s.FirstPlayer.Login, s.SecondPlayer.Login},
                        Id = s.SessionID
                    }).ToList());
            client.Communicator.SendMessage(_responseEncoder.EncodeMessage(Model));
        }
    }
}