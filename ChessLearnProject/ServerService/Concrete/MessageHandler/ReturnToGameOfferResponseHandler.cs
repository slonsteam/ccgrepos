﻿using SamePorjLibrary.Interfaces;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class ReturnToGameOfferResponseHandler : MessageHandlerServerBase
    {
        private ISessionController<StandartGameSession> _controller;

        public ReturnToGameOfferResponseHandler(IDependencyResolver DI) : base("ReturnToGameOfferResponse", DI)
        {
        }

        public override void HandleMessage(string message, IClient client)
        {
            var answer = bool.Parse(message);
            var session = _controller.GetSessionOf(client);
            if (answer)
                _controller.PlayerReturnToOldSession(session, client);
            else
                _controller.PlayerDenyOldSession(session, client);
        }
    }
}