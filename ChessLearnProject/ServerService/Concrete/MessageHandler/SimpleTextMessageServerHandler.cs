﻿using SamePorjLibrary.Handlers;
using SamePorjLibrary.Interfaces;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class SimpleTextMessageServerHandler : SimpleTextMessageHandler, IServerMessageHandler
    {
        public SimpleTextMessageServerHandler(string MessageType, IDependencyResolver DI) : base(MessageType, DI)
        {
        }

        public void HandleMessage(string message, IClient client)
        {
            HandleMessage(message, client.Communicator);
        }
    }
}