﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class ViewForSessionHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;
        private readonly IMessageEncoder _responseEncoder;
        private readonly ISessionStorage _storage;

        public ViewForSessionHandler(IDependencyResolver DI) : base("ViewForSession", DI)
        {
            _responseEncoder = DI.Get<IDependencyResolver>("DIEncoders").Get<IMessageEncoder>("ViewForSessionResponse");
            _storage = DI.Get<ISessionStorage>();
            _controller = DI.Get<ISessionController<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            var session = _storage.GetSessionBy(JsonConvert.DeserializeObject<string>(message));
            var result = _controller.AddPlayerToSession((StandartGameSession) session, client);
            client.Communicator.SendMessage(
                _responseEncoder.EncodeMessage(new SimpleTextNetworkModel(result.ToString())));
        }
    }
}