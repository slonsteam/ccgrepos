﻿using SamePorjLibrary.Interfaces;
using ServerService.Abstract;
using ServerService.Concrete.Models;
using ServerService.Interfaces;

namespace ServerService.Concrete.MessageHandler
{
    public class ReturnToOldGameResponceHandler : MessageHandlerServerBase
    {
        private readonly ISessionController<StandartGameSession> _controller;

        public ReturnToOldGameResponceHandler(IDependencyResolver DI) : base("ReturnToOldGameResponce", DI)
        {
            _controller = DI.Get<ISessionController<StandartGameSession>>();
        }

        public override void HandleMessage(string message, IClient client)
        {
            base.HandleMessage(message, client);
            _controller.PlayerReturnToOldSession(_controller.GetSessionOf(client), client);
        }
    }
}