﻿using System;
using System.Threading;
using ServerService.Interfaces;

namespace ServerService.Concrete.Notifyer
{
    public class EventCenterNotifyer : INotifyer
    {
        private readonly SynchronizationContext _synchronizationContext;

        public EventCenterNotifyer()
        {
            _synchronizationContext = new CustomSynchronizationContext();
        }

        public event EventHandler<ClientTryLogInEventArgs> ClientTryLogIn = (s, a) => { };

        public event EventHandler<ClientLogInEventArgs> ClientlogIn = (s, a) => { };

        public event EventHandler<ClientLogOutEventArgs> ClientLogOut = (s, a) => { };

        public event EventHandler<MessageRecievedEventArgs> MessageRecieved = (s, a) => { };

        public event EventHandler<MessageRecievedEventArgs> CanNotHandleMessage = (s, a) => { };

        public event EventHandler<ClientTryRegisterEventArgs> ClientTryRegister = (s, a) => { };

        public void OnClientTryRegister(string Login, string Password, IServerCommunicator Communicator)
        {
            _synchronizationContext.Post(GoClientLogInInMainThread
                , new ClientTryRegisterEventArgs
                {
                    Login = Login,
                    Password = Password,
                    Communicator = Communicator
                });
        }

        public void OnClientTryLogIn(string Login, string Password, IServerCommunicator Communicator)
        {
            _synchronizationContext.Post(GoClientLogInInMainThread
                , new ClientTryLogInEventArgs
                {
                    Login = Login,
                    Password = Password,
                    Communicator = Communicator
                });
        }

        public void OnClientTryLogOut(IClient client)
        {
            _synchronizationContext.Post(
                state => { ClientLogOut(null, state as ClientLogOutEventArgs); },
                new ClientLogOutEventArgs {Client = client});
        }

        public void OnMessageRecieved(IClient client, string message)
        {
            _synchronizationContext.Post(
                state => { MessageRecieved(null, state as MessageRecievedEventArgs); },
                new MessageRecievedEventArgs
                {
                    Client = client,
                    Message = message
                });
        }

        public void OnCanNotHandleMessage(IClient client, string message)
        {
            _synchronizationContext.Post(
                state => { CanNotHandleMessage(null, state as MessageRecievedEventArgs); },
                new MessageRecievedEventArgs
                {
                    Client = client,
                    Message = message
                });
        }

        public virtual void OnClientLogIn(IClient e)
        {
            ClientlogIn(this, new ClientLogInEventArgs {client = e});
        }

        private void GoClientLogInInMainThread(object state)
        {
            ClientTryLogIn(null, state as ClientTryLogInEventArgs);
        }
    }
}