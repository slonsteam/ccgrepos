﻿using System.Collections.Generic;
using System.Linq;
using Contract.Core;
using Newtonsoft.Json;
using SamePorjLibrary.Model;

namespace ServerService.Concrete.ActionLoger
{
    public class StandartActionLoger : IActionLogger
    {
        private readonly List<ActionModel> _body;

        public StandartActionLoger()
        {
            _body = new List<ActionModel>();
        }

        public void StartAction(string name)
        {
            var start = new ActionModel();
            start.ActionName = name;
            start.Status = ActionModelStatus.Start;
            _body.Add(start);
        }

        public void EndAction(string name)
        {
            var start = _body.LastOrDefault(m => m.ActionName == name && !m.IsClosed);
            if (start == null)
                return;
            var end = new ActionModel();
            end.ActionName = name;
            end.Status = ActionModelStatus.End;
            _body.Add(end);
            start.IsClosed = true;
        }

        public string GetMessage()
        {
            //todo object может содержать циклические ссылки
            var ret = JsonConvert.SerializeObject(_body,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    TypeNameHandling = TypeNameHandling.Objects
                });
            _body.Clear();
            return ret;
        }

        public void ActionFailed(string name)
        {
            var model = _body.LastOrDefault(m => m.ActionName == name && !m.IsClosed);
            EndAction(name);
            model.Failed = true;
        }

        public void SetActionParameters(string name, Dictionary<string, object> parameters)
        {
            var action = _body.LastOrDefault(a => a.ActionName == name && a.Status == ActionModelStatus.Start);
            if (action != null)
            {
                action.Parametrs.Clear();
                foreach (var pair in parameters) action.Parametrs.Add(pair.Key, pair.Value);
            }
        }
    }
}