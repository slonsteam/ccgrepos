﻿using SamePorjLibrary.Interfaces;

namespace ServerService
{
    public static class GlobalCore
    {
        public static IDependencyResolver DependencyResolver;
        public static IDependencyResolver DIForMessageHandling;
        public static IDependencyResolver DIEncoders;
    }
}