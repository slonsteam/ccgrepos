﻿using System.Collections.Generic;
using ServerService.Controllers;

namespace ServerService.Interfaces
{
    public interface ISessionController<T> where T : IGameSession
    {
        /// <summary>
        ///     Создание новой сессии для списка клиентов
        /// </summary>
        /// <param name="clients"></param>
        /// <returns></returns>
        T Create(List<SearcherModel> clients);

        /// <summary>
        ///     Добавление нового клиента в сессию (зрителя)
        /// </summary>
        /// <param name="session"></param>
        /// <param name="client"></param>
        bool AddPlayerToSession(T session, IClient client);

        /// <summary>
        ///     Срабатывает, когда один из игроков вылетает из сессии
        /// </summary>
        /// <param name="session"></param>
        /// <param name="client"></param>
        void AwaitLeavedPlayer(T session, IClient client);

        /// <summary>
        ///     Срабатывает когда вылетевший игрок возвращается на сервер
        /// </summary>
        /// <param name="session"></param>
        /// <param name="client"></param>
        void PlayerReturns(T session, IClient client);

        /// <summary>
        ///     Срабатывает когда игрок отказывается возвращаться в старую игру
        /// </summary>
        /// <param name="session"></param>
        /// <param name="client"></param>
        void PlayerDenyOldSession(T session, IClient client);

        /// <summary>
        ///     Срабатывает когда игрок возвращается в старуую игру
        /// </summary>
        /// <param name="session"></param>
        /// <param name="client"></param>
        void PlayerReturnToOldSession(T session, IClient client);

        /// <summary>
        ///     Завершает сессию с определенным статусом
        /// </summary>
        /// <param name="session"></param>
        /// <param name="status"></param>
        void EndSession(T session, EndSessionStatus status, IClient client);

        /// <summary>
        ///     Действие при сыгранной карте
        /// </summary>
        /// <param name="player"></param>
        /// <param name="id"></param>
        /// <param name="targets"></param>
        /// <returns></returns>
        void PlayCard(IClient player, T session, string id, List<string> targets);

        string GetTable(T session, IClient client);

        T GetSessionOf(IClient client);

        void EndTurn(IClient client, T session);
    }

    public enum EndSessionStatus
    {
        EndGame,
        PlayerLeave
    }
}