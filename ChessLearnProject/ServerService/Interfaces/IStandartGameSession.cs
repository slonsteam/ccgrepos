﻿namespace ServerService.Interfaces
{
    public interface IStandartGameSession : IGameSession
    {
        IClient FirstPlayer { get; }
        IClient SecondPlayer { get; }
        bool AddViewer(IClient viewer);
    }
}