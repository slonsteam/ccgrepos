﻿using SamePorjLibrary.Interfaces;

namespace ServerService.Interfaces
{
    public interface IServerCommunicator : ICommunicator
    {
        IClient Owner { get; set; }
    }
}