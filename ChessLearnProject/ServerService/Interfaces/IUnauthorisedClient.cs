﻿namespace ServerService.Interfaces
{
    public interface IUnauthorisedClient : IClient
    {
        TClient AuthorizeClient<TClient>(TClient client) where TClient : IClient;
    }
}