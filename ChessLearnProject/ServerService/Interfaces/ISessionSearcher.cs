﻿using System.Collections.Generic;

namespace ServerService.Interfaces
{
    public interface ISessionSearcher<T> where T : IGameSession
    {
        void Add(IClient client, List<string> deck, string hero);
        void Remove(IClient client);
    }
}