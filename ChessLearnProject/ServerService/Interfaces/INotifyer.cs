﻿using System;

namespace ServerService.Interfaces
{
    public interface INotifyer
    {
        event EventHandler<ClientTryLogInEventArgs> ClientTryLogIn;

        event EventHandler<ClientLogInEventArgs> ClientlogIn;

        event EventHandler<ClientLogOutEventArgs> ClientLogOut;

        event EventHandler<MessageRecievedEventArgs> MessageRecieved;

        event EventHandler<MessageRecievedEventArgs> CanNotHandleMessage;

        event EventHandler<ClientTryRegisterEventArgs> ClientTryRegister;

        void OnClientTryRegister(string Login, string Password, IServerCommunicator Communicator);

        void OnClientTryLogIn(string Login, string Password, IServerCommunicator Communicator);

        void OnClientLogIn(IClient client);

        void OnClientTryLogOut(IClient client);

        void OnMessageRecieved(IClient client, string message);

        void OnCanNotHandleMessage(IClient client, string message);
    }
}