﻿namespace ServerService.Interfaces
{
    public interface IClient
    {
        string Login { get; set; }
        string Password { get; set; }
        string Identificator { get; set; }
        IServerCommunicator Communicator { get; set; }
        bool IsLoged { get; set; }
    }
}