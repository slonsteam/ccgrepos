﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Contract.Core;

namespace ServerService.Interfaces
{
    public interface IGameSession
    {
        /// <summary>
        ///     Необходим, для распределения сессий по нескольким потокам.
        /// </summary>
        SynchronizationContext SynchronizationContext { get; set; }

        ObservableCollection<IClient> Clients { get; }
        SessionStatus Status { get; }
        IEngine Engine { get; }
        string SessionID { get; set; }
        string GetToken(IClient client);
        void Pause();
        void Resume();
        void Start(Dictionary<IClient, List<string>> playerdecks);
        void EndTurn(IClient client);
        void PlayCard(IClient client, string id, List<string> targets);
        event EventHandler SessionPaused;
        event EventHandler SessionResumed;
        event EventHandler SessionEndedStandart;
        event EventHandler SessionEndedUnstandart;
        void EndSession(bool standartEnd, string token);
    }

    public enum SessionStatus
    {
        Play,
        Pause,
        AwaitPlayer,
        Create
    }
}