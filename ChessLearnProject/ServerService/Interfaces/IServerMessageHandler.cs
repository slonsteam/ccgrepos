﻿namespace ServerService.Interfaces
{
    public interface IServerMessageHandler
    {
        void HandleMessage(string message, IClient communicator);
    }
}