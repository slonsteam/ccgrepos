﻿using System;

namespace ServerService.Interfaces
{
    public interface ICommDispatcher : IDisposable
    {
        void Add(IServerCommunicator communicator);
        void Process();
        event EventHandler<MessageRecievedEventArgs> MessageRecieved;
        event EventHandler<ClientLogOutEventArgs> ClientLogOut;
    }
}