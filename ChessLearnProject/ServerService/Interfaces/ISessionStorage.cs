﻿using System.Collections.Generic;

namespace ServerService.Interfaces
{
    public interface ISessionStorage
    {
        List<IGameSession> AllSessions { get; }
        void AddSesssions(IGameSession session);
        void Remove(IGameSession session);
        IGameSession GetSessionOf(IClient client);
        IGameSession GetSessionBy(string id);
    }
}