﻿using System;
using SamePorjLibrary.Interfaces;
using ServerService.Concrete.Client;
using ServerService.Interfaces;

namespace ServerService.Abstract
{
    public class CommDispatcherBase : ICommDispatcher
    {
        protected IDependencyResolver DI;

        public CommDispatcherBase(IDependencyResolver DI)
        {
            this.DI = DI;
            MessageRecieved = (s, a) => { };
        }

        public virtual void Add(IServerCommunicator communicator)
        {
            new UnauthorisedClient(communicator);
        }

        public virtual void Process()
        {
        }

        public event EventHandler<MessageRecievedEventArgs> MessageRecieved;
        public event EventHandler<ClientLogOutEventArgs> ClientLogOut = (s, a) => { };


        public void Dispose()
        {
        }

        protected virtual void OnMessageRecieved(IServerCommunicator communicator, string message)
        {
            MessageRecieved(this, new MessageRecievedEventArgs
            {
                Client = communicator.Owner,
                Message = message
            });
        }

        protected virtual void OnClientLogOut(ClientLogOutEventArgs e)
        {
            ClientLogOut(this, e);
        }
    }
}