﻿using SamePorjLibrary.Abstract;
using SamePorjLibrary.Interfaces;
using ServerService.Interfaces;

namespace ServerService.Abstract
{
    public class MessageHandlerServerBase : MessageHandlerBase, IServerMessageHandler
    {
        protected INotifyer _notifyer;

        public MessageHandlerServerBase(string MessageType, IDependencyResolver DI) : base(MessageType, DI)
        {
            _notifyer = DI.Get<INotifyer>();
        }

        public virtual void HandleMessage(string message, IClient client)
        {
            HandleMessage(message, client.Communicator);
        }
    }
}