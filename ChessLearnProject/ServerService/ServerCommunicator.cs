﻿using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using SamePorjLibrary;
using ServerService.Interfaces;

namespace ServerService
{
    public class ServerCommunicator : TcpCommunicatorBase, IServerCommunicator
    {
        public ServerCommunicator(TcpClient client, Encoding enc) : base(client, enc)
        {
        }

        public override void SendMessage(string text)
        {
            base.SendMessage(text);
            Debug.Print("From server");
        }

        public override bool Connect(string adres, int port)
        {
            return false;
        }

        public override void ConnectAsync(string adres, int port)
        {
        }

        public IClient Owner { get; set; }
    }
}