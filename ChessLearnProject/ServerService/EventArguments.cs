﻿using System;
using ServerService.Interfaces;

namespace ServerService
{
    public class ClientTryLogInEventArgs : EventArgs
    {
        public IServerCommunicator Communicator;
        public string Login;
        public string Password;
    }

    public class ClientLogInEventArgs : EventArgs
    {
        public IClient client;
    }

    public class ClientLogOutEventArgs : EventArgs
    {
        public IClient Client;
    }

    public class MessageRecievedEventArgs : EventArgs
    {
        public IClient Client;
        public string Message;
    }

    public class ClientTryRegisterEventArgs : EventArgs
    {
        public IServerCommunicator Communicator;
        public string Login;
        public string Password;
    }
}