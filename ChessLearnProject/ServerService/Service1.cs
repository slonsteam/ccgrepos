﻿using System;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;

namespace ServerService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            GlobalCore.DependencyResolver.Get<TcpListener>().Start();
            AcceptTcpClientAsync();
        }

        private static void AcceptTcpClientAsync()
        {
            var listener = GlobalCore.DependencyResolver.Get<TcpListener>();
            listener.BeginAcceptTcpClient(AsyncCallback, listener);
        }

        protected static void AsyncCallback(IAsyncResult ar)
        {
            var listener = (TcpListener) ar.AsyncState;
            var client = listener.EndAcceptTcpClient(ar);

            var communicator = new ServerCommunicator(client, GlobalCore.DependencyResolver.Get<Encoding>());
            //Program.Comunicators.Add(communicator);
            communicator.BeginListenForMessage();
            communicator.MessageReaded += (s, a) =>
            {
                communicator.SendMessage($"Message \"{a.Message}\" recieved");
                communicator.EndListenForMessage();
            };

            AcceptTcpClientAsync();
        }

        protected override void OnStop()
        {
            GlobalCore.DependencyResolver.Get<TcpListener>().Stop();
        }
    }
}