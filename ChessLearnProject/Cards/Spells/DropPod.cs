﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;

namespace Cards.Spells
{
    [Export(typeof(Card))]
    public class DropPod : Spell
    {
        public DropPod()
        {
            Name = "Удачно приземлившийся дроп-под";
            Description = "Наносит 5 урона цели";
            Cost = BaseCost = 5;
            Damage = BaseDamage = 5;
            ImageSource += "dropdod.jpeg";
            Identificator = nameof(DropPod);
        }

        public override Card Initialize()
        {
            return this;
        }

        public override void Do(List<Card> targets)
        {
            foreach (var target in targets) Engine.Actions.Delay(new CreateDamage(Damage, target));
        }

        public override IEnumerable<Card> GetTargets(Table table)
        {
            return table.Cards.Where(x => x.Location == Location.Table);
        }
    }
}