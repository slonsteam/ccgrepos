﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;

namespace Cards.Spells
{
    [Export(typeof(Card))]
    public class Commissar : Spell
    {
        public Commissar()
        {
            Name = "Коммисар";
            Description = "Вы сбрасываете карту, противник сбрасывает карту";
            Cost = BaseCost = 3;
            ImageSource += "commissar.jpg";
            Identificator = nameof(Commissar);
        }

        public override Card Initialize()
        {
            return this;
        }

        public override void Do(List<Card> targets)
        {
            var rand = new Random();
            var firstPlayerHand = Engine.Table.Players.First().Cards.FromLocation(Location.InHand);
            var secondPlayerHand = Engine.Table.Players.Last().Cards.FromLocation(Location.InHand);

            var cardFromFirstPlayer = firstPlayerHand[rand.Next(0, firstPlayerHand.Count)];
            var cardFromSecondPlayer = secondPlayerHand[rand.Next(0, secondPlayerHand.Count)];

            Engine.Actions.Delay(new RemoveCard(cardFromFirstPlayer));
            Engine.Actions.Delay(new RemoveCard(cardFromSecondPlayer));
        }
    }
}