﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;

namespace Cards.Spells
{
    [Export(typeof(Card))]
    public class ReinforcementsOrbit : Spell
    {
        public ReinforcementsOrbit()
        {
            Name = "Подкрепление с орбиты";
            Description = "Берите 2 карты";
            Identificator = nameof(ReinforcementsOrbit);
            Cost = BaseCost = 3;
            ImageSource += "ReinforcementsOrbit.jpg";
        }

        public override Card Initialize()
        {
            return this;
        }

        public override void Do(List<Card> targets)
        {
            Engine.Actions.Delay(new GiveCard(PlayerModel));
            Engine.Actions.Delay(new GiveCard(PlayerModel));
        }

        public override IEnumerable<Card> GetTargets(Table table)
        {
            return new List<Card>();
        }
    }
}