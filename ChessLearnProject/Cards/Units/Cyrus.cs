﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Contract.Abilities;
using Contract.Abilities.States;
using Contract.Cards;
using Contract.Core;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Cyrus : Unit
    {
        public Cyrus()
        {
            Identificator = nameof(Cyrus);
            Name = "Цирус";
            Cost = BaseCost = 4;
            Damage = BaseDamage = 2;
            Health = BaseHealth = 2;
            Description = "Маскировка. Дает всем разведчикам +1/+1";
            ImageSource += "Cyrus.jpg";
        }

        public override Card Initialize()
        {
            var targets = PlayerModel.Cards.FromLocation(Location.Table).Where(x => x.Group == "Scout").ToList();
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                new Сamouflage(),
                new Aura(new Buff(this) {Health = h => h + 1, Damage = d => d + 1}, targets)
            });
            return this;
        }
    }
}