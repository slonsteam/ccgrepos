﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Leviathan : Unit
    {
        public Leviathan()
        {
            Identificator = nameof(Leviathan);
            Name = "Левиафан";
            Cost = BaseCost = 7;
            Damage = BaseDamage = 5;
            Health = BaseHealth = 8;
            ImageSource += "Левиафан.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}