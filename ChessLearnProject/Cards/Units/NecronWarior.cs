﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class NecronWarior : Unit
    {
        public NecronWarior()
        {
            Identificator = nameof(NecronWarior);
            Name = "Воин некронов";
            Cost = BaseCost = 4;
            Damage = BaseDamage = 2;
            Health = BaseHealth = 3;
            ImageSource += "Воин некронов.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}