﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class KruutReapers : Unit
    {
        public KruutReapers()
        {
            Identificator = nameof(KruutReapers);
            Name = "Крууты жнецы";
            Cost = BaseCost = 2;
            Damage = BaseDamage = 4;
            Health = BaseHealth = 4;
            ImageSource += "Крууты жнецы.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}