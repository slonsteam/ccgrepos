﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Abilities.Effects;
using Contract.Abilities.States;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;
using Attack = Contract.Abilities.Attack;
using CounterAttack = Contract.Abilities.CounterAttack;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Tankred : Unit
    {
        public Tankred()
        {
            Name = "Танкред";
            Description = "Предсмертный хрип: призывает раненного танкреда";
            Health = BaseHealth = 5;
            BaseCost = Cost = 7;
            Damage = BaseDamage = 5;
            Identificator = "Tankred";
            ImageSource = "pack://application:,,,/UI/Content/Images/Units/tankred.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                new Deathrattle(new List<BaseAction>
                {
                    new DeployCard(PlayerModel, new WoundedTankred())
                })
            });
            return this;
        }
    }

    [Export(typeof(Card))]
    public class WoundedTankred : Unit
    {
        public WoundedTankred()
        {
            Identificator = nameof(WoundedTankred);
            Description = "Провокация";
            Health = BaseHealth = 3;
            BaseCost = Cost = 3;
            Damage = BaseDamage = 3;
            ImageSource = "pack://application:,,,/UI/Content/Images/Units/woundedtankred.jpg";
            Location = Location.Core;
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                new Provocation()
            });
            return this;
        }
    }
}