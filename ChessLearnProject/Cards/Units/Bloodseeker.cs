﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Bloodseeker : Unit
    {
        public Bloodseeker()
        {
            Identificator = nameof(Bloodseeker);
            Name = "Кровопускатели";
            Cost = BaseCost = 6;
            Damage = BaseDamage = 5;
            Health = BaseHealth = 7;
            ImageSource += "Кровопускатели.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}