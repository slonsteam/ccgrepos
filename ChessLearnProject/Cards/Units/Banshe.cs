﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Banshe : Unit
    {
        public Banshe()
        {
            Identificator = nameof(Banshe);
            Name = "Воющая баньша";
            Cost = BaseCost = 5;
            Damage = BaseDamage = 3;
            Health = BaseHealth = 5;
            ImageSource += "Воющая баньша.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}