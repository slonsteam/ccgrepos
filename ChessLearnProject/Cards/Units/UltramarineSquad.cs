﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class UltramarineSquad : Unit
    {
        public UltramarineSquad()
        {
            Identificator = nameof(UltramarineSquad);
            Name = "Отряд ультрамаринов";
            Cost = BaseCost = 3;
            Damage = BaseDamage = 4;
            Health = BaseHealth = 4;
            ImageSource += "Отряд ультрамаринов.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}