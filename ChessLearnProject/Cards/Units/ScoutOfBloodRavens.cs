﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Contract.Abilities;
using Contract.Abilities.Effects;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;
using Attack = Contract.Abilities.Attack;
using CounterAttack = Contract.Abilities.CounterAttack;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class ScoutOfBloodRavens : Unit
    {
        public ScoutOfBloodRavens()
        {
            Identificator = nameof(ScoutOfBloodRavens);
            Cost = BaseCost = 2;
            BaseDamage = Damage = 2;
            Health = BaseHealth = 2;
            ImageSource += "ScoutOfBloodRavens.jpg";
            Name = "Разведчик кровавых воронов";
            Description = "Боевой клич: наносит 1 урон выбранной карте";
            Group = "Scout";
        }

        public override IEnumerable<Card> GetTargets(Table table)
        {
            if (Location == Location.InHand)
                return table.Cards.Where(x => x.Location == Location.Table);
            return base.GetTargets(table);
        }

        public override Card Initialize()
        {
            var actions = new List<BaseAction>();
            var battlecry = new BattleCry(actions);
            //todo костыль
            actions.Add(new CreateDamage(1, PlayerModel.Engine.Targets.First()));
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                battlecry
            });
            return this;
        }
    }
}