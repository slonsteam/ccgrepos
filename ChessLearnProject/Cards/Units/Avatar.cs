﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Avatar : Unit
    {
        public Avatar()
        {
            Identificator = nameof(Avatar);
            Name = "Аватар";
            Cost = BaseCost = 10;
            Damage = BaseDamage = 6;
            Health = BaseHealth = 10;
            ImageSource += "Аватар.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}