﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Abilities.States;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Ogre : Unit
    {
        public Ogre()
        {
            Name = "Ogre-mag";
            Description = "Ogre mag";
            Health = BaseHealth = 4;
            BaseCost = Cost = 1;
            Damage = BaseDamage = 1;
            Identificator = nameof(Ogre);
            ImageSource = "pack://application:,,,/UI/Content/Images/Units/cosmo.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new DamageToSelf(),
                new Attack(),
                new CounterAttack(),
                new Provocation()
            });
            return this;
        }
    }

    [Export(typeof(Card))]
    public class Cosmodesant : Unit
    {
        public Cosmodesant()
        {
            Name = "Cosmodesant";
            Description = "Cosmo";
            BaseHealth = 4;
            Health = BaseHealth;
            Cost = 2;
            Damage = 2;
            Identificator = "Cosmo";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new DamageToSelf(),
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }

    [Export(typeof(Card))]
    public class Warlock : Hero
    {
        public Warlock()
        {
            Name = "Warlocks";
            Identificator = "Warlock";
            BaseHealth = 30;
            Health = BaseHealth;
            ImageSource = "pack://application:,,,/UI/Content/Images/gabriel.png";
        }

        public override Card Initialize()
        {
            return this;
        }
    }
}