﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class MechSentient : Unit
    {
        public MechSentient()
        {
            Identificator = nameof(MechSentient);
            Name = "Механический часовой";
            Cost = BaseCost = 1;
            Damage = BaseDamage = 2;
            Health = BaseHealth = 2;
            ImageSource += "Механический часовой.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}