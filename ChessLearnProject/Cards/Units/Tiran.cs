﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Contract.Abilities;
using Contract.Cards;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Tiran : Unit
    {
        public Tiran()
        {
            Identificator = nameof(Tiran);
            Name = "Тиран улья";
            Cost = BaseCost = 9;
            Damage = BaseDamage = 6;
            Health = BaseHealth = 8;
            ImageSource += "Тиран улья.jpg";
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack()
            });
            return this;
        }
    }
}