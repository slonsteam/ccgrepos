﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Contract.Abilities;
using Contract.Abilities.Effects;
using Contract.Abilities.States;
using Contract.Actions;
using Contract.Cards;
using Contract.Core;
using Contract.Emmiter;
using TestGameLogic.Events;
using Attack = Contract.Abilities.Attack;
using CounterAttack = Contract.Abilities.CounterAttack;

namespace Cards.Units
{
    [Export(typeof(Card))]
    public class Genestealer : Unit
    {
        public Genestealer()
        {
            Cost = BaseCost = 3;
            Damage = BaseDamage = 6;
            Health = BaseHealth = 5;
            Description = "Дает противнику двух рабов генокрада";
            ImageSource += "genstealer.jpg";
            Identificator = nameof(Genestealer);
        }

        public override Card Initialize()
        {
            var player = PlayerModel.Engine.Table.Players.First(x => x.Token != PlayerModel.Token);
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                new BattleCry(new List<BaseAction>
                {
                    new DeployCard(player, new GenestealerSlave()),
                    new DeployCard(player, new GenestealerSlave())
                })
            });
            return this;
        }
    }

    [Export(typeof(Card))]
    public class GenestealerSlave : Unit
    {
        public GenestealerSlave()
        {
            Cost = BaseCost = 2;
            Damage = BaseDamage = 2;
            Health = BaseHealth = 6;
            Name = "Раю генокрада";
            Description = "Неистовство ветра, переходит, когда получает урон в первый раз";
            ImageSource += "genstealerslave.jpg";
            Identificator = nameof(GenestealerSlave);
            Location = Location.Core;
        }

        public override Card Initialize()
        {
            Abilities.Add(new List<Ability>
            {
                new Attack(),
                new CounterAttack(),
                new WindRage(),
                new GenstealerSlaveAbility()
            });
            return this;
        }
    }

    public class GenstealerSlaveAbility : Ability, IListener<After<CreateDamage>>
    {
        private bool _isused;

        public void On(After<CreateDamage> ev)
        {
            if (ev.Action.Target.Token == Source.Token && !_isused && (Source as Unit).Health > 0)
            {
                _isused = true;
                var player = Engine.Table.Players.FirstOrDefault(x => x.Token != Source.PlayerModel.Token);
                (Source as Unit).Abilities.Get<Attack>().Quantity = 0;
                Engine.Actions.Delay(new RemoveCard(Source, Location.Core));
                Engine.Actions.Delay(new DeployCard(player, Source));
            }
        }
    }
}