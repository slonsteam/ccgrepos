﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Client.UI.UserControls;
using Client.ViewModels;

namespace Client.UI.Windows
{
    /// <summary>
    ///     Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        private Card _attacksource;

        //карта на которую перетаскивается карта из колоды
        private int _currentcardindex;

        private Window _dragdropWindow;

        public GamePage()
        {
            InitializeComponent();
        }

        public GamePageViewModel ViewModel => DataContext as GamePageViewModel;

        private void SetAttackCursor()
        {
            var sri = Application.GetResourceStream(
                new Uri("pack://application:,,,/UI/Content/Cursors/Attack! Link Select.ani"));
            var customCursor = new Cursor(sri.Stream);
            Cursor = customCursor;
        }

        private void CancelAttack()
        {
            ViewModel.IsAttack = false;
            ViewModel.ResetTargets();
            ViewModel.Source = null;
            Cursor = Cursors.Arrow;
        }

        private void OnTargetClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (!ViewModel.IsAttack)
                {
                    _attacksource = sender as Card;
                    ViewModel.Source = _attacksource.ViewModel;
                    ViewModel.ActionController.GetTargets(_attacksource.ViewModel.Token);
                }
                else
                {
                    if (ViewModel.Source != null && sender is Card target && target.ViewModel.CanBeTarget)
                        ViewModel.ActionController.PlayCard(ViewModel.Source.Token,
                            new List<string> {target.ViewModel.Token});
                    CancelAttack();
                }
            }
            catch (Exception d)
            {
                Debug.WriteLine("Ошибка в UI при атаке" + d.Message);
            }
        }


        private void GamePage_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel.IsAttack) CancelAttack();
        }

        private void GamePage_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) ViewModel.MenuActive = !ViewModel.MenuActive;
        }

        private void GamePage_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) ViewModel.MenuActive = !ViewModel.MenuActive;
        }

        #region DragAndDrop

        protected void CardList_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!ViewModel.IsAttack && sender is Card card && card.ViewModel.CanBePlayed)
            {
                // получаем коэффициент масштабирования
                var child = VisualTreeHelper.GetChild(CardsViewBox, 0) as ContainerVisual;
                var scale = child.Transform as ScaleTransform;
                var scalecopy = new ScaleTransform(scale.ScaleX + 0.2, scale.ScaleY + 0.2, 0, 100);

                CreateDragDropWindow(card, scalecopy);
                card.ViewModel.IsRemoved = true;
                ViewModel.Source = card.ViewModel;
                var result = DragDrop.DoDragDrop(card, card.ViewModel, DragDropEffects.Move);

                //закрываем окно
                if (result == DragDropEffects.None)
                    card.ViewModel.IsRemoved = false;
                _dragdropWindow.Close();
                _dragdropWindow = null;
            }
        }

        public void TargetsRecievedHandler(object sender, CardViewModel e)
        {
            if (!ViewModel.Targets.Any())
            {
                if (e.CanBePlayed)
                    ViewModel.ActionController.PlayCard(e.Token, new List<string>());
            }
            else
            {
                SetAttackCursor();
                ViewModel.Source = e;
                ViewModel.IsAttack = true;
            }
        }

        protected void CardList_Drop(object sender, DragEventArgs e)
        {
            //var pont = e.GetPosition(MyUnitsViewBox);
            var droppedData = e.Data.GetData(typeof(CardViewModel)) as CardViewModel;
            //todo 
            ViewModel.DraggedCard = droppedData;
            ViewModel.ActionController.GetTargets(droppedData.Token);

            //вставляем карту в зависимости от положения курсора
            //if (sender is Card card)
            //{
            //    ViewModel.PlayerModel.UnitsOnTable.Insert(ViewModel.PlayerModel.UnitsOnTable.IndexOf(card.ViewModel), droppedData);
            //    return;
            //}
            //if (pont.X >= MyUnitsViewBox.ActualWidth)
            //    ViewModel.PlayerModel.UnitsOnTable.Add(droppedData);
            //if (pont.X <= 0)
            //    ViewModel.PlayerModel.UnitsOnTable.Insert(0, droppedData);
        }

        private void CardList_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            // update the position of the visual feedback item
            var w32Mouse = new POINT();
            GetCursorPos(out w32Mouse);
            var wpfpoint = Application.Current.MainWindow.PointFromScreen(new Point(w32Mouse.X, w32Mouse.Y));
            _dragdropWindow.Left = wpfpoint.X;
            _dragdropWindow.Top = wpfpoint.Y;
        }

        private void CreateDragDropWindow(Visual dragElement, Transform transform, bool show = true)
        {
            //создание окна для перетаскивания
            _dragdropWindow = new Window();
            _dragdropWindow.WindowStyle = WindowStyle.None;
            _dragdropWindow.AllowsTransparency = true;
            _dragdropWindow.AllowDrop = false;
            _dragdropWindow.Background = null;
            _dragdropWindow.IsHitTestVisible = false;
            _dragdropWindow.SizeToContent = SizeToContent.WidthAndHeight;
            _dragdropWindow.Topmost = false;
            _dragdropWindow.Owner = Application.Current.MainWindow;
            _dragdropWindow.ShowInTaskbar = false;

            var r = new Rectangle();
            r.Width = ((FrameworkElement) dragElement).RenderSize.Width;
            r.Height = ((FrameworkElement) dragElement).RenderSize.Height;

            r.RenderTransform = transform;

            //todo неполное клонирование, подумать как сделать лучше
            //создаем клон элемента 
            var dragcopy = new Card();
            var copied = dragElement as Card;
            dragcopy.ViewModel = copied.ViewModel;
            dragcopy.MaxWidth = copied.MaxWidth;
            dragcopy.MaxHeight = copied.MaxHeight;
            dragcopy.RenderTransform = new MatrixTransform(copied.RenderTransform.Value);
            dragcopy.ShowDescription = copied.ShowDescription;
            dragcopy.ManaVisible = copied.ManaVisible;

            r.Fill = new VisualBrush(dragcopy);
            _dragdropWindow.Content = r;

            //получаем координаты мыши 
            var w32Mouse = new POINT();
            GetCursorPos(out w32Mouse);
            var wpfpoint = Application.Current.MainWindow.PointFromScreen(new Point(w32Mouse.X, w32Mouse.Y));

            _dragdropWindow.Left = wpfpoint.X;
            _dragdropWindow.Top = wpfpoint.Y;
            if (show)
                _dragdropWindow.Show();
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out POINT lpPoint);

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                X = x;
                Y = y;
            }
        }


        private void Grid_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            //if (_currentcardindex != 0 && _currentcardindex + 1 != ViewModel.PlayerModel.UnitsOnTable.Count)
            //{
            //    var card = MyUnitItemsControl.Items[_currentcardindex];
            //}
        }

        private void OnMouseEnterCard(object sender, MouseEventArgs e)
        {
            //var card = sender as ContentPresenter;

            //_currentcardindex = ViewModel.PlayerModel.UnitsOnTable.IndexOf(card.Content as CardViewModel);
        }

        private void CardDragEnter(object sender, DragEventArgs e)
        {
            //var card = sender as Card;
            //_currentcardindex = ViewModel.PlayerModel.UnitsOnTable.IndexOf(card.ViewModel);
        }

        private void CardDragOver(object sender, DragEventArgs e)
        {
            //_currentcardindex = -1;
        }

        #endregion
    }
}