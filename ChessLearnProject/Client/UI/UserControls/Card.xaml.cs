﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Client.ViewModels;

namespace Client.UI.UserControls
{
    /// <summary>
    ///     Interaction logic for Card.xaml
    /// </summary>
    public partial class Card : UserControl
    {
        public static readonly DependencyProperty ShowDescriptionDependencyProperty =
            DependencyProperty.Register(nameof(ShowDescription), typeof(bool), typeof(Card));

        public static readonly DependencyProperty ShowNameDependencyProperty =
            DependencyProperty.Register(nameof(ShowName), typeof(bool), typeof(Card), new PropertyMetadata(true));

        public static readonly DependencyProperty ManaVisibleDependencyProperty =
            DependencyProperty.Register(nameof(ManaVisible), typeof(bool), typeof(Card), new PropertyMetadata(true));

        public static readonly DependencyProperty HealthVisibleDependencyProperty =
            DependencyProperty.Register(nameof(HealthVisible), typeof(bool), typeof(Card), new PropertyMetadata(true));

        public static readonly DependencyProperty DamageVisibleDependencyProperty =
            DependencyProperty.Register(nameof(DamageVisible), typeof(bool), typeof(Card), new PropertyMetadata(true));

        public static readonly DependencyProperty ViewModelDependencyProperty =
            DependencyProperty.Register(nameof(ViewModel), typeof(CardViewModel), typeof(Card));

        public static readonly DependencyProperty IconSizeDependencyProperty =
            DependencyProperty.Register(nameof(IconSize), typeof(int), typeof(Card), new PropertyMetadata(25));

        public static readonly DependencyProperty HealthColorDependencyProperty =
            DependencyProperty.Register(nameof(HealthColor), typeof(SolidColorBrush), typeof(Card),
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb(193, 223, 208))));

        public static readonly DependencyProperty DamageColorDependencyProperty =
            DependencyProperty.Register(nameof(DamageColor), typeof(SolidColorBrush), typeof(Card),
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb(193, 223, 208))));


        public Card()
        {
            InitializeComponent();
        }

        public SolidColorBrush HealthColor
        {
            get => (SolidColorBrush) GetValue(HealthColorDependencyProperty);
            set => SetValue(HealthColorDependencyProperty, value);
        }

        public SolidColorBrush DamageColor
        {
            get => (SolidColorBrush) GetValue(DamageColorDependencyProperty);
            set => SetValue(DamageColorDependencyProperty, value);
        }

        public bool ShowDescription
        {
            get => (bool) GetValue(ShowDescriptionDependencyProperty);
            set => SetValue(ShowDescriptionDependencyProperty, value);
        }

        public bool ShowName
        {
            get => (bool) GetValue(ShowNameDependencyProperty);
            set => SetValue(ShowNameDependencyProperty, value);
        }

        public bool ManaVisible
        {
            get => (bool) GetValue(ManaVisibleDependencyProperty);
            set => SetValue(ManaVisibleDependencyProperty, value);
        }

        public bool HealthVisible
        {
            get => (bool) GetValue(HealthVisibleDependencyProperty);
            set => SetValue(HealthVisibleDependencyProperty, value);
        }

        public bool DamageVisible
        {
            get => (bool) GetValue(DamageVisibleDependencyProperty);
            set => SetValue(DamageVisibleDependencyProperty, value);
        }

        public int IconSize
        {
            get => (int) GetValue(IconSizeDependencyProperty);
            set => SetValue(IconSizeDependencyProperty, value);
        }

        public CardViewModel ViewModel
        {
            get => (CardViewModel) GetValue(ViewModelDependencyProperty);
            set => SetValue(ViewModelDependencyProperty, value);
        }
    }
}