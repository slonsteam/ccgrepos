﻿using System;
using Client.Interfaces;
using SamePorjLibrary.Abstract;
using SamePorjLibrary.Interfaces;

namespace Client.Abstract
{
    public class ClientMessageHandlerBase : MessageHandlerBase, IClientMessageHandler
    {
        public ICommunicator Communicator;

        public ClientMessageHandlerBase(string MessageType, IDependencyResolver DI, ICommunicator communicator) : base(
            MessageType, DI)
        {
            Communicator = communicator;
        }

        public event EventHandler<MessageHandledEventArgs> MessageHandled;

        public virtual void HandleMessage(string message)
        {
            if (HandleMessage(message, Communicator))
                OnMessageHandled(message);
        }

        protected virtual void OnMessageHandled(string message)
        {
            MessageHandled?.Invoke(this, new MessageHandledEventArgs
            {
                Message = message
            });
        }
    }
}