﻿using System.Collections.Generic;
using System.Linq;
using CommonDll.Data;
using SamePorjLibrary.Data;

namespace Client
{
    public class CardModelRepos : IRepository<CardModel>
    {
        public IEnumerable<CardModel> Collection { get; set; }

        public CardModel Get(string id)
        {
            return Collection.FirstOrDefault(x => x.Identificator == id);
        }
    }
}