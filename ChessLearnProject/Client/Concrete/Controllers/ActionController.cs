﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Client.ViewModels;
using CommonDll.Data;
using Newtonsoft.Json;
using SamePorjLibrary.Data;
using SamePorjLibrary.Model;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Client.Concrete.Controllers
{
    public class ActionController
    {
        private readonly IRepository<string> _mydeck;
        public GamePageViewModel _viewModel;
        private readonly GamePocessor GameController;

        public ActionController(IRepository<string> deck, IRepository<CardModel> cards, GamePocessor controller)
        {
            //todo костыль
            _mydeck = deck;
            Cards = cards;
            GameController = controller;
        }

        public IRepository<CardModel> Cards { get; set; }

        public void Handle(List<ActionModel> actions)
        {
            foreach (var action in actions)
                if (action.Status == ActionModelStatus.Start)
                    Handle(action);
        }

        public void Handle(ActionModel action)
        {
            try
            {
                switch (action.ActionName)
                {
                    case "StartGame":
                        StartGame(action.Parametrs);
                        break;
                    case "EndGame":
                        EndGame(action.Parametrs);
                        break;
                    case "GiveCard":
                        GiveCard(action.Parametrs, action.Failed);
                        break;
                    case "RemoveCard":
                        RemoveCard(action.Parametrs, action.Failed);
                        break;
                    case "GiveMana":
                        GiveMana(action.Parametrs);
                        break;
                    case "CardPlay":
                        CardPlay(action.Parametrs);
                        break;
                    case "CanBePlayed":
                        CanBePlayed(action.Parametrs);
                        break;
                    case "CreateDamage":
                        CreateDamage(action.Parametrs, action.Failed);
                        break;
                    case "PayResources":
                        PayResources(action.Parametrs);
                        break;
                    case "DeployCard":
                        DeployCard(action.Parametrs, action.Failed);
                        break;
                    case "EndTurn":
                        EndTurn(action.Parametrs);
                        break;
                    case "BeginTurn":
                        BeginTurn(action.Parametrs);
                        break;
                    case "Death":
                        Death(action.Parametrs);
                        break;
                    case "AddBuff":
                        AddBuff(action.Parametrs);
                        break;
                    case "RemoveBuff":
                        RemoveBuff(action.Parametrs);
                        break;
                    case "GetTable":
                        GetTable(action.Parametrs);
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Ошибка в обработке экшнов - {action.ActionName} " + e.Message);
            }
        }

        public void GetTable(Dictionary<string, object> actionParametrs)
        {
            void func(IEnumerable<dynamic> cards, PlayerModel player)
            {
                foreach (var card in cards)
                {
                    var cardm = Cards.Get(card.Identificator);
                    var newcard =
                        new CardViewModel(card.Token, player, cardm)
                        {
                            Health = card.Health,
                            CurrentBaseHealth = card.BaseHealth,
                            CurrentBaseDamage = card.BaseDamage,
                            Damage = card.Damage
                        };
                    if (cardm.Type == "Hero")
                        player.Hero = newcard;
                    else
                        player.UnitsOnTable.Add(newcard);
                }
            }

            var unitlist = new[]
            {
                new
                {
                    Identificator = "",
                    Token = "",
                    BaseHealth = 0,
                    Health = 0,
                    Damage = 0,
                    BaseDamage = 0
                }
            }.ToList();
            var cardlist = new[]
            {
                new
                {
                    Identificator = "",
                    Token = ""
                }
            };
            var mydeck = JsonConvert.DeserializeAnonymousType(actionParametrs["CardsInDeck"].ToString(), cardlist);
            var myhand = JsonConvert.DeserializeAnonymousType(actionParametrs["CardsHand"].ToString(), cardlist);
            var enemyhand = JsonConvert.DeserializeObject<List<string>>(actionParametrs["CardsHandEnemy"].ToString());
            var enemydeck = JsonConvert.DeserializeObject<List<string>>(actionParametrs["CardsInDeckEnemy"].ToString());
            var myunits = JsonConvert.DeserializeAnonymousType(actionParametrs["CardsTable"].ToString(), unitlist);
            var enemyunits =
                JsonConvert.DeserializeAnonymousType(actionParametrs["EnemyCardsTable"].ToString(), unitlist);
            var myplayer = actionParametrs["Player"].ToString();
            var enemy = actionParametrs["EnemyPlayer"].ToString();
            _viewModel.PlayerModel = new PlayerModel(myplayer);
            _viewModel.PlayerModel.IsMyTurn = _viewModel.PlayerModel.Token != enemy;
            _viewModel.EnemyPlayerModel = new PlayerModel(enemy);
            foreach (var card in myhand)
                _viewModel.PlayerModel.CardsInHand.Add(new CardViewModel(card.Token, _viewModel.PlayerModel,
                    Cards.Get(card.Identificator)));
            foreach (var card in enemyhand)
                _viewModel.EnemyPlayerModel.CardsInHand.Add(new CardViewModel(card, _viewModel.PlayerModel));
            func(myunits, _viewModel.PlayerModel);
            func(enemyunits, _viewModel.EnemyPlayerModel);
            //todo костыль из-за того что тесная привязка к отправленной колоде
            foreach (var card in mydeck)
                _viewModel.PlayerModel.Deck.Add(new CardViewModel(card.Token, _viewModel.PlayerModel,
                    Cards.Get(card.Identificator)));
            foreach (var card in enemydeck)
                _viewModel.EnemyPlayerModel.Deck.Add(new CardViewModel(card, _viewModel.EnemyPlayerModel));
        }

        private void RemoveBuff(Dictionary<string, object> actionParametrs)
        {
            var health = Convert.ToInt16(actionParametrs["Health"]);
            var damage = Convert.ToInt16(actionParametrs["Damage"]);
            var cardtoken = actionParametrs["Card"].ToString();

            var card = _viewModel.AllCards.FirstOrDefault(x => x.Token == cardtoken);
            if (card != null)
            {
                card.CurrentBaseDamage -= damage;
                card.Damage -= damage;
                card.CurrentBaseHealth -= health;
            }
        }

        private void AddBuff(Dictionary<string, object> actionParametrs)
        {
            var health = Convert.ToInt16(actionParametrs["Health"]);
            var damage = Convert.ToInt16(actionParametrs["Damage"]);
            var cardtoken = actionParametrs["Card"].ToString();

            var card = _viewModel.AllCards.FirstOrDefault(x => x.Token == cardtoken);
            if (card != null)
            {
                card.CurrentBaseDamage += damage;
                card.Damage += damage;
                card.Health += health;
                card.CurrentBaseHealth += health;
            }
        }

        private void RemoveCard(Dictionary<string, object> actionParametrs, bool actionFailed)
        {
            var playertoken = actionParametrs["Player"].ToString();
            var cardtoken = actionParametrs["Card"].ToString();


            var player = GameCurentPlayer(playertoken);
            var card = player.AllCards.FirstOrDefault(x => x.Token == cardtoken);


            if (!player.CardsInHand.Contains(card))
            {
                player.UnitsOnTable.Remove(card);
            }
            else
            {
                card.IsRemoved = true;
                //костыль
                player.CardsInHand.Remove(card);
            }
        }

        public void CanBePlayed(Dictionary<string, object> actionParametrs)
        {
            var canbeplayed = JsonConvert.DeserializeObject<List<string>>(actionParametrs["CanBePlayed"].ToString());
            var player = _viewModel.PlayerModel;
            if (canbeplayed != null)
                foreach (var card in player.AllCards)
                    card.CanBePlayed = !(canbeplayed.FirstOrDefault(x => x == card.Token) is null);
        }

        private void EndGame(Dictionary<string, object> actionParametrs)
        {
            var winner = actionParametrs["Winner"].ToString();
            var result = "";
            if (winner == "DRAW")
            {
                result = "НИЧЬЯ";
            }
            else
            {
                var player = GameCurentPlayer(winner);
                result = _viewModel.PlayerModel == player ? "ВЫ ВЫИГРАЛИ" : "ВЫ ПРОИГРАЛИ";
            }

            var res = MessageBox.Show(Application.Current.MainWindow, result, "Результат",
                (Style) Application.Current.MainWindow.FindResource("MessageBoxStyle1"));
            var mvm = GlobalCore.DependencyResolver.Get<MainViewModel>();
            mvm.CurrentPage = mvm.StartPage;
        }

        private void BeginTurn(Dictionary<string, object> actionParametrs)
        {
            var tokenplayer = actionParametrs["Player"].ToString();
            var player = GameCurentPlayer(tokenplayer);
            player.IsMyTurn = true;
        }

        private void Death(Dictionary<string, object> actionParametrs)
        {
            var playertoken = actionParametrs["Player"].ToString();
            var cardtoken = actionParametrs["Card"].ToString();

            var player = GameCurentPlayer(playertoken);
            var card = player.UnitsOnTable.FirstOrDefault(x => x.Token == cardtoken);
            card.IsRemoved = true;

            //player.UnitsOnTable.Remove(card);
        }

        private void EndTurn(Dictionary<string, object> actionParametrs)
        {
            var playertoken = actionParametrs["Player"].ToString();
            var player = GameCurentPlayer(playertoken);
            player.IsMyTurn = false;
        }

        private string States(string state)
        {
            var dir = "pack://application:,,,/UI/Content/Images/States/";
            switch (state)
            {
                case "PROVO": return dir + "provo.png";
                case "WindRage": return dir + "WindRage.png";
                case "Сamouflage": return dir + "Сamouflage.png";
            }

            return "";
        }

        private void DeployCard(Dictionary<string, object> actionParametrs, bool faield = false)
        {
            if (!faield)
            {
                var ident = actionParametrs["Identificator"].ToString();
                var cardtoken = actionParametrs["Card"].ToString();
                var playertoken = actionParametrs["Player"].ToString();
                var states = actionParametrs["States"].ToString().Split(',');
                var card = _viewModel.AllCards.FirstOrDefault(x => x.Token == cardtoken);
                var player = GameCurentPlayer(playertoken);
                var cardmodel = Cards.Get(ident);
                //todo доделать с картами не из колоды
                if (card != null)
                {
                    //проверяем чья карта выложена
                    card.Initialize(cardmodel, player);
                    player.UnitsOnTable.Add(card);
                    //var card = _viewModel.DraggedCard;
                    //playerm.CardsInHand.Remove(card);
                }
                else
                {
                    card = new CardViewModel(cardtoken, player, cardmodel);
                    player.UnitsOnTable.Add(card);
                    player.RemovedCards.Add(card);
                }

                foreach (var state in states)
                    card.States.Add(States(state));
            }
            else
            {
                _viewModel.DraggedCard.IsRemoved = false;
            }
        }

        private PlayerModel GameCurentPlayer(string token)
        {
            return _viewModel.PlayerModel.Token == token ? _viewModel.PlayerModel : _viewModel.EnemyPlayerModel;
        }

        private void StartGame(Dictionary<string, object> param)
        {
            var playercards = param["CardTokens"] as Dictionary<string, List<string>>;
            var enemyid = param["EnemyHero"].ToString();
            var queue = param["Queue"].ToString();
            _viewModel.PlayerModel.IsMyTurn = _viewModel.PlayerModel.Token == queue;
            foreach (var cards in playercards)
            {
                var deck = _mydeck.Collection.ToList();
                var player = GameCurentPlayer(cards.Key);
                var heroid = player == _viewModel.PlayerModel ? _mydeck.Collection.First() : enemyid;
                var modelhero = Cards.Get(heroid);
                player.Hero = new CardViewModel(cards.Value.First(), player)
                {
                    Health = modelhero.BaseHealth,
                    BaseHealth = modelhero.BaseHealth,
                    Type = "Hero",
                    Name = modelhero.Name,
                    ImageSource = modelhero.ImageSource
                };
                var i = 1;
                foreach (var card in cards.Value.Skip(1))
                    if (cards.Key == _viewModel.PlayerModel.Token)
                    {
                        var cardinrep = Cards.Get(deck[i]);
                        _viewModel.PlayerModel.Deck.Add(new CardViewModel(card, _viewModel.PlayerModel, cardinrep));
                        i++;
                    }
                    else
                    {
                        _viewModel.EnemyPlayerModel.Deck.Add(new CardViewModel(card, _viewModel.EnemyPlayerModel));
                    }
            }
        }

        private void GiveCard(Dictionary<string, object> param, bool failed = false)
        {
            if (!failed)
            {
                var token = param["Player"].ToString();
                var cardtoken = param["Card"].ToString();
                var player = GameCurentPlayer(token);
                var card = player.Deck.FirstOrDefault(x => x.Token == cardtoken);
                player.RemovedCards.Add(card);
                if (card != null)
                    player.CardsInHand.Add(card);
            }
        }

        private void GiveMana(Dictionary<string, object> param)
        {
            var token = param["Player"].ToString();
            var quantity = Convert.ToInt16(param["Quantity"]);
            var staticmana = Convert.ToBoolean(param["Staticmana"]);
            var player = GameCurentPlayer(token);
            if (staticmana)
                player.FullMana += quantity;
            else
                player.CurrentMana += quantity;
        }

        private void PayResources(Dictionary<string, object> param)
        {
            var quantity = Convert.ToInt16(param["Quantity"]);
            var tokenplayer = param["Player"].ToString();
            var player = GameCurentPlayer(tokenplayer);
            player.CurrentMana -= quantity;
        }

        public bool PlayCard(string source, List<string> target)
        {
            GameController.PlayCard(source, target);
            return true;
        }

        public void EndTurn()
        {
            GameController.EndTurn();
        }

        public void GetTargets(string token)
        {
            GameController.GetTargets(token);
        }

        private void CardPlay(Dictionary<string, object> param, bool failed = false)
        {
            if (!failed)
            {
                var tokenplayer = param["Player"].ToString();
                var player = GameCurentPlayer(tokenplayer);
            }
        }

        public void EndGame()
        {
            GameController.EndGame();
        }

        public void SetTarget(List<string> targets)
        {
            if (targets != null)
                foreach (var card in _viewModel.AllCards)
                    card.CanBeTarget = !(targets.FirstOrDefault(x => x == card.Token) is null);
            _viewModel.RaiseTargetsRecived(_viewModel.Source);
        }

        private void CreateDamage(Dictionary<string, object> param, bool failed)
        {
            if (!failed)
            {
                var target = param["Target"].ToString();
                var quantity = Convert.ToInt16(param["Quantity"]);
                var card = _viewModel.AllCards.FirstOrDefault(x => x.Token == target);
                card.CardAttacked();
                card.Health -= quantity;
            }
        }
    }
}