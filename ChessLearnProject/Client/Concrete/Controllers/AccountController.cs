﻿using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.Controllers
{
    public class AccountController
    {
        private readonly IDependencyResolver DI;
        private readonly IMessageEncoder LogInEncoder;
        public string Token;

        public AccountController(IDependencyResolver DI)
        {
            this.DI = DI;
            LogInEncoder = DI.Get<IDependencyResolver>("DIEncoder").Get<IMessageEncoder>("LogIn");
        }

        private ICommunicator communicator
        {
            get
            {
                if (GlobalCore.Communicator == null)
                    return DI.Get<ICommunicator>();
                return GlobalCore.Communicator;
            }
        }

        public void LogIn(string login, string password)
        {
            if (Token == null)
                communicator.SendMessage(LogInEncoder.EncodeMessage(new LogInNetworkModel(login, password)));
        }

        public void LogIn(string token)
        {
            Token = token;
            communicator.SendMessage(LogInEncoder.EncodeMessage(new LogInNetworkModel(Token)));
        }
    }
}