﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Client.Concrete.Controllers
{
    public class LocalCashController
    {
        public static LocalCashModel Model = new LocalCashModel();

        /// <summary>
        ///     Закешировать модель
        /// </summary>
        /// <param name="fileName">Имя файла с кешем</param>
        public static void GoToCash(string fileName)
        {
            File.WriteAllText(fileName, JsonConvert.SerializeObject(Model));
        }

        /// <summary>
        ///     Раскешировать модель
        /// </summary>
        /// <param name="fileName">Имя файла с кешем</param>
        public static void GoFromCash(string fileName)
        {
            Model = JsonConvert.DeserializeObject<LocalCashModel>(File.ReadAllText(fileName));
        }
    }

    public class LocalCashModel
    {
        public Dictionary<string, List<string>> Decks;
        public string Token;

        public void AddDeck(string name, ICollection<string> deck)
        {
            if (Decks == null)
                Decks = new Dictionary<string, List<string>>();
            if (Decks.ContainsKey(name))
            {
                Decks[name].Clear();
                Decks[name].AddRange(deck);
            }
            else
            {
                var newDeck = new List<string>();
                newDeck.AddRange(deck);
                Decks.Add(name, newDeck);
            }
        }
    }
}