﻿using System;
using System.Collections.Generic;
using System.Threading;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.Controllers
{
    public class GamePocessor
    {
        private readonly IMessageEncoder _endGameEncoder;
        private readonly IMessageEncoder _endTurnEncoder;
        private readonly Lazy<IMessageEncoder> _getTargetsEncoder;
        private readonly IMessageEncoder _playCardEncoder;
        private readonly IDependencyResolver DI;

        public GamePocessor(IDependencyResolver DI)
        {
            this.DI = DI;
            var diencoders = DI.Get<IDependencyResolver>("DIEncoder");
            _endTurnEncoder = diencoders.Get<IMessageEncoder>("EndTurn");
            _playCardEncoder = diencoders.Get<IMessageEncoder>("PlayCardMessage");
            _endGameEncoder = diencoders.Get<IMessageEncoder>("EndGame");
            _getTargetsEncoder = new Lazy<IMessageEncoder>(() =>
                DI.Get<IDependencyResolver>("DIEncoder").Get<IMessageEncoder>("GetTargets"));
        }

        private ICommunicator _communicator
        {
            get
            {
                if (GlobalCore.Communicator == null)
                    return DI.Get<ICommunicator>();
                return GlobalCore.Communicator;
            }
        }

        public void PlayCard(string id, List<string> targets)
        {
            var thr = new Thread(() =>
            {
                _communicator.SendMessage(
                    _playCardEncoder.EncodeMessage(new PlayCardNetworkModel {CardPlayed = id, Targets = targets}));
            });
            thr.Start();
            //GlobalCore.Communicator.SendMessage(
            //    _playCardEncoder.EncodeMessage(new PlayCardNetworkModel {CardPlayed = id, Targets = targets}));
        }

        public void EndTurn()
        {
            _communicator.SendMessage(_endTurnEncoder.EncodeMessage(new SimpleTextNetworkModel("EndTurn")));
        }

        public void EndGame()
        {
            _communicator.SendMessage(_endGameEncoder.EncodeMessage(new SimpleTextNetworkModel("EndGame")));
        }

        public void GetTargets(string id)
        {
            _communicator.SendMessage(
                _getTargetsEncoder.Value.EncodeMessage(new GetTargetsNetworkModel {Id = id}));
        }
    }
}