﻿using System.Collections.Generic;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.Controllers
{
    public class GameSearchController
    {
        private readonly IMessageEncoder _encoder;
        private IDependencyResolver DI;

        public GameSearchController(IDependencyResolver DI)
        {
            this.DI = DI;
            _encoder = DI.Get<IDependencyResolver>("DIEncoder").Get<IMessageEncoder>("SearchGame");
        }

        public void GoToSearch(List<string> deck, string hero)
        {
            GlobalCore.Communicator.SendMessage(
                _encoder.EncodeMessage(new SearchGameMessageNetworkModel(SearchGameStatus.GoInSearch, deck, hero)));
        }

        public void LeaveFromSearch()
        {
            GlobalCore.Communicator.SendMessage(
                _encoder.EncodeMessage(new SearchGameMessageNetworkModel(SearchGameStatus.GoOutSearch, null, null)));
        }

        public void GetSessionsForView()
        {
            GlobalCore.Communicator.SendMessage(GlobalCore.DependencyResolver.Get<IDependencyResolver>("DIEncoder")
                .Get<IMessageEncoder>("GetSessionList").EncodeMessage(new SimpleTextNetworkModel("GetSessionList")));
        }

        public void ViewForSession(string id)
        {
            GlobalCore.Communicator.SendMessage(GlobalCore.DependencyResolver.Get<IDependencyResolver>("DIEncoder")
                .Get<IMessageEncoder>("ViewForSession").EncodeMessage(new SimpleTextNetworkModel(id)));
        }
    }
}