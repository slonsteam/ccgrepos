﻿using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.Controllers
{
    public class CardController
    {
        private readonly IMessageEncoder _getCardModelEncoder;
        private readonly IDependencyResolver DI;

        public CardController(IDependencyResolver DI)
        {
            this.DI = DI;
            _getCardModelEncoder = DI.Get<IDependencyResolver>("DIEncoder").Get<IMessageEncoder>("GetCardModel");
        }

        public void GetCardModel()
        {
            DI.Get<ICommunicator>()
                .SendMessage(_getCardModelEncoder.EncodeMessage(new SimpleTextNetworkModel("getcards")));
        }
    }
}