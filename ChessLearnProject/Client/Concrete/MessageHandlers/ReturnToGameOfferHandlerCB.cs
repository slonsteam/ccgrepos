﻿using System.Windows;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Client.Concrete.MessageHandlers
{
    public class ReturnToGameOfferHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            //Пришло предложение вернуться в игру
            Application.Current.Dispatcher.Invoke(() =>
            {
                var result = MessageBox.Show(Application.Current.MainWindow, "Вернуться в игру?", "Найдена сессия",
                    MessageBoxButton.YesNo, (Style) Application.Current.MainWindow.FindResource("MessageBoxStyle1"));
                SendReturnToGameOffer(result == MessageBoxResult.OK, DI);
            });
        }

        /// <summary>
        ///     Вызывать для отправки ответа
        /// </summary>
        /// <param name="answer">Ответ</param>
        /// <param name="DI"></param>
        public static void SendReturnToGameOffer(bool answer, IDependencyResolver DI)
        {
            var encoder = DI.Get<IDependencyResolver>("DIEncoder").Get<IMessageEncoder>("ReturnToGameOfferResponse");
            GlobalCore.Communicator.SendMessage(encoder.EncodeMessage(new SimpleTextNetworkModel(answer.ToString())));
        }
    }
}