﻿using System.Windows;
using Client.ViewModels;
using SamePorjLibrary.Interfaces;

namespace Client.Concrete.MessageHandlers
{
    public class ClientLeaveMessageHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                var gvm = DI.Get<GamePageViewModel>();
                if (gvm.EnemyPlayerModel.Token == text)
                {
                }
            });
        }
    }
}