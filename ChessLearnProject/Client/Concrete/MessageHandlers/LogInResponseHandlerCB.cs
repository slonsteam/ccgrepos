﻿using Client.ViewModels;
using SamePorjLibrary.Interfaces;
using Sprache;

namespace Client.Concrete.MessageHandlers
{
    public class LogInResponseHandlerCB
    {
        private static readonly Parser<bool> _resultParser =
            from status in Parse.String("Correct").Or(Parse.String("Error")).Text()
            from other in Parse.AnyChar.Many().End()
            select status == "Correct";

        private static Parser<string> _tokenParser =
            from status in Parse.String("Correct")
            from splitter in Parse.Char(':').Once()
            from token in Parse.LetterOrDigit.Many().End().Text()
            select token;

        public static void Handle(string text, IDependencyResolver DI)
        {
            if (_resultParser.Parse(text))
            {
                DI.Get<StartPageViewModel>().ResetEvent.Set();

                //App.Current.Dispatcher.Invoke(() =>
                //{
                //    DI.Get<GamePageViewModel>().PlayerModel = new PlayerModel(Token);
                //});
                //Валидация прошла, забирай токен
            }
        }
    }
}