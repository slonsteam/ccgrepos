﻿using System.Collections.Generic;
using System.Windows;
using Client.ViewModels;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.Model;

namespace Client.Concrete.MessageHandlers
{
    public class EndTurnResponceHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            //Конец хода
            var actions = JsonConvert.DeserializeObject<List<ActionModel>>(text, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects
            });
            Application.Current.Dispatcher.Invoke(() =>
            {
                DI.Get<GamePageViewModel>().ActionController.Handle(actions);
            });
        }
    }
}