﻿using System.Collections.Generic;
using System.Windows;
using Client.ViewModels;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.Model;

namespace Client.Concrete.MessageHandlers
{
    public class EndGameHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            var Token = text;
            var actions = JsonConvert.DeserializeObject<List<ActionModel>>(text, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects
            });
            Application.Current.Dispatcher.Invoke(() =>
            {
                DI.Get<GamePageViewModel>().ActionController.Handle(actions);
            });
        }
    }
}