﻿using System;
using System.Diagnostics;
using Client.Interfaces;
using SamePorjLibrary.Handlers;
using SamePorjLibrary.Interfaces;

namespace Client.Concrete.MessageHandlers
{
    public class SimpleTextMessageClientHandler : SimpleTextMessageHandler, IClientMessageHandler
    {
        private readonly IDependencyResolver DI;

        public SimpleTextMessageClientHandler(string MessageType, IDependencyResolver DI) : base(MessageType, DI)
        {
            this.DI = DI;
        }

        public void HandleMessage(string message)
        {
            Debug.Print($"Handler:{MessageType} work with {message}");
            var communicator = DI.Get<ICommunicator>();
            HandleMessage(message, communicator);
            MessageHandled(this, new MessageHandledEventArgs {Message = message});
        }

        public event EventHandler<MessageHandledEventArgs> MessageHandled = (s, a) => { };
    }
}