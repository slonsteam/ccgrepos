﻿using System;
using System.Diagnostics;
using Client.Abstract;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.MessageHandlers
{
    public class ChatMessageHandler : ClientMessageHandlerBase
    {
        public ChatMessageHandler(IDependencyResolver DI, ICommunicator communicator) : base("ChatMessage", DI,
            communicator)
        {
        }

        protected override bool HandleMessage(string message, ICommunicator communicator)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ChatMessageNetworkModel>(message);
                GlobalCore.EventCenter.OnMessageForGlobalChatRecieved(model.Name, model.Message);
                return true;
            }
            catch (Exception e)
            {
                Debug.Print(e.Message);
                return false;
            }
        }
    }
}