﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Client.ViewModels;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.Model;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.MessageHandlers
{
    public class GetTableResponseHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            //Пришел стол, забирай
            var model = JsonConvert.DeserializeObject<GetTableNetworkModel>(text);
            var actions = JsonConvert.DeserializeObject<List<ActionModel>>(model.Table);
            //todo доделать костыль с репозом
            Application.Current.Dispatcher.Invoke(() =>
            {
                DI.Get<GamePageViewModel>().ActionController.GetTable(
                    actions.FirstOrDefault(x => x.ActionName == "GetTable" && x.Status == ActionModelStatus.Start)
                        ?.Parametrs
                );
                DI.Get<GamePageViewModel>().ActionController.CanBePlayed(
                    actions.LastOrDefault(x => x.ActionName == "CanBePlayed" && x.Status == ActionModelStatus.Start)
                        ?.Parametrs
                );
                DI.Get<StartPageViewModel>().SearchReset.Set();
            });
        }
    }
}