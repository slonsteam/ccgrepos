﻿using System.Windows;
using Client.ViewModels;
using SamePorjLibrary.Interfaces;
using Sprache;

namespace Client.Concrete.MessageHandlers
{
    public class SessionFindedHandlerCB
    {
        private static readonly Parser<string> _getTokenParser =
            from prefix in Parse.String("Your Token:")
            from Token in Parse.AnyChar.Except(Parse.Char(':')).Many().Text()
            from splitter in Parse.Char(':').Once()
            from other in Parse.AnyChar.Many().End()
            select Token;

        public static void Handle(string text, IDependencyResolver DI)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                var Token = _getTokenParser.Parse(text);
                DI.Get<GamePageViewModel>().PlayerModel = new PlayerModel(Token);
                DI.Get<StartPageViewModel>().SearchReset.Set();
            });
        }
    }
}