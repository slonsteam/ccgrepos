﻿using System.Collections.Generic;
using Client.ViewModels;
using CommonDll.Data;
using Newtonsoft.Json;
using SamePorjLibrary.Data;
using SamePorjLibrary.Interfaces;

namespace Client.Concrete.MessageHandlers
{
    public class GetCardModelResponseHandlerCb
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            var model = JsonConvert.DeserializeObject<List<CardModel>>(text);
            //todo доделать костыль с репозом
            DI.Get<IRepository<CardModel>>().Collection = model;
            DI.Get<StartPageViewModel>().ResetEvent.Set();
        }
    }
}