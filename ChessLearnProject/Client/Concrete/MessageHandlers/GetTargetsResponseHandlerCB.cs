﻿using System.Windows;
using Client.ViewModels;
using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.MessageHandlers
{
    public class GetTargetsResponseHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            var model = JsonConvert.DeserializeObject<GetTargetResponseNetworkModel>(text);
            //todo возможна ошибка синхронизации потоков
            Application.Current.Dispatcher.Invoke(() =>
            {
                DI.Get<GamePageViewModel>().ActionController.SetTarget(model.Targets);
            });
        }
    }
}