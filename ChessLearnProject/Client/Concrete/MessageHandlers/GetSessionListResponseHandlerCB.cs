﻿using Newtonsoft.Json;
using SamePorjLibrary.Interfaces;
using SamePorjLibrary.NetworkModels;

namespace Client.Concrete.MessageHandlers
{
    public class GetSessionListResponseHandlerCB
    {
        public static void Handle(string text, IDependencyResolver DI)
        {
            var model = JsonConvert.DeserializeObject<GetSessionListResponseNetworkModel>(text);
            //Получай лист сессий
        }
    }
}