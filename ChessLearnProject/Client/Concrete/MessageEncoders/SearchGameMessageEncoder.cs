﻿using SamePorjLibrary.Abstract;

namespace Client.Concrete.MessageEncoders
{
    public class SearchGameMessageEncoder : MessageEncoderBase
    {
        public SearchGameMessageEncoder() : base("SearchGame")
        {
        }
    }
}