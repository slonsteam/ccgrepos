﻿using SamePorjLibrary.Abstract;

namespace Client.Concrete.MessageEncoders
{
    public class LogInMessageEncoder : MessageEncoderBase
    {
        public LogInMessageEncoder() : base("LogIn")
        {
        }
    }
}