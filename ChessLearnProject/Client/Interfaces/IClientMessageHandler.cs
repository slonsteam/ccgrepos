﻿using System;

namespace Client.Interfaces
{
    public interface IClientMessageHandler
    {
        void HandleMessage(string message);
        event EventHandler<MessageHandledEventArgs> MessageHandled;
    }

    public class MessageHandledEventArgs : EventArgs
    {
        public string Message;
    }
}