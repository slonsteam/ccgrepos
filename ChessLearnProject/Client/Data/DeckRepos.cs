﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Client.Concrete.Controllers;
using CommonDll.Data;

namespace Client.Data
{
    public class DeckRepos : IRepository<string>
    {
        public DeckRepos()
        {
            try
            {
                LocalCashController.GoFromCash("decks");
                Collection = LocalCashController.Model.Decks["deck"];
            }
            catch (Exception e)
            {
                Debug.WriteLine("Колады нет");
                Collection = new List<string>
                {
                    "Warlock",
                    "Cyrus",
                    "Genestealer",
                    "Genestealer",
                    "Ogre",
                    "Ogre",
                    "Cyrus",
                    "Tankred",
                    "DropPod",
                    "DropPod",
                    "ReinforcementsOrbit",
                    "ReinforcementsOrbit",
                    "ScoutOfBloodRavens",
                    "ScoutOfBloodRavens",
                    "Cyrus",
                    "Ogre"
                };
            }
        }

        public IEnumerable<string> Collection { get; set; }

        public string Get(string id)
        {
            return Collection.FirstOrDefault(x => x == id);
        }
    }
}