﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Client.Concrete.Controllers;
using Client.UI;
using Client.ViewModels.Menu;

namespace Client.ViewModels
{
    public class GamePageViewModel : BaseViewModel
    {
        private RelayCommand _endTurnCommand;
        private PlayerModel _enemyPlayerModel;

        private bool _isattack;
        private bool _menuActive;

        private RelayCommand _menuCommand;
        private PlayerModel _playerModel;
        private RelayCommand _surrenderCommand;

        public GamePageViewModel(ActionController actionController)
        {
            ResetEvent = new ManualResetEvent(false);
            PlayerModel = new PlayerModel();
            EnemyPlayerModel = new PlayerModel();
            ActionController = actionController;
            actionController._viewModel = this;
            MenuActive = false;
            Menu = new MenuViewModel
            {
                MenuItems = new ObservableCollection<MenuItem>
                {
                    new MenuItem
                    {
                        Name = "Сдаться",
                        Command = SurrenderCommand
                    }
                }
            };
        }

        public ManualResetEvent ResetEvent { get; }

        public bool MenuActive
        {
            get => _menuActive;
            set
            {
                _menuActive = value;
                OnPropertyChanged(nameof(MenuActive));
            }
        }

        public MenuViewModel Menu { get; set; }

        public bool IsAttack
        {
            get => _isattack;
            set
            {
                _isattack = value;
                OnPropertyChanged(nameof(IsAttack));
            }
        }

        public IEnumerable<CardViewModel> Targets => AllCards.Where(x => x.CanBeTarget);
        public IEnumerable<CardViewModel> AllCards => PlayerModel.AllCards.Concat(EnemyPlayerModel.AllCards);
        public ActionController ActionController { get; }

        public PlayerModel PlayerModel
        {
            get => _playerModel;
            set
            {
                _playerModel = value;
                OnPropertyChanged(nameof(PlayerModel));
            }
        }

        public PlayerModel EnemyPlayerModel
        {
            get => _enemyPlayerModel;
            set
            {
                _enemyPlayerModel = value;
                OnPropertyChanged(nameof(EnemyPlayerModel));
            }
        }

        public CardViewModel DraggedCard { get; set; }
        public CardViewModel Source { get; set; }

        public RelayCommand EndTurnCommand
        {
            get
            {
                return _endTurnCommand ??
                       (
                           _endTurnCommand = new RelayCommand(obj => { ActionController.EndTurn(); },
                               o => PlayerModel.IsMyTurn));
            }
        }

        public RelayCommand MenuCommand
        {
            get
            {
                return _menuCommand ??
                       (
                           _menuCommand = new RelayCommand(obj => { MenuActive = !MenuActive; })
                       );
            }
        }

        public RelayCommand SurrenderCommand => _surrenderCommand ??
                                                (_surrenderCommand = new RelayCommand(o =>
                                                {
                                                    ActionController.EndGame();
                                                }));

        public void ResetTargets()
        {
            foreach (var cardViewModel in Targets) cardViewModel.CanBeTarget = false;
        }

        public event EventHandler<CardViewModel> TargetsRecieved;

        public void RaiseTargetsRecived(CardViewModel vm)
        {
            TargetsRecieved?.Invoke(null, vm);
        }
    }
}