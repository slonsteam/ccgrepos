﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using Client.Concrete.Controllers;
using Client.UI;
using Client.UI.Windows;
using Client.ViewModels.Menu;
using CommonDll.Data;
using GalaSoft.MvvmLight.Threading;
using SamePorjLibrary.Data;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Client.ViewModels
{
    public class StartPageViewModel : BaseViewModel
    {
        private RelayCommand _cancelSearchCommand;
        private bool _isLogged;
        private bool _isLogging;
        private bool _isSearching;
        private string _login;
        private RelayCommand _loginCommand;
        private Uri _mediaUri;
        private string _password;

        public StartPageViewModel(GameSearchController searchController)
        {
            ResetEvent = new AutoResetEvent(false);
            SearchReset = new AutoResetEvent(false);
            GameSearchController = searchController;
            IsLogging = false;
            Menu = new MenuViewModel
            {
                MenuItems = new ObservableCollection<MenuItem>
                {
                    new MenuItem
                    {
                        Command = new RelayCommand(obj =>
                        {
                            IsBusy = false;
                            BusyMessage = "Поиск игры";
                            ThreadPool.QueueUserWorkItem(o =>
                            {
                                if (Connect())
                                {
                                    var mydeck = GlobalCore.DependencyResolver.Get<IRepository<string>>()
                                        .Collection;
                                    var hero = mydeck.First();
                                    var units = mydeck.Skip(1);
                                    IsBusy = true;
                                    GameSearchController.GoToSearch(units.ToList(), hero);
                                    SearchReset.WaitOne();
                                    DispatcherHelper.CheckBeginInvokeOnUI(
                                        () =>
                                        {
                                            IsSearching = false;
                                            IsBusy = false;
                                            var gamepage = new GamePage();
                                            MainViewModel.CurrentPage = gamepage;
                                            gamepage.DataContext =
                                                GlobalCore.DependencyResolver.Get<GamePageViewModel>();
                                            gamepage.ViewModel.TargetsRecieved += gamepage.TargetsRecievedHandler;
                                        });
                                }
                            });
                        }),
                        Name = "Поиск"
                    },
                    new MenuItem
                    {
                        Name = "Редактор колод",
                        Command = new RelayCommand(obj =>
                        {
                            ThreadPool.QueueUserWorkItem(o =>
                            {
                                if (Connect())
                                {
                                    var mydeck = GlobalCore.DependencyResolver.Get<IRepository<string>>()
                                        .Collection;
                                    DispatcherHelper.CheckBeginInvokeOnUI(
                                        () =>
                                        {
                                            MainViewModel.DeckPage = new DeckEditorFrame();
                                            MainViewModel.CurrentPage = MainViewModel.DeckPage;
                                            MainViewModel.DeckPage.DataContext =
                                                new DeckEditorViewModel(
                                                    GlobalCore.DependencyResolver.Get<IRepository<CardModel>>(),
                                                    MainViewModel);
                                        });
                                }
                            });
                        })
                    },
                    new MenuItem
                    {
                        Name = "Выход"
                    }
                }
            };
        }

        public MenuViewModel Menu { get; set; }
        public MainViewModel MainViewModel { get; set; }
        public GameSearchController GameSearchController { get; }
        public AutoResetEvent ResetEvent { get; }
        public AutoResetEvent SearchReset { get; set; }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        public Uri MediaUri
        {
            get => _mediaUri;
            set
            {
                _mediaUri = value;
                OnPropertyChanged(nameof(MediaUri));
            }
        }

        public bool IsSearching
        {
            get => _isSearching;
            set
            {
                _isSearching = value;
                OnPropertyChanged(nameof(MediaUri));
            }
        }

        public bool IsLogging
        {
            get => _isLogging;
            set
            {
                _isLogging = value;
                OnPropertyChanged(nameof(IsLogging));
            }
        }

        public bool IsLogged
        {
            get => _isLogged;
            set
            {
                _isLogged = value;
                OnPropertyChanged(nameof(IsLogged));
            }
        }

        public RelayCommand CancelSearchCommand
        {
            get
            {
                return _cancelSearchCommand ??
                       (
                           _cancelSearchCommand = new RelayCommand(obj =>
                           {
                               GameSearchController.LeaveFromSearch();
                               IsBusy = false;
                           }));
            }
        }

        public RelayCommand LoginCommand
        {
            get { return _loginCommand ?? (_loginCommand = new RelayCommand(o => { ResetEvent.Set(); })); }
        }

        public bool Connect()
        {
            try
            {
                if (!IsLogged)
                {
                    (Application.Current as App).ConnectToServer();
                    IsLogging = true;
                    ResetEvent.WaitOne();
                    IsLogging = false;
                    GlobalCore.DependencyResolver.Get<AccountController>().LogIn(Login, Password);
                    ResetEvent.WaitOne();
                    IsLogged = true;
                    GlobalCore.DependencyResolver.Get<CardController>().GetCardModel();
                    //доделать под колоду
                    ResetEvent.WaitOne();
                }
            }
            catch (Exception e)
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    MessageBox.Show(Application.Current.MainWindow, "Ошибка подключения", "Ошибка",
                        (Style) Application.Current.MainWindow.FindResource("MessageBoxStyle1"));
                    IsBusy = false;
                });
                return false;
            }

            return true;
        }
    }
}