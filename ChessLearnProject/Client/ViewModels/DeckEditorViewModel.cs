﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Client.Concrete.Controllers;
using Client.UI;
using CommonDll.Data;
using SamePorjLibrary.Data;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Client.ViewModels
{
    public class DeckEditorViewModel : BaseViewModel
    {
        private RelayCommand _addCardToDeckCommand;
        private RelayCommand _allFilterCommand;

        private RelayCommand _backCommand;
        private string _cardCount;
        private ICollectionView _deck;
        private RelayCommand _heroesFilterCommand;
        private ObservableCollection<CardViewModel> _myDeck;
        private RelayCommand _removeCardFromDeckCommand;
        private RelayCommand _saveCommand;
        private RelayCommand _spellsFilterCommand;
        private RelayCommand _unitsFilterCommand;
        private readonly IRepository<string> cacheddeck;
        private readonly MainViewModel mvm;

        public DeckEditorViewModel(IRepository<CardModel> _repository, MainViewModel mvm)
        {
            //todo костыль
            this.mvm = mvm;
            cacheddeck = GlobalCore.DependencyResolver.Get<IRepository<string>>();
            MyDeck = new ObservableCollection<CardViewModel>();

            var list = new List<CardViewModel>();

            foreach (var card in _repository.Collection)
                if (card.CanBeInDec)
                    list.Add(new CardViewModel(card.Identificator, null, card));

            foreach (var card in cacheddeck.Collection)
                MyDeck.Add(new CardViewModel(card, null, _repository.Get(card)));
            Deck = CollectionViewSource.GetDefaultView(list);
        }

        public ICollectionView Deck
        {
            get => _deck;
            set
            {
                _deck = value;
                OnPropertyChanged(nameof(Deck));
            }
        }

        public ObservableCollection<CardViewModel> MyDeck
        {
            get => _myDeck;
            set
            {
                _myDeck = value;
                OnPropertyChanged(nameof(MyDeck));
            }
        }

        public RelayCommand RemoveCardFromDeckCommand =>
            _removeCardFromDeckCommand ?? (_removeCardFromDeckCommand = new RelayCommand(o =>
            {
                var card = o as CardViewModel;
                MyDeck.Remove(card);
                OnPropertyChanged(nameof(CardCount));
            }));

        public string CardCount
        {
            get => MyDeck.Count + " / " + "16";
            set
            {
                _cardCount = value;
                OnPropertyChanged(nameof(CardCount));
            }
        }

        public RelayCommand AddCardToDeckCommand => _addCardToDeckCommand ?? (_addCardToDeckCommand = new RelayCommand(
                                                        obj =>
                                                        {
                                                            var card = obj as CardViewModel;
                                                            if (card.Type == "Hero")
                                                                if (MyDeck.FirstOrDefault(x => x.Type == "Hero") == null
                                                                )
                                                                    MyDeck.Insert(0, card);
                                                                else
                                                                    MyDeck[0] = card;
                                                            else
                                                                MyDeck.Add(card);
                                                            OnPropertyChanged(nameof(CardCount));
                                                        }));

        public RelayCommand BackCommand => _backCommand ?? (
                                               _backCommand = new RelayCommand(obj =>
                                               {
                                                   mvm.CurrentPage = mvm.StartPage;
                                               })
                                           );

        public RelayCommand HeroesFilterCommand => _heroesFilterCommand ?? (_heroesFilterCommand = new RelayCommand(o =>
        {
            Deck.Filter = obj =>
            {
                var card = obj as CardViewModel;
                return card.Type == "Hero";
            };
            Deck.Refresh();
        }));

        public RelayCommand UnitsFilterCommand
        {
            get => _unitsFilterCommand ?? (_unitsFilterCommand = new RelayCommand(o =>
            {
                Deck.Filter = obj =>
                {
                    var card = obj as CardViewModel;
                    return card.Type == "Unit";
                };
                Deck.Refresh();
            }));
            set => _unitsFilterCommand = value;
        }

        public RelayCommand SpellsFilterCommand
        {
            get => _spellsFilterCommand ?? (_spellsFilterCommand = new RelayCommand(o =>
            {
                Deck.Filter = obj =>
                {
                    var card = obj as CardViewModel;
                    return card.Type == "Spell";
                };
                Deck.Refresh();
            }));
            set => _spellsFilterCommand = value;
        }

        public RelayCommand AllFilterCommand
        {
            get => _allFilterCommand ?? (_allFilterCommand = new RelayCommand(o =>
            {
                Deck.Filter = obj => { return true; };
                Deck.Refresh();
            }));
            set => _allFilterCommand = value;
        }

        public RelayCommand SaveCommand => _saveCommand ?? (
                                               _saveCommand = new RelayCommand(obj =>
                                               {
                                                   if (MyDeck.FirstOrDefault(x => x.Type == "Hero") != null &&
                                                       MyDeck.Count == 16)
                                                   {
                                                       GlobalCore.DependencyResolver.Get<IRepository<string>>()
                                                               .Collection =
                                                           MyDeck.Select(x => x.Token);
                                                       LocalCashController.Model.AddDeck("deck",
                                                           MyDeck.Select(x => x.Token).ToList());
                                                       LocalCashController.GoToCash("decks");
                                                       MessageBox.Show(Application.Current.MainWindow,
                                                           "Колода сохранена", "Успех",
                                                           (Style) Application.Current.MainWindow.FindResource(
                                                               "MessageBoxStyle1"));
                                                   }
                                                   else
                                                   {
                                                       MessageBox.Show(Application.Current.MainWindow,
                                                           "Колода составлена неверно, изменения не сохранены",
                                                           "Ошибка",
                                                           (Style) Application.Current.MainWindow.FindResource(
                                                               "MessageBoxStyle1"));
                                                   }
                                               })
                                           );
    }
}