﻿using Client.UI;

namespace Client.ViewModels.Menu
{
    public class MenuItem : BaseViewModel
    {
        private RelayCommand _command;
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public RelayCommand Command
        {
            get => _command;
            set
            {
                _command = value;
                OnPropertyChanged(nameof(Command));
            }
        }
    }
}