﻿using System.Collections.ObjectModel;

namespace Client.ViewModels.Menu
{
    public class MenuViewModel : BaseViewModel
    {
        private ObservableCollection<MenuItem> _menuItems;

        public ObservableCollection<MenuItem> MenuItems
        {
            get => _menuItems;
            set
            {
                _menuItems = value;
                OnPropertyChanged(nameof(MenuItems));
            }
        }
    }
}