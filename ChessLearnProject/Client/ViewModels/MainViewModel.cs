﻿using System.Windows.Controls;
using Client.UI.Windows;

namespace Client.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private Page _currentpage;

        public DeckEditorFrame DeckPage;

        //todo зарефакторить
        public StartPage StartPage;

        public MainViewModel(StartPageViewModel startPageViewModel)
        {
            StartPage = new StartPage();
            CurrentPage = StartPage;
            CurrentPage.DataContext = startPageViewModel;
            startPageViewModel.MainViewModel = this;
        }

        public Page CurrentPage
        {
            set
            {
                _currentpage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
            get => _currentpage;
        }
    }
}