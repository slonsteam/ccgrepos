﻿using System.Collections.ObjectModel;
using SamePorjLibrary.Data;

namespace Client.ViewModels
{
    public class CardViewModel : BaseViewModel
    {
        private bool _attacked;
        private int _baseCost;
        private int _baseDamage;
        private int _baseHealth;
        private bool _canBePlayed;
        private bool _canBeTarget;
        private int _cost;
        private int _currentBaseDamage;
        private int _currentBaseHealth;
        private int _damage;
        private string _description;
        private int _health;
        private bool _isRemoved;
        private string _name;
        private ObservableCollection<string> _states;
        private string _type;

        public CardViewModel(string token, PlayerModel player, CardModel card = null)
        {
            Token = token;
            States = new ObservableCollection<string>();
            Player = player;
            IsRemoved = false;
            if (card != null)
                Initialize(card, player);
            else
                ImageSource = "pack://application:,,,/UI/Content/Images/cardface.png";
        }

        public PlayerModel Player { get; set; }

        public int CurrentBaseDamage
        {
            get => _currentBaseDamage;
            set
            {
                _currentBaseDamage = value;
                OnPropertyChanged(nameof(CurrentBaseDamage));
                OnPropertyChanged(nameof(IsDamageBuffed));
            }
        }

        public int CurrentBaseHealth
        {
            get => _currentBaseHealth;
            set
            {
                _currentBaseHealth = value;
                OnPropertyChanged(nameof(CurrentBaseHealth));
                OnPropertyChanged(nameof(IsHealthBuffed));
            }
        }

        public bool IsHealthBuffed => CurrentBaseHealth > BaseHealth;
        public bool IsDamageBuffed => CurrentBaseDamage > BaseDamage;
        public bool IsHelthDecreased => CurrentBaseHealth > Health;
        public bool IsDamageDecreased => CurrentBaseDamage > Damage;
        public bool Initialized { get; set; }

        public ObservableCollection<string> States
        {
            get => _states;
            set
            {
                _states = value;
                OnPropertyChanged(nameof(States));
            }
        }

        public string Token { get; }
        public string ImageSource { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int Health
        {
            get => _health;
            set
            {
                _health = value;
                OnPropertyChanged(nameof(Health));
                OnPropertyChanged(nameof(IsHelthDecreased));
            }
        }

        public int Damage
        {
            get => _damage;
            set
            {
                _damage = value;
                OnPropertyChanged(nameof(Damage));
                OnPropertyChanged(nameof(IsDamageDecreased));
            }
        }

        public int BaseDamage
        {
            get => _baseDamage;
            set
            {
                _baseDamage = value;
                OnPropertyChanged(nameof(BaseDamage));
            }
        }


        public int BaseCost
        {
            get => _baseCost;
            set
            {
                _baseCost = value;
                OnPropertyChanged(nameof(BaseCost));
            }
        }

        public int CurrentCost
        {
            get => _cost;
            set
            {
                _cost = value;
                OnPropertyChanged(nameof(CurrentCost));
            }
        }

        public int BaseHealth
        {
            get => _baseHealth;
            set
            {
                _baseHealth = value;
                OnPropertyChanged(nameof(BaseHealth));
            }
        }

        public string Type
        {
            get => _type;
            set
            {
                _type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public bool CanBeTarget
        {
            get => _canBeTarget;
            set
            {
                _canBeTarget = value;
                OnPropertyChanged(nameof(CanBeTarget));
            }
        }

        public bool CanBePlayed
        {
            get => _canBePlayed;
            set
            {
                _canBePlayed = value;
                OnPropertyChanged(nameof(CanBePlayed));
            }
        }

        public bool IsRemoved
        {
            get => _isRemoved;
            set
            {
                _isRemoved = value;
                OnPropertyChanged(nameof(IsRemoved));
            }
        }


        public bool Attacked
        {
            get => _attacked;
            set
            {
                _attacked = value;
                OnPropertyChanged(nameof(Attacked));
            }
        }

        public void Initialize(CardModel card, PlayerModel player)
        {
            if (!Initialized)
            {
                Name = card.Name;
                Description = card.Description;
                Type = card.Type;
                CurrentCost = BaseCost = card.BaseCost;
                CurrentBaseDamage = Damage = BaseDamage = card.BaseDamage;
                CurrentBaseHealth = Health = BaseHealth = card.BaseHealth;
                ImageSource = card.ImageSource;
                CanBePlayed = false;
            }

            Player = player;

            Initialized = true;
        }

        public void CardAttacked()
        {
            Attacked = true;
            Attacked = false;
        }

        public void RaiseChangedTarget()
        {
            OnPropertyChanged(nameof(CanBeTarget));
            OnPropertyChanged(nameof(CanBePlayed));
            OnPropertyChanged(nameof(Health));
            OnPropertyChanged(nameof(BaseHealth));
            OnPropertyChanged(nameof(CurrentCost));
            OnPropertyChanged(nameof(BaseCost));
            OnPropertyChanged(nameof(Damage));
            OnPropertyChanged(nameof(BaseDamage));
        }
    }
}