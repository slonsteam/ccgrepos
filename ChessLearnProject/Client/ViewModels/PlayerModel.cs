﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Client.ViewModels
{
    public class PlayerModel : BaseViewModel
    {
        private ObservableCollection<CardViewModel> _cardsInHand;
        private int _currentMana;
        private int _fullMana;
        private CardViewModel _hero;
        private bool _isLeaved;
        private bool _isMyTurn;
        private ObservableCollection<CardViewModel> _unitsOnTable;

        public PlayerModel(string token = null)
        {
            Token = token;
            RemovedCards = new List<CardViewModel>();
            Deck = new List<CardViewModel>();
            UnitsOnTable = new ObservableCollection<CardViewModel>();
            CardsInHand = new ObservableCollection<CardViewModel>();
            CardsInHand.CollectionChanged += (sender, args) =>
            {
                if (args.Action == NotifyCollectionChangedAction.Remove)
                {
                    var item = args.OldItems[0] as CardViewModel;
                    item.IsRemoved = true;
                    item.IsRemoved = false;
                }
            };
            CurrentMana = 0;
            FullMana = 0;
        }

        public int FullMana
        {
            get => _fullMana;
            set
            {
                _fullMana = value;
                OnPropertyChanged(nameof(FullMana));
                OnPropertyChanged(nameof(ManaString));
            }
        }

        public string ManaString => CurrentMana + "/" + FullMana;

        public int CurrentMana
        {
            get => _currentMana;
            set
            {
                _currentMana = value;
                OnPropertyChanged(nameof(CurrentMana));
                OnPropertyChanged(nameof(ManaString));
            }
        }

        public bool IsLeaved
        {
            get => _isLeaved;
            set
            {
                _isLeaved = value;
                OnPropertyChanged(nameof(IsLeaved));
            }
        }

        public bool IsMyTurn
        {
            get => _isMyTurn;
            set
            {
                _isMyTurn = value;
                OnPropertyChanged(nameof(IsMyTurn));
            }
        }

        public List<CardViewModel> RemovedCards { get; set; }

        public CardViewModel Hero
        {
            get => _hero;
            set
            {
                _hero = value;
                OnPropertyChanged(nameof(Hero));
            }
        }

        public string Token { get; }

        public IEnumerable<CardViewModel> AllCards => UnitsOnTable.Concat(CardsInHand).Concat(RemovedCards)
            .Concat(new List<CardViewModel> {Hero});

        public List<CardViewModel> Deck { get; set; }

        public ObservableCollection<CardViewModel> UnitsOnTable
        {
            get => _unitsOnTable;
            set
            {
                _unitsOnTable = value;
                OnPropertyChanged(nameof(UnitsOnTable));
            }
        }

        public ObservableCollection<CardViewModel> CardsInHand
        {
            get => _cardsInHand;
            set
            {
                _cardsInHand = value;
                OnPropertyChanged(nameof(CardsInHand));
            }
        }
    }
}