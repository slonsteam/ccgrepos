﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using Client.Concrete.Controllers;
using Client.Concrete.MessageHandlers;
using Client.Data;
using Client.Interfaces;
using Client.ViewModels;
using CommonDll.Data;
using GalaSoft.MvvmLight.Threading;
using Ninject;
using SamePorjLibrary;
using SamePorjLibrary.Data;
using SamePorjLibrary.Encoders;
using SamePorjLibrary.Interfaces;
using Sprache;

namespace Client
{
    /// <summary>
    ///     Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static IKernel kernel;

        private readonly Parser<string> _messageTypeParser =
            from type in Parse.Letter.Many().Text()
            from spliter in Parse.Char(':').Once()
            from message in Parse.AnyChar.Many()
            select type;

        private readonly Parser<string> _messageBodyParser =
            from type in Parse.Letter.Many()
            from spliter in Parse.Char(':').Once()
            from message in Parse.AnyChar.Many().Text()
            select message;

        private readonly string _serverAdress = "127.0.0.1";
        private readonly int _serverPort = 103;

        public void ConnectToServer()
        {
            GlobalCore.Communicator = GlobalCore.DependencyResolver.Get<ICommunicator>();
            if (GlobalCore.Communicator == null)
                throw new NetworkInformationException();
            GlobalCore.Communicator.MessageReaded += (s, a) =>
            {
                GlobalCore.GameProcess.Post(S =>
                {
                    var handler =
                        GlobalCore.DIForMessageConveyor.Get<IClientMessageHandler>(_messageTypeParser.Parse(a.Message));
                    handler.HandleMessage(_messageBodyParser.Parse(a.Message));
                }, null);
            };

            GlobalCore.Communicator.BeginListenForMessage();
        }


        private ICommunicator CreateCommunicator()
        {
            try
            {
                if (GlobalCore.Communicator == null)
                {
                    GlobalCore.Communicator =
                        new ClientCommunicator(new TcpClient(_serverAdress, _serverPort), Encoding.UTF8);

                    GlobalCore.Communicator.ConnectionBroken += (s, a) =>
                    {
                        MessageBox.Show("Потеряно соединение");
                        GlobalCore.Communicator?.Dispose();
                        GlobalCore.Communicator = null;
                    };
                }

                return GlobalCore.Communicator;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            GlobalCore.ClientClosing += (s, a) => { GlobalCore.GameProcess.Dispose(); };
            var DIEncoders = new NinjectDependencyResolver();
            DIEncoders.CreateBindings += kernel =>
            {
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("PlayCardMessage"))
                    .Named("PlayCardMessage");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("SearchGame"))
                    .Named("SearchGame");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("LogIn")).Named("LogIn");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("EndTurn")).Named("EndTurn");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetCardModel"))
                    .Named("GetCardModel");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("ReturnToGameOfferResponse"))
                    .Named("ReturnToGameOfferResponse");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetTable")).Named("GetTable");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetTargets"))
                    .Named("GetTargets");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("GetSessionList"))
                    .Named("GetSessionList");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("ViewForSession"))
                    .Named("ViewForSession");
                kernel.Bind<IMessageEncoder>().ToConstant(new SimpleTextMessageEncoder("EndGame"))
                    .Named("EndGame");
            };
            DIEncoders.InitializeBindings();

            var DI = new NinjectDependencyResolver();
            GlobalCore.DependencyResolver = DI;
            DI.CreateBindings += kernel =>
            {
                var ServerIP = _serverAdress;
                var ServerPort = _serverPort;
                kernel.Bind<ICommunicator>().ToMethod(c => CreateCommunicator());
                kernel.Bind<IDependencyResolver>().ToConstant(DIEncoders).Named("DIEncoder");
                kernel.Bind<GameSearchController>().ToSelf().InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<AccountController>().ToSelf().InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<CardController>().ToSelf().InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<GamePocessor>().ToSelf().InSingletonScope().WithConstructorArgument("DI", DI);
                kernel.Bind<MainViewModel>().ToSelf().InSingletonScope();
                kernel.Bind<StartPageViewModel>().ToSelf().InSingletonScope();
                kernel.Bind<ActionController>().ToSelf().InSingletonScope();
                kernel.Bind<GamePageViewModel>().ToSelf().InSingletonScope();
                //Временно для колоды
                kernel.Bind<IRepository<string>>().To<DeckRepos>().InSingletonScope();
                kernel.Bind<IRepository<CardModel>>().To<CardModelRepos>().InSingletonScope();
            };
            DI.InitializeBindings();

            //ConnectToServer();//Подключение к серверу, если не нужно, комменть


            var DIForMessageHandling = new NinjectDependencyResolver();
            DIForMessageHandling.CreateBindings += kernel =>
            {
                //kernel.Bind<IClientMessageHandler>()
                //    .ToConstant(new ChatMessageHandler(GlobalCore.DependencyResolver, GlobalCore.Communicator))
                //    .Named("ChatMessage");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("PlayCardResponce", GlobalCore.DependencyResolver)
                        {
                            Handler = PlayCardResponceHandlerCB.Handle
                        }).Named("PlayCardResponce");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("StartGame", GlobalCore.DependencyResolver)
                        {
                            Handler = StartGameHandlerCB.Handle
                        }).Named("StartGame");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("SessionFinded", GlobalCore.DependencyResolver)
                        {
                            Handler = SessionFindedHandlerCB.Handle
                        }).Named("SessionFinded");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("LogInResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = LogInResponseHandlerCB.Handle
                        }).Named("LogInResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("EndTurnResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = EndTurnResponceHandlerCB.Handle
                        }).Named("EndTurnResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("GetCardModelResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = GetCardModelResponseHandlerCb.Handle
                        }).Named("GetCardModelResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("ReturnToGameOffer", GlobalCore.DependencyResolver)
                        {
                            Handler = ReturnToGameOfferHandlerCB.Handle
                        }).Named("ReturnToGameOffer");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("GetTableResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = GetTableResponseHandlerCB.Handle
                        }).Named("GetTableResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("ClientLeave", GlobalCore.DependencyResolver)
                        {
                            Handler = ClientLeaveMessageHandlerCB.Handle
                        }).Named("ClientLeave");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("GetTargetsResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = GetTargetsResponseHandlerCB.Handle
                        }).Named("GetTargetsResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("EndGameResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = EndGameHandlerCB.Handle
                        }).Named("EndGameResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("GetSessionListResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = GetSessionListResponseHandlerCB.Handle
                        }).Named("GetSessionListResponse");
                kernel.Bind<IClientMessageHandler>()
                    .ToConstant(
                        new SimpleTextMessageClientHandler("ViewForSessionResponse", GlobalCore.DependencyResolver)
                        {
                            Handler = ViewForSessionResponseHandlerCB.Handle
                        }).Named("ViewForSessionResponse");
            };
            DIForMessageHandling.InitializeBindings();
            DispatcherHelper.Initialize();
            GlobalCore.DIForMessageConveyor = DIForMessageHandling;
            try
            {
                LocalCashController.GoFromCash(Directory.GetCurrentDirectory() + "\\localCash.txt");
            }
            catch
            {
                Debug.Print("Cash not found");
            }


            GlobalCore.EventCenter = new EventCenter();
            MainWindow = new MainWindow();
            MainWindow.DataContext = DI.Get<MainViewModel>();
            MainWindow.Show();
        }

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Debug.WriteLine("Необработанное исключение на клиенте - " + e.Exception.Message + e.Exception.Source);
        }
    }
}