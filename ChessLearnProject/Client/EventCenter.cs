﻿using System;

namespace Client
{
    public class EventCenter
    {
        public event EventHandler<ChatMessageEventArgs> MessageForGlobalChatRecieved = (s, a) => { };

        public virtual void OnMessageForGlobalChatRecieved(string nick, string message)
        {
            MessageForGlobalChatRecieved?.Invoke(this, new ChatMessageEventArgs
            {
                Message = message,
                Nick = nick
            });
        }
    }

    public class ChatMessageEventArgs : EventArgs
    {
        public string Message;
        public string Nick;
    }
}