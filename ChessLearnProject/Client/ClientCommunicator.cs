﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using SamePorjLibrary;

namespace Client
{
    public class ClientCommunicator : TcpCommunicatorBase
    {
        public ClientCommunicator(TcpClient client, Encoding enc) : base(client, enc)
        {
            SynchronizationContext = new CustomSynchronizationContext();
            GlobalCore.ClientClosing += (s, a) => { Dispose(); };
        }

        public CustomSynchronizationContext SynchronizationContext { get; }

        protected override void MessageRecievedAsyncCallback(IAsyncResult ar)
        {
            SynchronizationContext.Post(s => base.MessageRecievedAsyncCallback(ar), null);
        }

        public override void SendMessage(string text)
        {
            SynchronizationContext.Post(s =>
            {
                base.SendMessage(text);
                Debug.Print("From client");
            }, null);
        }

        public override void BeginListenForMessage()
        {
            SynchronizationContext.Post(s => base.BeginListenForMessage(), null);
        }

        public override void EndListenForMessage()
        {
            SynchronizationContext.Post(s => base.EndListenForMessage(), null);
        }

        public override void Dispose()
        {
            base.Dispose();
            SynchronizationContext.Dispose();
        }

        public override void ConnectAsync(string adres, int port)
        {
            SynchronizationContext.Post(S =>
            {
                var connectTimer = new Timer(s =>
                {
                    if (!_client.Connected)
                        try
                        {
                            _client = new TcpClient(adres, port);
                        }
                        catch
                        {
                            //Оповестить пользователя о том, что соединится не получилось
                        }

                    if (_client.Connected)
                    {
                        StartHeartbit();
                        OnConnectionCreated();
                    }
                }, null, 0, 5000);

                ConnectionCreated += (s, a) => { connectTimer?.Dispose(); };
            }, null);
        }
    }
}