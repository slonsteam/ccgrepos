﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.NetworkModels
{
    public class PlayCardNetworkModel
    {
        public int CardPlayed { get; set; }
        public List<int> Targets { get; set; }
    }
}
