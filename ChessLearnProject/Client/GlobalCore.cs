﻿using System;
using System.Threading;
using SamePorjLibrary;
using SamePorjLibrary.Interfaces;

namespace Client
{
    public static class GlobalCore
    {
        public static IDependencyResolver DependencyResolver;
        public static IDependencyResolver DIForMessageConveyor;
        public static ICommunicator Communicator;

        public static EventCenter EventCenter;

        //todo Заменить резет
        public static AutoResetEvent Reset = new AutoResetEvent(false);
        public static CustomSynchronizationContext GameProcess = new CustomSynchronizationContext();
        public static event EventHandler ClientClosing = (s, a) => { };

        public static void OnClientClosing()
        {
            ClientClosing(null, EventArgs.Empty);
        }
    }
}