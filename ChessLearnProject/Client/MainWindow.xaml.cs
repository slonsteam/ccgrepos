﻿using System.ComponentModel;
using System.Windows;
using Client.ViewModels;

namespace Client
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //GlobalCore.DependencyResolver.Get<GameSearchController>()
            //    .GoToSearch(new List<string> {"Ogre", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre", "Ogre" }, "Warlock");//Временное решение.
        }

        public MainViewModel ViewModel => DataContext as MainViewModel;

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            GlobalCore.OnClientClosing();
        }
    }
}